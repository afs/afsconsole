#!/usr/bin/env perl

################################################################################
#
# createTestVolumes.pl
#
#    Available options
#          -c              : check only, take no actions
#          -s              : prepare all servers
#	   -S <servername> : prepare server <servername>
#	   -u              : unprepare all servers
#	   -U <servername> : unprepare server <servername>
#	   -P <partition>  : pick only partition <partition>
#	   -v              : be verbose
#	   -h              : print this help and exit
#
# Create and mount test volumes for each partition on the production servers
# for access time measurements and health checking.
#
# The volumes are mounted under /p/s/<server name>.<partition name>.
#
# Each mount point will contain a file called "OK" after creation, and a file
# called "accessTimes" after the first measurement.
#
# @(#) $Header: /afs/cern.ch/project/afs/dev/monitoring/console/accessTimes/src/perl/createTestVolumes.pl,v 1.17 2009/01/20 15:37:33 arne Exp arne $
#
# Initial version by Arne Wiebalck IT/FIO, 2007.
################################################################################


use Getopt::Std;
use lib '/afs/cern.ch/project/afs/perlmodules';
use Vos;
use strict;


#-------------------------------------------------------------------------------
#
# Data Structures, Constants, Globals
#
#-------------------------------------------------------------------------------


# the list of partitions for the servers in @prod_servers
#
my @prod_partitions;


# the list of production servers as from "afsconf.pl -S"
#
# if a single server name is given, the array contains only that name 
# (if "afsconf.pl -E" knows about it)
#
my @prod_servers;


# set vos and fs commands
#
my $vos = fullpath("vos", "/usr/afsws/bin:/usr/afsws/etc:/usr/sbin:/usr/bin:/usr/sue/bin/");
my $fs  = fullpath("fs", "/usr/afsws/bin:/usr/afsws/etc:/usr/sbin:/usr/sue/bin/:/bin/:/usr/bin/");

# the prefix of the volumes used to measure the performance
# (full name is q.afs.<server>.<partition>)
#
my $vol_prefix = 'q.afs'; 

# the prefix of the file to use
# (full name is /afs/cern.ch/project/s/<server_name>.<partition>)
#
my $rfile_prefix = '/afs/cern.ch/project/afs/s'; 
my $wfile_prefix = '/afs/.cern.ch/project/afs/s'; 

# the initial quota of the test volumes
#
my $maxquota = 5000;

# the verbosity level
#
my $verbosity = 0;

# the check flag
#
my $check_only = 0;


#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

# find an element in a list
#
sub in_array() {

	my ($_array, $elem ) = @_;

	my $tmp;
	foreach $tmp (@$_array) {

		return 1 if( $tmp eq $elem );
	}
	
	return 0;
}


# set the global array of partitions to probe
#
sub set_partitions() {

	foreach my $srv (@prod_servers) {
		my @Atmp = `$vos parti $srv`;
		foreach my $l (@Atmp) {
			my @Atmp2 = split /\s+/, $l;
			if ($Atmp2[4] =~ /vicep(\w+)/) {
				push @prod_partitions, "$srv/$1";
			} else {
				print "    No partition found in >$l<!\n" if (1 == $verbosity);			
			}
		}
	}
	return;
}


# set the global array of servers to probe
#
sub set_servers() {

	@prod_servers = split /\s+/, `/afs/cern.ch/project/afs/etc/afsconf.pl -S`;
	return;
}


# check the targets and create them if required
# (if the check_only flag is set, nothing is changed)
#
sub check_and_correct_targets() {

	print "Checking targets ...\n" if( 1 == $verbosity );
		
	my ($srv, $part);

	my $changed = 0;

	foreach my $sp (@prod_partitions) {
		
		($srv, $part) = split /\//, $sp;

		my $vol_name = "$vol_prefix.$srv.$part";
		my $vos_command = "$vos exa $vol_name 2> /dev/null";
		
		my @vos_output  = `$vos_command`;

		# check if the test volume exists
		if( $vos_output[0] =~ /$vol_name/ ) {
			
			print "    Vol for $sp exists.\n" if (1 == $verbosity);
			
		} else {
			
			print "==> Vol for $sp does not exist.\n";
			# create it
			my $create_cmd = "$vos create -server $srv -partition /vicep$part -name $vol_name -maxquota $maxquota";
			print "    >> $create_cmd \n" if (1 == $verbosity); 
			`$create_cmd` unless (1 == $check_only);
			
			$changed = 1;

			# redo the vos exa!
			@vos_output  = `$vos_command`;
		}				

		# check if the the test volume is on the right place
		if (($vos_output[1] =~ /\s(afs\d{2,3})\.cern.ch \/vicep(\w+)/) ||
		    ($vos_output[1] =~ /\s(afsdb\d)\.cern.ch \/vicep(\w+)/)) {
			
			if( ($1 eq $srv) && ($2 eq $part) ) {

				print "    Vol for $sp is in the right place [$1:$2].\n" if( 1 == $verbosity );

			} else {
			
				print "==> Vol for $sp is not in the right place.\n";
		
				# move it 
				my $move_cmd = "$vos move -id $vol_name -fromserver $1 -frompartition $2 -toserver $srv -topartition $part";
				print "    >> $move_cmd \n" if (1 == $verbosity); 
				`$move_cmd` unless (1 == $check_only);

				$changed = 1;
			}

		} else {

			# unlikely
			print "    Vol for $sp cannot be vos examin'ed (doesn't exist?): $vos_output[1] \n\n" if( 1 == $verbosity );
			next;
		}
			
		
		# check if the mountpoint exists ...
		my $mountp = "$rfile_prefix/$srv.$part";
		$! = '';
		my @stat_data = stat( $mountp );
		
		# ... and if so do a stat 
		if( ($! eq '') ) {
			
			print "    Stat to $mountp succeeded.\n" if (1 == $verbosity);
			
		} else {
			
			print "==> Stat to $mountp failed.\n" if (1 == $verbosity);
			# mount it (should be unlikely, only during creation)
			my $mount_cmd = "$fs mkmount -dir $wfile_prefix/$srv.$part -vol $vol_prefix.$srv.$part";
			print "    >> $mount_cmd\n" if (1 == $verbosity);
			`$mount_cmd` unless ($check_only == 1);

			my $setacl_cmd = "$fs setacl -dir $wfile_prefix/$srv.$part -acl system:afs rlidwka";
			print "    >> $setacl_cmd\n" if (1 == $verbosity);
			`$setacl_cmd` unless ($check_only == 1);;

			my $ok_cmd = "date > $wfile_prefix/$srv.$part/OK";
			`$ok_cmd` unless ($check_only == 1);

			$changed = 1;
		}
		
		print "\n" if( 1 == $verbosity );       
	}

	my $vos_rel = "$vos release p.afs.s";
	print "    >> $vos_rel \n" if( (1 == $verbosity) && (1 == $changed) && (1 != $check_only));
	`$vos_rel` if ((1 == $changed) && (1 != $check_only));

	return $changed;
}


# remove mountpoints and volumes
#
sub cleanup() {

	print "Cleaning up ...\n" if (1 == $verbosity);
		
	my ($srv, $part);

	my $changed = 0;

	print "--> @prod_partitions\n";

	foreach my $sp (@prod_partitions) {
		
		($srv, $part) = split /\//, $sp;

		my $vol_name = "$vol_prefix.$srv.$part";

		# rmmount it
		my $rmmount_cmd = "fs rmmount -dir $wfile_prefix/$srv.$part";
		print "    >> $rmmount_cmd\n" if (1 == $verbosity);
		`$rmmount_cmd` unless (1 == $check_only);
		$changed = 1;
				
		# check if the volume exists ...
		my $vos_command = "$vos exa $vol_name 2> /dev/null";		
		my @vos_output  = `$vos_command`;
		
		# ... and if so remove it
		if ($vos_output[0] =~ /$vol_name/) {
			
			print "    Vol for $sp exists.\n" if( 1 == $verbosity );
			# remove it
			my $remove_cmd = "$vos remove -id $vol_name";
			print "    >> $remove_cmd\n" if( 1 == $verbosity ); 
			`$remove_cmd` unless (1 == $check_only);
			$changed = 1;
			
		} else {
			
			print "    Vol for $sp does not exist.\n" if( 1 == $verbosity );
		}

		print "\n" if (1 == $verbosity);       
	}

	my $vos_rel = "$vos release p.afs.s";
	print "    >> $vos_rel \n" if((1 == $verbosity) && (1 == $changed) && (1 != $check_only));
        `$vos_rel` if ((1 == $changed) && (1 != $check_only));
}


sub prepare() {

	my ($server, $partition) = @_;

        if ($server) {
		@prod_servers = ($server);
	} else {
		&set_servers($server);
 	}	

	if ($partition) {
		my @Atmp = `$vos parti $server`;
		if (grep /vicep$partition/, @Atmp) {
                	@prod_partitions = ("$server/$partition");
		} else {
			print "    There is no partition $partition on $server!\n";
			exit 1;				
		}
        } else {
                &set_partitions();
        }

	exit(&check_and_correct_targets());
}


sub unprepare {

	my ($server, $partition) = @_;

	if ($server) {
		@prod_servers = ($server);
	} else {
		&set_servers($server);
	}

	if ($partition) {
		# no check as the partition may has gone meanwhile
		@prod_partitions = ("$server/$partition");
	} else {
		# if the partition is not specified, take ALL you can
		# find ...
		opendir(DIR, $rfile_prefix) || die "can't opendir $rfile_prefix: $!";
		while (my $e = readdir(DIR)) {
			if ($e =~ /^$server\.([a-z]{1,2})$/) {
				push @prod_partitions, "$server/$1";
			}
		}
		closedir DIR;
	}

	&cleanup();
}


sub usage {
	
	print "Available options\n";
	print "  -c              : check only, take no actions\n";
	print "  -s              : prepare all servers\n";
	print "  -S <servername> : prepare server <servername>\n";
	print "  -u              : unprepare all servers\n";
	print "  -U <servername> : unprepare server <servername>\n";
	print "  -P <partition>  : pick only partition <partition>\n";
	print "  -v              : be verbose\n";
	print "  -h              : print this help and exit\n";
}


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------


# Argument parsing
#
my $argc = @ARGV;
if( 0 == $argc) {

	&usage;
	exit();
}

my %argv = ();
getopts("csS:uU:vhP:", \%argv );

die "Unknown option: $ARGV[0]. Try -h for a list of available options." 
    if( @ARGV > 0 );

if( $argv{h} ) {

	&usage;
	exit();
}

my $server = '';
my $partition = '';

# option -s (prepare all servers)
if ($argv{s}) {

	$server = '';
}

# is a server selected? (overrules -s)
if ($argv{S}) {

	$server = $argv{S};
}

# option -u (unprepare all servers)
if ($argv{u}) {

	$server = '';
}

# is a server selected? (overrules -u)
if ($argv{U}) {

	$server = $argv{U};
}

# get the partition
if ($argv{P}) {

	if ($argv{u} || $argv{s}) {
		print ">>\'-P\' and (\'-u\' or \'-s\') is not possible at the same time.<<\n" ;
		exit();
	}
	$partition = $argv{P};
}

# set the verbosity level
if ($argv{v}) {

	$verbosity = 1;
}

# set the check flag
if ($argv{c}) {

	print ">> CHECK ONLY <<\n" if (1 == $verbosity);
	$check_only = 1;
}

if( ($argv{S} || $argv{s}) && ($argv{U} || $argv{u}) ) {

	print ">>\'prepare\' and \'unprepare\' is not possible at the same time.<<\n";
	&usage;
	exit();
}


if ( $argv{S} || $argv{s} ) { 
	 
	&prepare($server, $partition);

} elsif( $argv{U} || $argv{u} ) { 
	
	&unprepare($server, $partition);

} else {

	print "Don't know what to do ... did you specify which servers to process?\n";
}
