DIRS = afsconsole-common afsconsole-mysql afsconsole-sensors afsconsole-web accessTimes
TARGETS = all clean sources rpm srpm

$(TARGETS):
	for d in $(DIRS); do make -C $$d $@; done
