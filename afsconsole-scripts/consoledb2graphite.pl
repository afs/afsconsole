#!/usr/bin/perl
#
# extract collected info from the AFS consoleDB and stuff it into Graphite
# run me ~once/day (evening, to get the maximum totalAccess count before reset)

use strict;
use warnings;
use POSIX;
use Getopt::Long;
use Date::Parse;
use IO::Socket::INET;
use DBI;
use FindBin;
use lib "$FindBin::Bin/../afsconsole-web/modules"; # better than from some random location on AFS

#database connection
use ConsoleMySQL;

my $carbonservers;
my $carbonprefix='afs.';
my @carbonsocks;
my $timestamp = time();
my $dbh;
my ($opt_debug, $opt_help, $starttime, $endtime);
if ((! GetOptions("debug" => \$opt_debug,
		  "starttime=s" => \$starttime,
		  "endtime=s" => \$endtime,
		  "carbonservers=s" => \$carbonservers,
		  "help" => \$opt_help,
     ))
     or ! $carbonservers
     or $opt_help) {
    print STDERR <<EOFusage;
$0: [--debug] [--starttime TIME1] [--endtime TIME2] --carbonservers=HOST1:PORT1[,HOST2:PORT2,..]
extracts AFS monitoring info from ConsoleDB and sends it to Carbon/Graphite/Grafana.

Optional TIME1, TIME2 restrict which data gets sent (and cause massive slowdowns, and some queries will not yet work).
Default is to send the most recent data.

Debug mode: data goes to different tree under "test.", and gets printed.
EOFusage
   exit 1;
}

if ($starttime) {
	my $temp = str2time($starttime);
	if ($temp > 0 ) {
	   $starttime = $temp;
        } else {
           die "Cannot parse starttime '$starttime'";
        }
}
if ($endtime) {
	my $temp = str2time($endtime);
	if ($temp > 0 ) {
	   $endtime = $temp;
        } else {
           die "Cannot parse endtime '$endtime'";
        }
}
$carbonprefix='test.afs.' if($opt_debug);




sub sendcarbon {
  my ($string,$timevalues) = @_;  # fixed string, with arrayref of arreyrefs (t,v)
  if (ref($timevalues) ne 'ARRAY') {
      $timevalues=[[$timestamp,$timevalues]];
  }
  for my $tv (@{ $timevalues }) {
      my ($t,$v) = @{ $tv };
      print "#DEBUG ".$carbonprefix.$string.' '.$v.' '.$t."\n" if($opt_debug);
      for my $sock (@carbonsocks) {
          $sock->send($carbonprefix.$string.' '.$v.' '.$t."\n");
      }
  }
}


sub get_project_volset_totals {  # blatant cut+paste to get per-project breakdown
    my $wheretime="";
    if($starttime or $endtime) {  # will return several values and respective timestamps
	if ($starttime) {
	    $wheretime .= " AND timestamp >= $starttime";
	}
	if($endtime) {
	    $wheretime .= " AND timestamp <= $endtime";
	}
     } else {
        $wheretime = " AND recent=1";
     }
     my (%time_count,%time_size,%time_quota,%time_files); # per-project arrays of [time,values]
     for my $rowref ( @{ $dbh->selectall_arrayref("
       SELECT project,timestamp,count(*),sum(size),sum(quota),sum(files)
         FROM volumes
        WHERE volumename IS NOT NULL
              $wheretime
        GROUP BY project,timestamp" ) }) {
	    my 	($project,$time,$count,$size,$quota,$files) = @{ $rowref };

            push(@{ $time_count{$project} },[$time, $count]);
	    push(@{ $time_size{$project} }, [$time, $size]);
	    push(@{ $time_quota{$project} },[$time, $quota]);
	    push(@{ $time_files{$project} },[$time, $files]);
      }
      return (\%time_count,\%time_size,\%time_quota,\%time_files);
}

sub get_volset_totals {
    my $pattern = shift;
    my $whereclause = shift;

    if (! $whereclause ) {
	$whereclause = "volumename like ?";
    } else {
	$whereclause .= " AND volumename IS NOT ?"; # need to keep same no. of bind vars
	$pattern = undef;
    }
    if($starttime or $endtime) {  # will return several values and respective timestamps
	my $wheretime="";
	if ($starttime) {
	    $wheretime .= " AND timestamp >= $starttime";
	}
	if($endtime) {
	    $wheretime .= " AND timestamp <= $endtime";
	}
	my (@time_count, @time_size, @time_quota, @time_files);
	for my $rowref ( @{ $dbh->selectall_arrayref("
       SELECT timestamp,count(*),sum(size),sum(quota),sum(files)
         FROM volumes
        WHERE $whereclause
              $wheretime
        GROUP BY timestamp" ,{},$pattern) }) {
	    my 	($time,$count,$size,$quota,$files) = @{ $rowref };
	    push(@time_count,[$time, $count]);
	    push(@time_size, [$time, $size]);
	    push(@time_quota,[$time, $quota]);
	    push(@time_files,[$time, $files]);
	}
	return (\@time_count,\@time_size,\@time_quota,\@time_files);

    } else {  # single values only
	my ($c,$s,$q,$f) = $dbh->selectrow_array("
       SELECT count(*),sum(size),sum(quota),sum(files)
         FROM volumes
        WHERE recent=1
          AND $whereclause",{},$pattern);
	return ($c,$s,$q,$f);
    }
}

# test/open DB
$dbh = db_connect() or die "no connection to ConsoleDB\n";

# test/open Graphite
for my $carbonserver (split(',', $carbonservers)) {
    my $sock = IO::Socket::INET->new(PeerAddr => $carbonserver) or # auto-handles host:port, timeout is useless
        die("cannot create Carbon socket to $carbonserver:$!");
    push(@carbonsocks, $sock);
}

my ($count,$size,$quota,$files)=(-1,-1,-1,-1);

print ("#DEBUG: volumes.total\n") if $opt_debug;
($count,$size,$quota,$files) = get_volset_totals('%');
sendcarbon("volumes.total.count",$count);
sendcarbon("volumes.total.size",$size);
sendcarbon("volumes.total.quota",$quota);
sendcarbon("volumes.total.files",$files);

VOL_USER:
print ("#DEBUG: volumes.user\n") if $opt_debug;
($count,$size,$quota,$files) = get_volset_totals('user.%');
sendcarbon("volumes.user.count",$count);
sendcarbon("users.count",$count); # not exactly the same as PTS entries, but close 
sendcarbon("volumes.user.size",$size);
sendcarbon("users.used",$size); # old alias. 
sendcarbon("volumes.user.quota",$quota);
sendcarbon("users.quota",$quota); # old alias. 
sendcarbon("volumes.user.files",$files);

VOL_WORK:
print ("#DEBUG: volumes.work\n") if $opt_debug;
($count,$size,$quota,$files) = get_volset_totals('work.%');
sendcarbon("volumes.work.count",$count);
sendcarbon("volumes.work.size",$size);
sendcarbon("volumes.work.quota",$quota);
sendcarbon("volumes.work.files",$files);

VOL_DELUSER:
print ("#DEBUG: volumes.del_user\n") if $opt_debug;
($count,$size,$quota,$files) = get_volset_totals('Y.user.%');
## since Aug 2017, personal work.* volumes are not deleted anymore - tag them as such
my ($count_w,$size_w,$quota_w,$files_w) = get_volset_totals('Y.work.%');
sendcarbon("volumes.del_user.count",$count + $count_w);
sendcarbon("volumes.del_user.size",$size + $size_w);
sendcarbon("volumes.del_user.quota",$quota + $quota_w);
sendcarbon("volumes.del_user.files",$files + $files_w);

VOL_PROJ:
print ("#DEBUG: volumes.projects\n") if $opt_debug;
($count,$size,$quota,$files) = get_volset_totals(undef,"
    (volumename LIKE 'p.%' OR
     volumename LIKE 'q.%' OR
     volumename LIKE 'u.%' OR
     volumename LIKE 's.%')");
sendcarbon("volumes.project.count",$count);
sendcarbon("volumes.project.size",$size);
sendcarbon("volumes.project.quota",$quota);
sendcarbon("volumes.project.files",$files);

VOL_OTHER:
print ("#DEBUG: volumes.other\n") if $opt_debug;
($count,$size,$quota,$files) = get_volset_totals(undef,"
        (volumename NOT LIKE 'user.%' AND
         volumename NOT LIKE 'work.%' AND
         volumename NOT LIKE 'p.%' AND
         volumename NOT LIKE 'q.%' AND
         volumename NOT LIKE 's.%' AND
         volumename NOT LIKE 'u.%' AND
         volumename NOT LIKE 'Y.user.%' AND
         volumename NOT LIKE 'Y.work.%')");
sendcarbon("volumes.other.count",$count);
sendcarbon("volumes.other.size",$size);
sendcarbon("volumes.other.quota",$quota);
sendcarbon("volumes.other.files",$files);


VOL_DETAIL:
# split by DB 'project', does not match 100% the above
print ("#DEBUG: volumes.detailed\n") if $opt_debug;
my ($p_count,$p_size,$p_quota,$p_files) = get_project_volset_totals();
for my $p (keys(%{$p_count})) {
      my $name = $p; $name =~ s/[^a-zA-Z0-9]/_/g;
      sendcarbon("volumes.detailed.".$name.".count",$$p_count{$p});
}
for my $p (keys(%{$p_size})) {
      my $name = $p; $name =~ s/[^a-zA-Z0-9]/_/g;
      sendcarbon("volumes.detailed.".$name.".size",$$p_size{$p});
}
for my $p (keys(%{$p_quota})) {
      my $name = $p; $name =~ s/[^a-zA-Z0-9]/_/g;
      sendcarbon("volumes.detailed.".$name.".quota",$$p_quota{$p});
}
for my $p (keys(%{$p_files})) {
      my $name = $p; $name =~ s/[^a-zA-Z0-9]/_/g;
      sendcarbon("volumes.detailed.".$name.".files",$$p_files{$p});
}

PROJECT_COUNT:
# track number of diff. projects
print ("#DEBUG: project.count\n") if $opt_debug;
if($starttime or $endtime) {  # will return several values and respective timestamps
  my $wheretime = "";
  if ($starttime) {
    $wheretime .= " AND timestamp >= $starttime";
  }
  if($endtime) {
    $wheretime .= " AND timestamp <= $endtime";
  }
  my @time_count;
  for my $rowref ( @{ $dbh->selectall_arrayref("
       SELECT timestamp,count(distinct(project))
         FROM volumes
        WHERE volumename IS NOT NULL
              $wheretime
        GROUP BY timestamp") }) {
     my 	($time,$count) = @{ $rowref };
     push(@time_count,[$time, $count]);
  }
  $count = \@time_count;
} else {
  ($count) = $dbh->selectrow_array("select count(distinct(project)) from volumes where recent=1");
}
sendcarbon("projects.count",$count);

# FIXME below not yet updated for time ranges..
if($starttime or $endtime) {
    die "bailing out: some queries do not yet work with time ranges";
}
USER_COUNT:
# some measure for "active" users
print ("#DEBUG: active users\n") if $opt_debug;
($count) = $dbh->selectrow_array("SELECT count(*) FROM volumes
    WHERE recent=1 AND
          volumename LIKE 'user.%' AND
     (totalWrites > 0 OR
      totalReads > 100 OR
      totalAccesses > 1000)");
sendcarbon("users.active1d",$count);

for my $sock (@carbonsocks) {
    $sock->close();
}
1;
