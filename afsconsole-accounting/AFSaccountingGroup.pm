#!/usr/bin/perl
#
# readonly access to translate usernames to accounting groups
# see https://its.cern.ch/jira/browse/CRM-3401

package AFSaccountingGroup;

use strict;
use warnings;

use LWP::UserAgent qw( );
use HTTP::Request::Common;
use JSON;

$::debug = 0;

use AFSaccountingGroupConf;

my $ua = LWP::UserAgent->new();

$ua->default_header( 'Content-Type' => 'application/json');

# debug, replaces LWP::Debug :-(
if($::debug) {
  $ua->add_handler("request_send",  sub { print STDERR shift->dump(); return });
  $ua->add_handler("response_done", sub { print STDERR shift->dump(); return });
}

sub bulk_translate2CG {
    my $hashref = {};
    my $request = HTTP::Request::Common::GET( $AFSaccountingGroupConf::api_bulk_resolve_url,
					      Content_Type => 'application/json' );
    my $response = $ua->request($request);
    if( ! $response->is_success) {
	warn("accounting service failed: ".$response->status_line);
	if ($::debug and $response->is_error) {
	    print STDERR $response->error_as_HTML."\n";
	}
	return $hashref;
    }
    my $content  = $response->decoded_content();
    eval {
	$hashref = from_json ($content);
    };
    if ($@) {
	warn "cannot decode JSON:".$@;
	return $hashref;
    }
    if (exists($$hashref{'anonymous'}) and $$hashref{'anonymous'} eq '') {
	$$hashref{'anonymous'} = 'ANONYMOUS';
    }
    return $hashref;
}

# take an array of usernames, returns a hashref that maps each user to the charging group
sub translate2CG {
    my $hashref = {};
    my @my_users = @_;
    my $all_accountsref = bulk_translate2CG();
    #
    foreach my $user (@my_users) {
	$$hashref{$user} = $$all_accountsref{$user};
    }
    return $hashref;
}

1;
