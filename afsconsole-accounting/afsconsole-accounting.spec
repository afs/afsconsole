Name: afsconsole-accounting
Version: 1.04
Release: 1
Summary: AFS Console accounting interface
Group: Filesystems
License: GPL
BuildArch: noarch
Vendor: CERN
Packager: Jan Iven <jan.iven@cern.ch>
URL: https://gitlab.cern.ch/afs/afsconsole.git
## not yet packaged properly
%global __requires_exclude  perl\\(Volset\\)

%description
'IT accounting' interface for the AFS Console. Should run as daily cronjob.
Relies on several 'Conf' perl modules for accounting API and DB access.

#install - see Makefile

%files
%{_bindir}/*.pl
%{perl_sitelib}/AFSaccountingGroup.pm
%{perl_sitelib}/AFSaccountingSubmit.pm
%config(noreplace) %{perl_sitelib}/AFSaccountingGroupConf.pm
%config(noreplace) %{perl_sitelib}/AFSaccountingSubmitConf.pm


