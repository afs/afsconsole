#!/usr/bin/perl
#
# provide some accounting of the AFS space, broken down by ChargeUnit (Department/Experiment)

# Format still somewhat unclear
# projects:
# ChargeUnit,ProjectName,ProjectQuota (i.e 'pledge', could be used without admin intervention),SumOfQuotas,SumOfUsage
#
# users:
# ChargeUnit,Account,SumOfQuotas,SumOfUsage
# can tie together via a 'Type' field. 'pledge' should be 110GB (max self-service user+work quotas)
# should we link acounts back to 'real person'? No. Actually, try to not talk about users at all.

use strict;
use warnings;
use Data::Dumper;
use JSON;
use POSIX;
use DBI;
use Getopt::Long;
use FindBin;
use List::Util qw(min);
use lib "$FindBin::Bin";
use lib qw(/opt/perlmodules/);
use Volset;
# Set lib after Volset to ensure that its include (/afs/cern...) is taken last
# Otherwise we get the wrong AFSConsoleConf
use lib qw(/usr/share/afsconsole/common/ /usr/local/share/perl5/);
use AFSConsoleConf;
use AFSaccountingSubmit;
use AFSaccountingGroup;

my $dsn = 'DBI:'.$AFSConsoleConf::db_backend.':database='.$AFSConsoleConf::db_name.';host='.$AFSConsoleConf::db_host;
my $dbh = DBI->connect($dsn, $AFSConsoleConf::db_ro_username, $AFSConsoleConf::db_ro_passwd) or die;


# need 'yesterday' as YYYY-MM-DD
my @timestamp=localtime; --$timestamp[3];
my $reportdate = POSIX::strftime("%Y-%m-%d", @timestamp);

my ($opt_verbose, $opt_help, $opt_nowarn, $opt_doprojects, $opt_dousers, $opt_format, $opt_submit);
$opt_doprojects=0;
$opt_dousers=0;
$::debug = 0;

# assumptions on CERN volume type quotas
my $user_maxquota = 10E9;
my $work_maxquota = 100E9;

# how many non-resolving accounts to print
my $n_unresolved = 10;

sub usage {
    print STDERR <<EOFusage;
$0 [--debug] [--nowarn] (--format=(csv|json_v2|json_v3)|--submit)  (--projects|--users)

provide some accounting of the AFS space, broken down by ChargeUnit (Department/Experiment).
Usage data is from ConsoleDB.
Will report on
* actual used space (sum of AFS volume usage)
* reserved quota (sum of AFS volume quotas)
* pledge (project quotas, maximum self-service quota for user+work volumes)

Project -> CG comes from tags in /p/adm/afsadmin.cf.
User    -> CG comes from 'accouting-receiver' webservice.

One of '--format' or '--submit' arguments needs to be specified.
One or both of '--projects' '--users'  needs to be specified.

By default will
* account for project space and user/work areas,
* print some warnings (empty projects, non-resolving users)
EOFusage
}

if (
    ! GetOptions(
	  "help" => \$opt_help,
	  "format=s" => \$opt_format,
	  "projects" => \$opt_doprojects,
	  "users" => \$opt_dousers,
	  "submit" => \$opt_submit,
	  "nowarn" => \$opt_nowarn,
	  "debug" => \$::debug,
    ) or $opt_help
    or ! ($opt_doprojects or $opt_dousers)
    or (!$opt_submit and (!$opt_format or $opt_format !~ m/^csv$|^json_v2$|^json_v3$/))
    ){
    usage();
    exit(1);
};


### projects == 'Volsets'. OUTER loop is volsets, but aggegrate sub-volsets beforehand
sub account_by_topvolset {
    my %volsets = get_volsets();
    my %top_volsets;
    my @accounting;

    for my $s (sort(keys(%volsets))) {
	next if $s =~ m/^__/; # skip weird internal placeholders

	# different 'quality' sub-projects with own quota: sum these
	# up into the parent (on the assumption that this exists!)
	next if $s =~ m/_[qpsu]$/;

	# mismatch between afsadmin.cf (has volumes)
	# and ConsoleDB (which has these listed some of these under 'n.a.')
	next if $s =~ m/^(obsolete|recover|std|users|project|work|sys|project_user|asis)$/;

	# booked as 'atlas_p' in ConsoleDB, but with own project quota. Sum up to 'atlas'
	next if $s =~ m/^atlasT0$/;
	
	my $projectquota = volset_quota ( $s );
	my $projectquota_num = 0;
	if($projectquota) {
	    $projectquota_num = decode_quota($projectquota);
	}
	my $chargegroup = volsetopts('accounting', $s);
	my $chargerole;
	if ($chargegroup and $chargegroup =~ m/^Department:IT-(\w+)/ ) {
	    $chargerole = "AFS project '$s' for IT-$1";
	    $chargegroup = 'IT';
	} elsif ($chargegroup and $chargegroup =~ m/^(Experiment|Department):(\w+)/ ) {
	    # accounting API does not make this distiction yet anyway, and neither handles IT-group
	    $chargerole = "AFS project '$s'";
	    $chargegroup = $2;
	} elsif ($chargegroup and $chargegroup =~ m/^Service:(\S+)/ ) {
	    $chargegroup = $1;
	    $chargegroup =~ s/_/ /g; # FEs can have whitespace
	    $chargerole = "AFS project '$s'";
	} elsif ($chargegroup) {
	    $chargerole = "AFS project '$s'";
	    $chargegroup = $chargegroup;
	} else {
	    print STDERR "#WARN: no 'accounting' defined for $s\n" unless $opt_nowarn;
	    $chargerole = "AFS project '$s' - unaccounted";
	    $chargegroup = 'AFS';  # book on us until we've figured out who is behind this
	}
	$top_volsets{$s}{'quota'} += $projectquota_num;
	$top_volsets{$s}{'chargegroup'} = $chargegroup;
	$top_volsets{$s}{'chargerole'}=$chargerole;
    }

    # note: backups don't take extra space - should not be in DB, but exclude anyway.
    #       readonly replicas might take space (beyond first..) - include if in DB.
	
    # ignore split by 'quality' here
    my $query= $dbh->prepare("SELECT COALESCE(SUM(size),0),
                                 COALESCE(SUM(quota),0)
                              FROM volumes
			      WHERE RECENT=1
                                AND project=?
				AND volumename NOT LIKE '%.backup'
			        AND volumename NOT LIKE 'work.%'");

    for my $s (keys(%top_volsets)) {
	my ($sum_size, $sum_quotas);
	$query->execute($s);
	($sum_size, $sum_quotas) = $query->fetchrow_array();
	
	my $projectquota_num = $top_volsets{$s}{'quota'};
	my $chargegroup = $top_volsets{$s}{'chargegroup'};
	my $chargerole = $top_volsets{$s}{'chargerole'};
	if( $projectquota_num or $sum_quotas or $sum_size ) {
	    push (@accounting, [
		      $chargegroup,
		      $chargerole,
		      $projectquota_num,
		      $sum_quotas,
		      $sum_size
		  ]);
	}
	if ($sum_quotas == 0 and  $sum_size == 0) {
	    print STDERR "#WARN: may need to investigate/clean up empty project '$s'\n" unless $opt_nowarn;
	}
    }
    return @accounting;
}



# user and work volumes are not linked to some meaningful project
# need to get list, derive owning account from volumename, look up CG (bulk!),
# and manually aggregate per CG..
# note: will avoid to expose individual accountnames (unless this is required upstream)
# note: 'user.readonly' exists but is part of the service - no other user+work vol should have readonly replicas.
#        backup volumes seem not to be in the DB, but exclude them anyway.
sub account_by_users {

    my $query= $dbh->prepare("SELECT volumename, MAX(size) AS size, MAX(quota) AS quota
                              FROM volumes
                              WHERE RECENT=1
                                    AND ( volumename LIKE 'user.%' OR volumename LIKE 'work.%' )
				    AND volumename NOT LIKE '%.readonly'
				    AND volumename NOT LIKE '%.backup'
                              GROUP BY volumename");  # just need one entry.. just in case 'recent' does not make this unique
    $query->execute();
    my $hash_ref = $query->fetchall_hashref(1);  # 1== volumename, i.e use $hash_ref{'work.iven'}->size

	my @volumes = keys %{$hash_ref};
	my %accounts = map { /^(user|work)\.(.*)/ ? ($2 => 1) : () } @volumes;
    my @unique_accounts = keys %accounts;

    my $user2cg = AFSaccountingGroup::translate2CG( @unique_accounts );

    my %cgs;
    my @unresolved;
    for my $user ( @unique_accounts ) {
        my $cghash = $$user2cg{$user};  # v2 API returns {'owner' => 'iven','charge_group' => 'IT','type' => ' department'}
	my $cg;
	if($cghash) {
	    if (ref($cghash) eq 'HASH') {  # can also return "no information was found" :-(
		$cg = $$cghash{'charge_group'};
	    }
	}
	unless ( $cg ) {
	    $cg = 'unresolved';
	    push ( @unresolved, $user );
	}

	if(! $cgs{$cg}) {
	    # first time we see this?
	    $cgs{$cg} = {
		user => {size => 0, quota => 0, vols => 0},
		work => {size => 0, quota => 0, vols => 0},
	    };
	}
	my $uservol = 'user.'.$user;
	if ($$hash_ref{$uservol}) {
	    $cgs{$cg}->{user}->{size} += $$hash_ref{$uservol}->{size};
	    $cgs{$cg}->{user}->{quota} += $$hash_ref{$uservol}->{quota};
	    $cgs{$cg}->{user}->{vols}++;
	}
	my $workvol = 'work.'.$user;
	if($$hash_ref{$workvol}) {
	    $cgs{$cg}->{work}->{size} += $$hash_ref{$workvol}->{size};
	    $cgs{$cg}->{work}->{quota} += $$hash_ref{$workvol}->{quota};
	    $cgs{$cg}->{work}->{vols}++;
	}
    }

    my @accounting;
    for my $cg (sort(keys(%cgs))) {
	push (@accounting, [ $cg,
			     'AFS uservolumes',
			     $user_maxquota * $cgs{$cg}->{user}->{vols},
			     $cgs{$cg}->{user}->{quota},
			     $cgs{$cg}->{user}->{size}
	      ]);
    }
    for my $cg (sort(keys(%cgs))) {
	push (@accounting, [ $cg,
			     'AFS workvolumes',
			     $work_maxquota * $cgs{$cg}->{work}->{vols},
			     $cgs{$cg}->{work}->{quota},
			     $cgs{$cg}->{work}->{size}
	      ]);
    }

    if ( @unresolved and not $opt_nowarn ) {
	print STDERR "#WARN: could not CG-resolve ";
	print STDERR $cgs{unresolved}->{user}->{vols}." user volumes, ";
	print STDERR $cgs{unresolved}->{work}->{vols}." work volumes, ";
	print STDERR "for accounts ".join(', ', @unresolved[0 .. min($#unresolved, $n_unresolved)]);
	if ($#unresolved >= $n_unresolved) {
	    print  STDERR ',...';
	}
	print STDERR "\n";
    }
    return @accounting;
}

my @accounting;
if ($opt_dousers) {
    push (@accounting, account_by_users());
}
if ($opt_doprojects) {
    push (@accounting, account_by_topvolset());
}

if ($opt_format =~ m/^json/ ) {
    AFSaccountingSubmit::format_and_submit($opt_format, $reportdate, $opt_submit, \@accounting );
} else { # ($opt_format eq 'csv')
    print '#'.join(',', ('maxquota', 'quota', 'used'))."\n";
    for my $item (@accounting) {
	print join(',', @$item)."\n";
    }
}
