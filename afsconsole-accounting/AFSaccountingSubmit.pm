#!/usr/bin/perl
#
# https://cern.ch/accounting-docs for the endpoint and JSON format(s)

package AFSaccountingSubmit;

use strict;
use warnings;

use LWP::UserAgent qw( );
use HTTP::Request::Common;
use JSON;

$::debug = 0;
    
use AFSaccountingSubmitConf;

my $this_fe = 'AFS';

my $ua = LWP::UserAgent->new();

$ua->default_header( 'Content-Type' => 'application/json');
$ua->default_header( 'API-Key' => $AFSaccountingSubmitConf::api_key );

# debug, replaces LWP::Debug :-(
if($::debug) {
  $ua->add_handler("request_send",  sub { print STDERR shift->dump(); return });
  $ua->add_handler("response_done", sub { print STDERR shift->dump(); return });
}

sub get_v2_records {
    my $accounting = shift;
    my $reportdate = shift;
    my $kb2byte = 1024;  # all of AFS reports in kB, also stored in DB.
    my @records;
    for my $item (@$accounting) {
	my $record = {
	    MessageFormatVersion => 2,
	    Date => $reportdate,
	    FE => $this_fe,
	    ChargeGroup => $$item[0],
	    ChargeRole => $$item[1],
	    #Pledge => $$item[2],
	    DiskQuota => $$item[3] * $kb2byte,
	    DiskUsage => $$item[4] * $kb2byte,
	    Dedicated => JSON::true,
	    WallClockHours => 0.0,
	    CPUHours => 0.0,
	};
	push (@records, $record);

    }
    return \@records;
}

sub get_v3_records {
    my $accounting = shift;
    my $reportdate = shift;
    my $kb2byte = 1024;  # all of AFS reports in kB, also stored in DB.
    my @records;
    for my $item (@$accounting) {
	my $record = {
	    ToChargeGroup => $$item[0],
	    ToChargeRole => $$item[1],
	    MetricValue	=> $$item[3] * $kb2byte,
	    #Pledge => $$item[2],
	    #DiskQuota => $$item[3] * $kb2byte,
	    #DiskUsage => $$item[4] * $kb2byte,
	};
	push (@records, $record);

    }
    my $full_record = {
	MessageFormatVersion => 3,
	TimePeriod => 'day',
	TimeStamp => $reportdate,
	FromChargeGroup => $this_fe,
	MetricName => 'AFSQuotaBytes',
	TimeAggregate => 'avg',
	AccountingDoc => 'https://afs-service-docs.web.cern.ch/monitoring/accounting/',  # i.e 'quota bytes', not 'used bytes'
	data =>  \@records,
    };
    return $full_record;
}

sub format_and_submit {
    my ($format, $for_date, $actually_submit, $dataref) = @_;
    my $records;
    if ($format eq 'json_v2') {
	$records = get_v2_records($dataref, $for_date);
    } elsif ($format eq 'json_v3') {
	$records = get_v3_records($dataref, $for_date);
    } else {
	die "wot? wrong JSON format";
    }

    my $jsonout  = to_json ( $records );
    if ($actually_submit) {
	my $submit_url = 'define_me';
	if ($format eq 'json_v2') {
	    $submit_url = $AFSaccountingSubmitConf::api_submit_endpoint . '/v2/fe/'.$this_fe;
	} elsif ($format eq 'json_v3') {
	    $submit_url = $AFSaccountingSubmitConf::api_submit_endpoint . '/v3/fe';
	}
	my $request = HTTP::Request::Common::POST( $submit_url,
						   Content_Type => 'application/json',
						   Content => $jsonout );
	my $response = $ua->request($request);
	if( ! $response->is_success) {
	    warn("accounting service failed: ".$response->status_line."\n".$response->content);
	}
    } else {
	print $jsonout."\n";
    }
}

1;
