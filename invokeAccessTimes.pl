#!/usr/bin/perl


################################################################################
#
# invokeAccessTimes.pl
#
# Get the local partitions of a server and invoke the access time measurement.
#
# Initial version by Arne Wiebalck IT/FIO, 2007.
#
# @(#) $Header: /afs/cern.ch/project/afs/dev/monitoring/console_Lemon/accessTimes/src/perl/invokeAccessTimes.pl,v 1.4 2009/09/29 10:58:33 wiebalck Exp wiebalck $
#
################################################################################


use strict;
use Getopt::Std;

#-------------------------------------------------------------------------------
# Globals
#-------------------------------------------------------------------------------

my $maxrc = 0;
my $path  = '/afs/cern.ch/project/afs/var/console/accessTimes/';

#-------------------------------------------------------------------------------
# Subroutines
#-------------------------------------------------------------------------------

sub error_msg {
	
	my ($m,$rc) = @_;
	if ($rc > $maxrc) { $maxrc=$rc; }	
	print STDERR $m, "\n";
}

sub usage {
	
	print "Available options\n";
        print "  -S <host>       : use <host> instead of localhost\n";
	print "  -v              : be verbose\n";
	print "  -h              : print this help and exit\n";
}

sub today {

	my ($sec, $min, $hrs, $day, $mon,
	    $yr, $dow, $doy, $dst) = localtime(time);
	
	if ($day<10) {
		$day = "0" . $day;
	}

	$mon++;
	if ($mon<10) {
		$mon = "0" . $mon;
	}
	
	$yr += 1900;

	return "$yr-$mon-$day";
}

#-------------------------------------------------------------------------------
# Main
#-------------------------------------------------------------------------------

# argument parsing
#
my %argv = ();
getopts("S:hv", \%argv );

# help
if ($argv{h}) {
	&usage();
}


# check the C executable
#
my $x = '/afs/cern.ch/project/afs/tools/accessTimes';
if (!(-x $x)) {
	
	&error_msg( "$x not found/not executable", 8 );
	exit( $maxrc );
}


# determine the local hostname
#
my $host = '';
if ($argv{S} =~ /afs\d{2,3}/) {
	$host = $argv{S};
} else {
	chomp($host = `uname -n`);
	$host = $1 if ($host =~ /(\S+).cern.ch/);
}

# get the partition names and exit if none found
#
my @pp = split /\s+/, `/afs/cern.ch/project/afs/etc/afsconf.pl -P`;
my @lp = ();

foreach my $sp (@pp) {
	
	if ($sp =~ /$host\//) {

		my ($s, $p) = split /\//, $sp;		
		push @lp, $p;
	}
}
my $len = @lp;
exit( $maxrc ) unless ($len > 0);

# invoke accessTimes
#
my $cmd = "$x $host " . join(" ", @lp); 
my $chk = `$cmd 2>&1`;
my $rc  = $?;

if ($rc) { &error_msg( "Access problems on afs server $host ($cmd): \n$chk", 8 ); }

# be verbose?
if ($argv{v}) {

	# determine the results' file name
	my $rfile = $path . $host . "." . &today() . ".dat";	

	open FILE, $rfile || die "Unable to open result file: $!";
	my @a = ();
	while (<FILE>) {
		if ($_ =~ /^\#/) {
			print $_;
			next;
		}		
		push @a, $_;		
	}
	close FILE;
	
	# print the newest results
	my ($f, $l);
	$f = (scalar @a) - (2 * $len);
	$l = (scalar @a);

	for ($f..$l) { 
		
		print $a[$_]; 
	}
}

exit( $maxrc );
