Name: afsconsole-mysql
Version: 1.00
Release: 3
Summary: AFS Console MySQL Setup
Group: Filesystems
License: GPL
Requires: gcc, mysql-devel, mysql-server

%description
Sets up a MySQL DB for use with the AFS Console

%files
/usr/share/afsconsole/mysql/setup.pl
/usr/share/afsconsole/mysql/my.cnf

%post
echo "Please run /usr/share/afsconsole/mysql/setup.pl to complete the installation."

