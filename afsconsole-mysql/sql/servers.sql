CREATE TABLE `servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nodename` varchar(256) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL,
  `server` varchar(64) DEFAULT NULL,
  `idleThreads` int(10) unsigned NOT NULL,
  `serverConnections` int(10) unsigned NOT NULL,
  `clientConnections` int(10) unsigned NOT NULL,
  `waitedCalls` int(10) DEFAULT NULL,
  `udpInDatagrams` bigint(20) unsigned NOT NULL,
  `udpNoPorts` bigint(20) unsigned NOT NULL,
  `udpInErrors` bigint(20) unsigned NOT NULL,
  `udpOutDatagrams` bigint(20) unsigned NOT NULL,
  `recent` tinyint(1) NOT NULL DEFAULT '0',
  `avgd` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `recent` (`recent`,`server`)
) ENGINE=InnoDB;

DROP PROCEDURE IF EXISTS addserversample;
DELIMITER //
CREATE PROCEDURE `addserversample`(IN t_nodename VARCHAR(256),
                                  IN t_timestamp INTEGER UNSIGNED,
                                  IN t_server VARCHAR(64),
                                  IN t_idleThreads INTEGER UNSIGNED,
                                  IN t_serverConnections INTEGER UNSIGNED,
                                  IN t_clientConnections INTEGER UNSIGNED,
                                  IN t_waitedCalls INTEGER,
                                  IN t_udpInDatagrams BIGINT UNSIGNED,
                                  IN t_udpNoPorts BIGINT UNSIGNED,
                                  IN t_udpInErrors BIGINT UNSIGNED,
                                  IN t_udpOutDatagrams BIGINT UNSIGNED)
BEGIN
    UPDATE servers SET recent=0 WHERE server=t_server AND recent=1;
    INSERT INTO servers (nodename, timestamp, server, idleThreads, serverConnections, clientConnections, waitedCalls, udpInDatagrams, udpNoPorts, udpInErrors, udpOutDatagrams, recent, avgd)
        VALUES (t_nodename, t_timestamp, t_server, t_idleThreads, t_serverConnections, t_clientConnections, t_waitedCalls, t_udpInDatagrams, t_udpNoPorts, t_udpInErrors, t_udpOutDatagrams, 1, 0);
END
//
DELIMITER ;
