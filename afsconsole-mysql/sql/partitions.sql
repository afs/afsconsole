DROP PROCEDURE IF EXISTS prunepartitions;
DELIMITER //
CREATE PROCEDURE prunepartitions (_timestamp INT UNSIGNED)
BEGIN
    UPDATE partitions SET recent = 0 WHERE recent = 1 AND timestamp < _timestamp - 21600;
END
//
DELIMITER ;

