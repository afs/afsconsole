CREATE TABLE `flashcaches` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nodename` varchar(256) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL,
  `server` varchar(64) NOT NULL,
  `partition` varchar(64) NOT NULL,
  `reads` int(10) unsigned NOT NULL,
  `writes` int(10) unsigned NOT NULL,
  `read_hits` int(10) unsigned NOT NULL,
  `read_hit_percent` int(10) unsigned NOT NULL,
  `write_hits` int(10) unsigned NOT NULL,
  `write_hit_percent` int(10) unsigned NOT NULL,
  `replacement` int(10) unsigned NOT NULL,
  `write_replacement` int(10) unsigned NOT NULL,
  `write_invalidates` int(10) unsigned NOT NULL,
  `read_invalidates` int(10) unsigned NOT NULL,
  `pending_enqueues` int(10) unsigned NOT NULL,
  `pending_inval` int(10) unsigned NOT NULL,
  `no_room` int(10) unsigned NOT NULL,
  `disk_reads` int(10) unsigned NOT NULL,
  `disk_writes` int(10) unsigned NOT NULL,
  `ssd_reads` int(10) unsigned NOT NULL,
  `ssd_writes` int(10) unsigned NOT NULL,
  `uncached_reads` int(10) unsigned NOT NULL,
  `uncached_writes` int(10) unsigned NOT NULL,
  `uncached_IO_requeue` int(10) unsigned NOT NULL,
  `uncached_sequential_reads` int(10) unsigned NOT NULL,
  `uncached_sequential_writes` int(10) unsigned NOT NULL,
  `pid_adds` int(10) unsigned NOT NULL,
  `pid_dels` int(10) unsigned NOT NULL,
  `pid_drops` int(10) unsigned NOT NULL,
  `pid_expiry` int(10) unsigned NOT NULL,
  `delta_timestamp` int(10) unsigned NOT NULL,
  `delta_reads` int(10) unsigned NOT NULL,
  `delta_writes` int(10) unsigned NOT NULL,
  `delta_read_hits` int(10) unsigned NOT NULL,
  `delta_read_hit_percent` int(10) unsigned NOT NULL,
  `delta_write_hits` int(10) unsigned NOT NULL,
  `delta_write_hit_percent` int(10) unsigned NOT NULL,
  `delta_replacement` int(10) unsigned NOT NULL,
  `delta_write_replacement` int(10) unsigned NOT NULL,
  `delta_write_invalidates` int(10) unsigned NOT NULL,
  `delta_read_invalidates` int(10) unsigned NOT NULL,
  `delta_pending_enqueues` int(10) unsigned NOT NULL,
  `delta_pending_inval` int(10) unsigned NOT NULL,
  `delta_no_room` int(10) unsigned NOT NULL,
  `delta_disk_reads` int(10) unsigned NOT NULL,
  `delta_disk_writes` int(10) unsigned NOT NULL,
  `delta_ssd_reads` int(10) unsigned NOT NULL,
  `delta_ssd_writes` int(10) unsigned NOT NULL,
  `delta_uncached_reads` int(10) unsigned NOT NULL,
  `delta_uncached_writes` int(10) unsigned NOT NULL,
  `delta_uncached_IO_requeue` int(10) unsigned NOT NULL,
  `delta_uncached_sequential_reads` int(10) unsigned NOT NULL,
  `delta_uncached_sequential_writes` int(10) unsigned NOT NULL,
  `delta_pid_adds` int(10) unsigned NOT NULL,
  `delta_pid_dels` int(10) unsigned NOT NULL,
  `delta_pid_drops` int(10) unsigned NOT NULL,
  `delta_pid_expiry` int(10) unsigned NOT NULL,
  `recent` tinyint(1) NOT NULL DEFAULT '0',
  `avgd` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `timestamp_server_partition_nodename_avgd` (`timestamp`,`server`,`partition`,`nodename`,`avgd`),
  KEY `recent_server_partition` (`recent`,`server`,`partition`)
) ENGINE=InnoDB;

DROP PROCEDURE IF EXISTS addflashcachesample;
DELIMITER //
CREATE PROCEDURE `addflashcachesample`(IN t_nodename VARCHAR(256),
                                      IN t_timestamp INTEGER UNSIGNED,
                                      IN t_server VARCHAR(64),
                                      IN t_partition VARCHAR(64),
                                      IN t_reads int(10) unsigned,
                                      IN t_writes int(10) unsigned,
                                      IN t_read_hits int(10) unsigned,
                                      IN t_read_hit_percent int(10) unsigned,
                                      IN t_write_hits int(10) unsigned,
                                      IN t_write_hit_percent int(10) unsigned,
                                      IN t_replacement int(10) unsigned,
                                      IN t_write_replacement int(10) unsigned,
                                      IN t_write_invalidates int(10) unsigned,
                                      IN t_read_invalidates int(10) unsigned,
                                      IN t_pending_enqueues int(10) unsigned,
                                      IN t_pending_inval int(10) unsigned,
                                      IN t_no_room int(10) unsigned,
                                      IN t_disk_reads int(10) unsigned,
                                      IN t_disk_writes int(10) unsigned,
                                      IN t_ssd_reads int(10) unsigned,
                                      IN t_ssd_writes int(10) unsigned,
                                      IN t_uncached_reads int(10) unsigned,
                                      IN t_uncached_writes int(10) unsigned,
                                      IN t_uncached_IO_requeue int(10) unsigned,
                                      IN t_uncached_sequential_reads int(10) unsigned,
                                      IN t_uncached_sequential_writes int(10) unsigned,
                                      IN t_pid_adds int(10) unsigned,
                                      IN t_pid_dels int(10) unsigned,
                                      IN t_pid_drops int(10) unsigned,
                                      IN t_pid_expiry int(10) unsigned)
BEGIN
    DECLARE l_timestamp int(10) unsigned DEFAULT 0;
    DECLARE l_reads int(10) unsigned DEFAULT 0;
    DECLARE l_writes int(10) unsigned DEFAULT 0;
    DECLARE l_read_hits int(10) unsigned DEFAULT 0;
    DECLARE l_read_hit_percent int(10) unsigned DEFAULT 0;
    DECLARE l_write_hits int(10) unsigned DEFAULT 0;
    DECLARE l_write_hit_percent int(10) unsigned DEFAULT 0;
    DECLARE l_replacement int(10) unsigned DEFAULT 0;
    DECLARE l_write_replacement int(10) unsigned DEFAULT 0;
    DECLARE l_write_invalidates int(10) unsigned DEFAULT 0;
    DECLARE l_read_invalidates int(10) unsigned DEFAULT 0;
    DECLARE l_pending_enqueues int(10) unsigned DEFAULT 0;
    DECLARE l_pending_inval int(10) unsigned DEFAULT 0;
    DECLARE l_no_room int(10) unsigned DEFAULT 0;
    DECLARE l_disk_reads int(10) unsigned DEFAULT 0;
    DECLARE l_disk_writes int(10) unsigned DEFAULT 0;
    DECLARE l_ssd_reads int(10) unsigned DEFAULT 0;
    DECLARE l_ssd_writes int(10) unsigned DEFAULT 0;
    DECLARE l_uncached_reads int(10) unsigned DEFAULT 0;
    DECLARE l_uncached_writes int(10) unsigned DEFAULT 0;
    DECLARE l_uncached_IO_requeue int(10) unsigned DEFAULT 0;
    DECLARE l_uncached_sequential_reads int(10) unsigned DEFAULT 0;
    DECLARE l_uncached_sequential_writes int(10) unsigned DEFAULT 0;
    DECLARE l_pid_adds int(10) unsigned DEFAULT 0;
    DECLARE l_pid_dels int(10) unsigned DEFAULT 0;
    DECLARE l_pid_drops int(10) unsigned DEFAULT 0;
    DECLARE l_pid_expiry int(10) unsigned DEFAULT 0;

    DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
    SELECT timestamp, `reads`, writes, read_hits, read_hit_percent, write_hits, write_hit_percent, replacement, write_replacement,
           write_invalidates, read_invalidates, pending_enqueues, pending_inval, no_room, disk_reads, disk_writes,
           ssd_reads, ssd_writes, uncached_reads, uncached_writes, uncached_IO_requeue, uncached_sequential_reads,
           uncached_sequential_writes, pid_adds, pid_dels, pid_drops, pid_expiry
           INTO l_timestamp, l_reads, l_writes, l_read_hits, l_read_hit_percent, l_write_hits, l_write_hit_percent, l_replacement,
           l_write_replacement, l_write_invalidates, l_read_invalidates, l_pending_enqueues, l_pending_inval, l_no_room,
           l_disk_reads, l_disk_writes, l_ssd_reads, l_ssd_writes, l_uncached_reads, l_uncached_writes, l_uncached_IO_requeue,
           l_uncached_sequential_reads, l_uncached_sequential_writes, l_pid_adds, l_pid_dels, l_pid_drops, l_pid_expiry
           FROM flashcaches WHERE server=t_server AND partition=t_partition AND recent=1;
    START TRANSACTION;
    UPDATE flashcaches SET recent=0 WHERE server=t_server AND partition=t_partition AND recent=1;
    INSERT INTO flashcaches SET nodename = t_nodename,
                                timestamp = t_timestamp,
                                server = t_server,
                                partition = t_partition,
                                `reads` = t_reads,
                                writes = t_writes,
                                read_hits = t_read_hits,
                                read_hit_percent = t_read_hit_percent,
                                write_hits = t_write_hits,
                                write_hit_percent = t_write_hit_percent,
                                replacement = t_replacement,
                                write_replacement = t_write_replacement,
                                write_invalidates = t_write_invalidates,
                                read_invalidates = t_read_invalidates,
                                pending_enqueues = t_pending_enqueues,
                                pending_inval = t_pending_inval,
                                no_room = t_no_room,
                                disk_reads = t_disk_reads,
                                disk_writes = t_disk_writes,
                                ssd_reads = t_ssd_reads,
                                ssd_writes = t_ssd_writes,
                                uncached_reads = t_uncached_reads,
                                uncached_writes = t_uncached_writes,
                                uncached_IO_requeue = t_uncached_IO_requeue,
                                uncached_sequential_reads = t_uncached_sequential_reads,
                                uncached_sequential_writes = t_uncached_sequential_writes,
                                pid_adds = t_pid_adds,
                                pid_dels = t_pid_dels,
                                pid_drops = t_pid_drops,
                                pid_expiry = t_pid_expiry,
                                delta_timestamp = t_timestamp - l_timestamp,
                                delta_reads = t_reads - l_reads,
                                delta_writes = t_writes - l_writes,
                                delta_read_hits = t_read_hits - l_read_hits,
                                delta_read_hit_percent = 0,
                                delta_write_hits = t_write_hits - l_write_hits,
                                delta_write_hit_percent = 0,
                                delta_replacement = t_replacement - l_replacement,
                                delta_write_replacement = t_write_replacement - l_write_replacement,
                                delta_write_invalidates = t_write_invalidates - l_write_invalidates,
                                delta_read_invalidates = t_read_invalidates - l_read_invalidates,
                                delta_pending_enqueues = t_pending_enqueues - l_pending_enqueues,
                                delta_pending_inval = t_pending_inval - l_pending_inval,
                                delta_no_room = t_no_room - l_no_room,
                                delta_disk_reads = t_disk_reads - l_disk_reads,
                                delta_disk_writes = t_disk_writes - l_disk_writes,
                                delta_ssd_reads = t_ssd_reads - l_ssd_reads,
                                delta_ssd_writes = t_ssd_writes - l_ssd_writes,
                                delta_uncached_reads = t_uncached_reads - l_uncached_reads,
                                delta_uncached_writes = t_uncached_writes - l_uncached_writes,
                                delta_uncached_IO_requeue = t_uncached_IO_requeue - l_uncached_IO_requeue,
                                delta_uncached_sequential_reads = t_uncached_sequential_reads - l_uncached_sequential_reads,
                                delta_uncached_sequential_writes = t_uncached_sequential_writes - l_uncached_sequential_writes,
                                delta_pid_adds = t_pid_adds - l_pid_adds,
                                delta_pid_dels = t_pid_dels - l_pid_dels,
                                delta_pid_drops = t_pid_drops - l_pid_drops,
                                delta_pid_expiry = t_pid_expiry - l_pid_expiry,
                                recent = 1,
                                avgd = 0;
    UPDATE flashcaches SET delta_read_hit_percent = (delta_read_hits/delta_reads)*100 WHERE server=t_server AND partition=t_partition AND recent=1;
    UPDATE flashcaches SET delta_write_hit_percent = (delta_write_hits/delta_writes)*100 WHERE server=t_server AND partition=t_partition AND recent=1;

    COMMIT;
END
//
