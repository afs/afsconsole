#!/usr/bin/env perl

#------------------------------------------------------------------------------
# setup.pl        : the mysql setup
#
# Initial version : Jerome Belleman and Arne Wiebalck IT-DSS/FDO, Oct 2010.
#------------------------------------------------------------------------------

use strict;
use DBI;

my $sql;
my $host = 'localhost';

#------------------------------------------------------------------------------
# Global variables
#------------------------------------------------------------------------------

my $yestoall = 0;

#------------------------------------------------------------------------------
# Subroutines
#------------------------------------------------------------------------------

sub yesnoskip() {

    return 0 if (1 == $yestoall);
   
    my $yesno;
    my $ctr = 0;
    my $q = shift;

    do {	
	if ($ctr++>0) {print "Please answer 'yes', 'no', or 'skip'!\n"}
	print $q."\nOK? [yes/no/skip]: ";
	$yesno = <STDIN>;
	chomp $yesno;
	system('stty echo');
    } until ($yesno eq "yes" or $yesno eq "no" or $yesno eq "skip");

    if ($yesno eq "no") {
	print "Setup aborted by user\n";
	exit 1;
    } elsif ($yesno eq "skip") {
	print "\n";
	return 1;
    } 
    print "\n";
    return 0;
}

#------------------------------------------------------------------------------
# Main
#------------------------------------------------------------------------------

foreach my $i (0 .. $#ARGV) {
    if ($ARGV[$i] eq "-y") {
	$yestoall = 1;
    }
}

# Up-front warning
#
print "The setup will require the local MySQL admin password.\n";
print "The setup will require adaptations to the local MySQL setup.\n";
if (1 == &yesnoskip("The setup will require a restart of the local MySQL deamon.")) {
    print "Setup skipped by user\n";
    exit 1;
}

# Adapt my.cnf (InnoDB setup)
#
if (0 == &yesnoskip("Will replace /etc/my.cnf.")) {   
    unless (-e "/etc/my.cnf.beforeAFSConsole") {
	system("mv /etc/my.cnf /etc/my.cnf.beforeAFSConsole");
    }
    system("cp /usr/share/afsconsole/mysql/my.cnf /etc/");    
} else {
    print "/etc/my.cnf left untouched\n";    
}

# Confirm changes
#
print "Please check your /etc/my.cnf and make necessary changes, like\n";
print "the \'datadir\' option.";
&yesnoskip("");

# Restart the daemon to take note 
#
if (0 == &yesnoskip("Will now restart mysqld.")) {   
    system("service mysqld restart 2>&1 >/dev/null");
} else {
    print "mysqld was not restarted\n";    
}

# Setup the MySQL connection
#
print "\nMySQL admin account for $host: ";
my $user = <STDIN>;
chomp $user;
print "Password: ";
system('stty -echo');
my $passwd = <STDIN>;
chomp $passwd;
system('stty echo');
print "\n";

# Connect to database server
#
my $dbh;
my $dbh = DBI->connect("DBI:mysql:mysql:$host", $user, $passwd, {PrintError => 0})
	or die $DBI::errstr;

# Create database
#
print "Creating DB \'afsconsole\'\n";
if (! $dbh->do('CREATE DATABASE afsconsole')) {
	if ($DBI::errstr =~ /database exists/) {
		print "WARNING: database \'afsconsole\' already exists!\n";
	} else {
		die $DBI::errstr;
	}
} 

# Create RO user
#
print "Creating read-only user \'afsconsole_ro\'\n";
my $passwdagain;
do {
        print "  Please enter a password for the read-only account \'afsconsole_ro\'.\n";
        print "  Password: ";
        system('stty -echo');
        $passwd = <STDIN>;
        chomp $passwd;
        system('stty echo');
        print "\n";
        print "  Password (again): ";
        system('stty -echo');
        $passwdagain = <STDIN>;
        chomp $passwdagain;
        system('stty echo');
        print "\n";
} until ($passwd eq $passwdagain);
$passwdagain = '';

if (! $dbh->do("INSERT user (host,user,password) VALUES ('%', 'afsconsole_ro', PASSWORD('$passwd'))")) {
	if ($DBI::errstr =~ /Duplicate entry/) {
		print "WARNING: user \'afsconsole_ro\' already exists!\n";
	} else {
		die $DBI::errstr;
   	}
}


if (! $dbh->do("INSERT db (host,db,user,select_priv,execute_priv) VALUES ('%', 'afsconsole', 'afsconsole_ro', 'Y', 'N')")) {
        if ($DBI::errstr =~ /Duplicate entry/) {
                print "WARNING: grants for user \'afsconsole_ro\' already set!\n";
        } else {
                die $DBI::errstr;
        }
}


# Create RW user
#
print "Creating read-write user \'afsconsole_rw\'\n";
do {
	print "  Please enter a password for the read-write account \'afsconsole_rw\'.\n";
	print "  Password: ";
	system('stty -echo');
	$passwd = <STDIN>;
	chomp $passwd;
	system('stty echo');
	print "\n";
	print "  Password (again): ";
	system('stty -echo');
	$passwdagain = <STDIN>;
	chomp $passwdagain;
	system('stty echo');
	print "\n";
} until ($passwd eq $passwdagain);
if (! $dbh->do("INSERT user (host,user,password) VALUES ('localhost', 'afsconsole_rw', PASSWORD('$passwd'))")) {
        if ($DBI::errstr =~ /Duplicate entry/) {
                print "WARNING: user \'afsconsole_rw\' already exists!\n";
        } else {
                die $DBI::errstr;
        }
}



if (!$dbh->do("INSERT db (host,db,user,select_priv,insert_priv,update_priv,delete_priv,execute_priv) VALUES ('localhost', 'afsconsole', 'afsconsole_rw', 'Y', 'Y', 'Y', 'Y', 'Y')")) {
        if ($DBI::errstr =~ /Duplicate entry/) {
                print "WARNING: grants for user \'afsconsole_rw\' already set!\n";
        } else {
                die $DBI::errstr;
        }
}


$dbh->do("FLUSH PRIVILEGES") or die $DBI::errstr;

# Create tables
#
print "Creating tables...\n";
$sql = <<EOF;
CREATE TABLE afsconsole.volumes (id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                                 nodename VARCHAR(256) NOT NULL,
								 timestamp INT UNSIGNED NOT NULL,
								 volumeid INT UNSIGNED NOT NULL,
                                 volumename VARCHAR(64) NOT NULL,
								 server VARCHAR(64) NOT NULL,
								 partition VARCHAR(64) NOT NULL,
								 type VARCHAR(64) NOT NULL,
								 size INT UNSIGNED NOT NULL,
								 files INT UNSIGNED NOT NULL,
								 status VARCHAR(64) NOT NULL,
								 quota INT UNSIGNED NOT NULL,
								 availability INT UNSIGNED NOT NULL,
								 currentAccesses INT NOT NULL,
								 currentReads INT NOT NULL,
								 currentWrites INT NOT NULL,
								 totalAccesses INT UNSIGNED NOT NULL,
								 totalReads INT UNSIGNED NOT NULL,
								 totalWrites INT UNSIGNED NOT NULL,
								 project VARCHAR(64) NOT NULL,
								 quality VARCHAR(64) NOT NULL,
								 depth INT UNSIGNED NOT NULL,
								 flag INT UNSIGNED NOT NULL,
								 totalRemoteReads INT UNSIGNED NOT NULL,
								 totalRemoteWrites INT UNSIGNED NOT NULL,
                                 recent BOOL NOT NULL DEFAULT 0,
								 avgd BOOL NOT NULL) ENGINE INNODB
EOF
if (! $dbh->do($sql)) {
        if ($DBI::errstr =~ /already exists/) {
                print "WARNING: database table \'volumes\' already exists!\n";
        } else {
                die $DBI::errstr;
        }
}
 

$sql = <<EOF;
CREATE TABLE afsconsole.partitions (id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                                    nodename VARCHAR(256) NOT NULL,
									timestamp INT UNSIGNED NOT NULL,
									server VARCHAR(64) NOT NULL,
									partition VARCHAR(64) NOT NULL,
									fillstatus INT UNSIGNED NOT NULL,
									size BIGINT UNSIGNED NOT NULL,
									used BIGINT UNSIGNED NOT NULL,
									volumes INT UNSIGNED NOT NULL,
									online INT NOT NULL,
									offline INT NOT NULL,
									committed BIGINT UNSIGNED NOT NULL,
									readAccessTime INT UNSIGNED NOT NULL,
									writeAccessTime INT UNSIGNED NOT NULL,
                                    recent BOOL NOT NULL DEFAULT 0,
									avgd BOOL NOT NULL) ENGINE INNODB
EOF
if (! $dbh->do($sql)) {
        if ($DBI::errstr =~ /already exists/) {
                print "WARNING: database table \'partitions\' already exists!\n";
        } else {
                die $DBI::errstr;
        }
}



# Create primary keys and indices
#
print "Creating indexes...\n";
$sql = <<EOF;
ALTER TABLE afsconsole.volumes
ADD UNIQUE timestamp_volumeid_server_partition_avgd 
          (timestamp,volumeid,server,partition,avgd),
ADD KEY volumeid_timestamp (volumeid,timestamp),
ADD KEY volumename_timestamp (volumename,timestamp),
ADD KEY recent_server_partition (recent,server,partition),
ADD KEY server_partition_timestamp (server,partition,timestamp),
ADD KEY recent_server_timestamp (recent,server,timestamp),
ADD KEY recent_currentAccesses (recent,currentAccesses),
ADD KEY recent_project (recent,project),
ADD KEY recent_volumename (recent,volumename),
ADD KEY recent_volumeid (recent,volumeid), 
ADD KEY avgd_timestamp (avgd,timestamp)
EOF
if (! $dbh->do($sql)) {
        if ($DBI::errstr =~ /Duplicate key name/) {
                print "WARNING: indeces already exist!\n";
        } else {
                die $DBI::errstr;
        }
}



$sql = <<EOF;
ALTER TABLE afsconsole.partitions
ADD UNIQUE timestamp_server_partition_nodename_avgd
          (timestamp,server,partition,nodename,avgd),
ADD KEY recent_server_partition (recent,server,partition)
EOF
if (! $dbh->do($sql)) {
        if ($DBI::errstr =~ /Duplicate key name/) {
                print "WARNING: indeces already exist!\n";
        } else {
                die $DBI::errstr;
        }
}


# Create procedures
#
print "Creating procedures...\n";
$sql = <<EOF;
CREATE PROCEDURE afsconsole.addvolumesample (_nodename VARCHAR(256),
											 _timestamp INT UNSIGNED,
											 _volumeid INT UNSIGNED,
											 _volumename VARCHAR(64),
											 _server VARCHAR(64),
											 _partition VARCHAR(64),
											 _type VARCHAR(64),
											 _size INT UNSIGNED,
											 _files INT UNSIGNED,
											 _status VARCHAR(64),
											 _quota INT UNSIGNED,
											 _availability INT UNSIGNED,
											 _currentAccesses INT,
											 _currentReads INT,
											 _currentWrites INT,
											 _totalAccesses INT UNSIGNED,
											 _totalReads INT UNSIGNED,
											 _totalWrites INT UNSIGNED,
											 _project VARCHAR(64),
											 _quality VARCHAR(64),
											 _depth INT UNSIGNED,
											 _flag INT UNSIGNED,
											 _totalRemoteReads INT UNSIGNED,
											 _totalRemoteWrites INT UNSIGNED)
BEGIN
	INSERT volumes VALUES (NULL, _nodename, _timestamp, _volumeid, _volumename, 
						   _server, _partition, _type, _size, _files, _status, 
						   _quota, _availability, _currentAccesses, 
						   _currentReads, _currentWrites, _totalAccesses, 
						   _totalReads, _totalWrites, _project, _quality, 
						   _depth, _flag, _totalRemoteReads,
						   _totalRemoteWrites, 1, 0);
END
EOF
if (! $dbh->do($sql)) {
        if ($DBI::errstr =~ /already exists/) {
                print "WARNING: procedure already exists!\n";
        } else {
                die $DBI::errstr;
        }
}


$sql = <<EOF;
CREATE PROCEDURE afsconsole.prune (_timestamp INT UNSIGNED)
BEGIN
    UPDATE volumes SET recent = 0
    WHERE recent = 1 AND timestamp < _timestamp - 21600;
END
EOF
if (! $dbh->do($sql)) {
        if ($DBI::errstr =~ /already exists/) {
                print "WARNING: procedure already exists!\n";
        } else {
                die $DBI::errstr;
        }
}

$sql = <<EOF;
CREATE PROCEDURE afsconsole.pruneonce (_timestamp INT UNSIGNED,
                                       _server VARCHAR(64))
BEGIN
    UPDATE volumes SET recent = 0
    WHERE server = _server
    AND recent = 1
    AND timestamp < _timestamp;
END
EOF
if (! $dbh->do($sql)) {
        if ($DBI::errstr =~ /already exists/) {
                print "WARNING: procedure already exists!\n";
        } else {
                die $DBI::errstr;
        }
}
$sql = <<EOF;
CREATE PROCEDURE afsconsole.addpartitionsample (_nodename VARCHAR(256),
						_timestamp INT UNSIGNED,
						_server VARCHAR(64),
						_partition VARCHAR(64),
						_fillstatus INT UNSIGNED,
						_size BIGINT UNSIGNED,
						_used BIGINT UNSIGNED,
						_volumes INT UNSIGNED,
						_online INT,
						_offline INT,
						_committed BIGINT UNSIGNED,
						_readAccessTime INT UNSIGNED,
						_writeAccessTime INT UNSIGNED)
BEGIN
    UPDATE partitions SET recent = 0
    WHERE server = _server 
    AND vice_partition = _partition
    AND recent = 1;

    INSERT partitions VALUES (NULL, _nodename, _timestamp, _server, _partition,
			  _fillstatus, _size, _used, _volumes, _online,
			  _offline, _committed, _readAccessTime,
			  _writeAccessTime, 1, 0);
END
EOF
if (! $dbh->do($sql)) {
        if ($DBI::errstr =~ /already exists/) {
                print "WARNING: procedure already exists!\n";
        } else {
                die $DBI::errstr;
        }
}


# Cleanup
#
$dbh->disconnect();

print "Done\n";
