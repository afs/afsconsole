#!/usr/bin/env perl

#------------------------------------------------------------------------------
# Volumes         : The volumes sensor of the AFS Console.
#
# Initial version : Jerome Belleman and Arne Wiebalck IT-DSS/FDO, Oct 2010.
#------------------------------------------------------------------------------

package Volumes;

use strict;
use warnings;

use DBI;
use Sys::Hostname;
use Time::HiRes;

use POSIX ':sys_wait_h';
use POSIX ':signal_h';

use lib '/usr/share/afsconsole/common';
use AFSConsoleConf;
use AFSConsoleSensors;


#------------------------------------------------------------------------------
# Configuration
#------------------------------------------------------------------------------

# the max time to collect the data
use constant WINTERVAL => 600;

# usually no need to change these:
use constant VOL => 'volumes';

my $statedir = '/tmp/AFSvolumes';


#------------------------------------------------------------------------------
# Subroutines
#------------------------------------------------------------------------------

sub get_project {

    # extract the project from the volume's name.
    #
    # returns: the associated project of a volume.

    my $_volumeName = shift;

    if (not $_volumeName) {
        return 'n.a.';
    }
    if ($_volumeName =~ /^[pqsuZ]\./) {
        my @info = split(/\./, $_volumeName);
        return $info[1];
    }
    if ($_volumeName =~ /^(user)\./) {
        return $1;
    }
    if ($_volumeName =~ /^(work)\./) {
        return $1;
    }
    if ($_volumeName =~ /^R\.\d+\.\d+/) {
        return 'recover';
    }
    if ($_volumeName =~ /^Y\.(user|work)\./) {
        return 'deleted';
    }
    # truly legacy stuff below
    if ($_volumeName =~ /^(asis|chorus)\./) {
        return $1;
    }
    if ($_volumeName =~ /^(root\..*|sys\..*|user|system|work)$/) {
        return 'afs';
    }

    return 'n.a.';
}

sub get_quality {

    # extract the volume's quality from the volume's name.
    #
    # returns: the quality of a volume, i.e. p, q, u, s or user.

    my $_volumeName = shift;

    if (not $_volumeName) {
        return 'n.a.';
    }

    if ($_volumeName =~ /^[pqsu]\./) {
        my @info = split(/\./, $_volumeName);
        return $info[0];
    }

    if ($_volumeName =~ /^(user)\./) {
        return $1;
    }

    return 'n.a.';
}

sub srv_and_part {

    # extract server and partition from vos output.
    #
    # returns: nothing.

    my ($_line, $_Rsrv, $_Rpart) = @_;
    if (/Total number of volumes on server (\w+) partition \/vicep(\S+): \d+/) {
        $$_Rsrv  = $1;
        $$_Rpart  = $2;
    }
}

sub vol {

    # Extract volume information.
    #
    # returns: nothing.

    my ($_line, $_srv, $_part, $_RvolumeName, $_RvolumeID, $_Rtype, $_Rsize,
        $_Rfiles, $_Rstatus, $_Rproject, $_Rquality, $_Rdepth, $_Rflag,
        $_Rkey) = @_;

    $_ = $_line;

    if (/files/) {
        if (/([\S]+)\s+(\d+) (\w\w)\s+(\d+) K used (\d+) files (\w+)/) {
            $$_RvolumeName = $1;
            $$_RvolumeID   = $2;
            $$_Rtype       = $3;
            $$_Rsize       = $4;
            $$_Rfiles      = $5;
            $$_Rstatus     = $6;
            $$_Rproject    = get_project($$_RvolumeName);
            $$_Rquality    = get_quality($$_RvolumeName);
            $$_Rdepth      = 0; # not used
            $$_Rflag       = 0; # not used

            # set the key for the data hashes
            $$_Rkey = "$_srv-$_part-$$_RvolumeID";

        } elsif (/([\S]+)\s+(\d+) (\w\w)\s+-(\d+) K used (\d+) files (\w+)/) {
            print "$_: negative size\n";

            $$_RvolumeName = $1;
            $$_RvolumeID   = $2;
            $$_Rtype       = $3;
            $$_Rsize       = 0;  # !!
            $$_Rfiles      = $5;
            $$_Rstatus     = $6;
            $$_Rproject    = get_project($$_RvolumeName);
            $$_Rquality    = get_quality($$_RvolumeName);
            $$_Rdepth      = 0; # not used
            $$_Rflag       = 0; # not used

            # set the key for the data hashes
            $$_Rkey = "$_srv-$_part-$$_RvolumeID";
        }
    }
}

sub quota {

    # extract quota information.
    #
    # returns: nothing.

    my ($_line, $_Rquota) = @_;

    $_ = $_line;
    if (/MaxQuota\s+(\d+) K/) {
        $$_Rquota = $1;
    }
}

sub totals {

    # extract totals of access/read/write.
    #
    # returns: nothing.

    my ($_line, $_RtotalAccesses, $_RtotalReads, $_RtotalRemoteReads,
        $_RtotalWrites, $_RtotalRemoteWrites) = @_;

    # total number of accesses
    $_ = $_line;
    if (/(\d+) accesses in the past day/) {
        $$_RtotalAccesses = $1;
    }

    # reads and writes
    if (/^Reads/) {
        my @Atmp = split(/\s+/, $_);
        $$_RtotalReads = $Atmp[2] + $Atmp[6];
        $$_RtotalRemoteReads = $Atmp[6];
    }
    if (/^Writes/) {
        my @Atmp = split(/\s+/, $_);
        $$_RtotalWrites = $Atmp[2] + $Atmp[6];
        $$_RtotalRemoteWrites = $Atmp[6];
    }
}

sub end_and_rates {

    # determine the access rates.
    #
    # returns: nothing.

    my ($_line, $_ts, $_key, $_volumeID, $_volumeName, $_srv, $_part, $_type,
        $_size, $_files, $_status, $_quota, $_project, $_quality, $_depth,
        $_flag, $_totalRemoteReads, $_totalRemoteWrites, $_RtotalAccesses,
        $_RtotalReads, $_RtotalWrites, $_RcurrentAccesses, $_RcurrentReads,
        $_RcurrentWrites, $_fileacc, $_RAdata, $_RHtotals) = @_;

    if ($_line =~ /> 1wk/) {
        # can happen for completely unused volumes
        $$_RtotalAccesses = 0 if (!defined $$_RtotalAccesses);
        $$_RtotalReads    = 0 if (!defined $$_RtotalReads);
        $$_RtotalWrites   = 0 if (!defined $$_RtotalWrites);

        my $entries = "$_ts $$_RtotalAccesses $$_RtotalReads $$_RtotalWrites";
        print $_fileacc "$_key $entries\n";

        # determine the rates
        if (exists $$_RHtotals{$_key}) {
            my @AnewData = split(/\s+/, $entries);
            my @AoldData = split(/\s+/, $$_RHtotals{$_key});

            my $oldLen = @AoldData;
            my $newLen = @AnewData;
            if (4 != $oldLen or 4 != $newLen) {
                &AFSConsoleSensors::warn(VOL, "Not enough data for volume $_key!");
                &AFSConsoleSensors::warn(VOL,
                           "AoldData: $oldLen (@AoldData) AnewData: $newLen");
                $$_RcurrentAccesses = -1;
                $$_RcurrentReads    = -1;
                $$_RcurrentWrites   = -1;
            } else {
                my $rate;

                # accesses / sec
                if ($AnewData[1] < $AoldData[1]) {
                    $AoldData[1] = 0; # reset can lead to negative rates
                }
                $rate = ($AnewData[1] - $AoldData[1]) /
                        ($AnewData[0] - $AoldData[0]);
                $$_RcurrentAccesses = sprintf("%.3f", $rate);

                # reads / sec
                if ($AnewData[2] < $AoldData[2]) {
                    $AoldData[2] = 0; # reset can lead to negative rates
                }
                $rate = ($AnewData[2] - $AoldData[2]) /
                        ($AnewData[0] - $AoldData[0]);
                $$_RcurrentReads = sprintf("%.3f", $rate);

                # writes / sec
                if ($AnewData[3] < $AoldData[3]) {
                    $AoldData[3] = 0; # reset can lead to negative rates
                }
                $rate = ($AnewData[3] - $AoldData[3]) /
                        ($AnewData[0] - $AoldData[0]);
                $$_RcurrentWrites = sprintf("%.3f", $rate);
            }
        } else {
            $$_RcurrentAccesses = -1;
            $$_RcurrentReads    = -1;
            $$_RcurrentWrites   = -1;
        }

        # insert the collected data into the big data hash
        # (omit backup volumes; the analysis costs nothing
        # and these volumes maybe added again later on ...);
        # omit also the recovered volumes as they appear and
        # disappear quickly
        if ($_volumeName !~ /\.backup$/ and $_volumeName !~ /\._R/) {
            my @entries;
            push(@entries, $_volumeID);
            push(@entries, $_volumeName);
            push(@entries, $_srv);
            push(@entries, $_part);
            push(@entries, $_type);
            push(@entries, $_size);
            push(@entries, $_files);
            push(@entries, $_status);
            push(@entries, $_quota);
            push(@entries, availability($_status));
            push(@entries, $$_RcurrentAccesses);
            push(@entries, $$_RcurrentReads);
            push(@entries, $$_RcurrentWrites);
            push(@entries, $$_RtotalAccesses);
            push(@entries, $$_RtotalReads);
            push(@entries, $$_RtotalWrites);
            push(@entries, $_project);
            push(@entries, $_quality);
            push(@entries, $_depth);
            push(@entries, $_flag);
            push(@entries, $_totalRemoteReads);
            push(@entries, $_totalRemoteWrites);

            push(@$_RAdata, "@entries");
        }
    }
}

sub feedback {

    # give feedback on end of loop.
    #
    # returns: nothing.

    my ($_RAchldpids, $_RHpid2srv, $_RHsrvWorking) = @_;

    my @Achldpids2 = @$_RAchldpids;
    @$_RAchldpids = ();
    foreach my $pid (@Achldpids2) {
        my $server = $$_RHpid2srv{$pid};
        &AFSConsoleSensors::info(VOL, "Check $pid, server $server ...");
        my $rc = waitpid($pid, WNOHANG);
        if (!$?) {
            &AFSConsoleSensors::info(VOL, "Server $server done.");
            $$_RHsrvWorking{$server} = 0;
        } else {
            &AFSConsoleSensors::info(VOL,
                       "Server $server not done yet. Skipped on this round.");
            $$_RHsrvWorking{$server} = 1;
            push(@$_RAchldpids, $pid);
        }
    }
}

sub get_data {

    # collect the data from the servers.
    #
    # returns: nothing.

    my ($_RAservers, $_RHsrvWorking, $_RAchldpids, $_RHpid2srv,
        $_RAdata, $_ts, $_RHtotals) = @_;

    my $voscmd = "timeout $AFSConsoleSensors::VOS_TIMEOUT vos listvol -ext -noauth -server";

    foreach my $server (@$_RAservers) {
        # skip server if it's working
        if (exists $$_RHsrvWorking{$server} && $$_RHsrvWorking{$server} == 1) {
            &AFSConsoleSensors::warn(VOL, "Skipping server $server (not done yet)!");
            next;
        }

        # fork and loop parent
        my $pid = fork;
        if ($pid) {
            push(@$_RAchldpids, $pid);
            @$_RHpid2srv{$pid} = $server;
            next;
        }
        die "fork failed: $!" unless defined $pid;

        # child code

        # do not re-use old data
        unlink ("$statedir/$server.dump");
        unlink ("$statedir/$server.acc");
        # dump the data
        my $start = Time::HiRes::time();
        system($voscmd . " $server > $statedir/$server.dump");
        my $rc = $?;
        my $duration = sprintf("%.3f", Time::HiRes::time() - $start);
        if ($rc == -1) {
            die "could not run $voscmd: $!\n";
        } elsif ($rc) {
            ## might have managed to write some data already, so keep going
            &AFSConsoleSensors::warn(VOL, "'vos listvol' for $server failed after ${duration}s: ".($rc >> 8));
        }
        # analyse data
        my $srv;
        my $part;
        my $volumeName;
        my $volumeID;
        my $type;
        my $size;
        my $files;
        my $status;
        my $project;
        my $quality;
        my $depth;
        my $flag;
        my $quota;
        my $totalAccesses;
        my $totalReads;
        my $totalWrites;
        my $currentAccesses;
        my $currentReads;
        my $currentWrites;
        my $totalRemoteReads;
        my $totalRemoteWrites;

        my $key;

        &AFSConsoleSensors::info(VOL, "Analyze server $server (dump took ${duration}s)...");

        open(my $FILE, '<', "$statedir/$server.dump") or die "cannot open $statedir/$server.dump: $!\n";
        open(my $FILEACC, '>', "$statedir/$server.acc") or die "cannot open $statedir/$server.acc: $!\n";

        while (<$FILE>) {
            # beginning of partition: get srv, partition and number of volumes
            &srv_and_part($_, \$srv, \$part);

            # beginning of volume
            &vol($_, $srv, $part, \$volumeName, \$volumeID, \$type,
		 \$size, \$files, \$status, \$project, \$quality,
		 \$depth, \$flag, \$key);

            # the quota
            &quota($_, \$quota);

            # totals
            &totals($_, \$totalAccesses, \$totalReads, \$totalRemoteReads,
		    \$totalWrites, \$totalRemoteWrites);

            # end of volumes and rates
            &end_and_rates($_, $_ts, $key, $volumeID, $volumeName, $srv,
			   $part, $type, $size, $files, $status, $quota,
			   $project, $quality, $depth, $flag,
			   $totalRemoteReads, $totalRemoteWrites,
			   \$totalAccesses, \$totalReads, \$totalWrites,
			   \$currentAccesses, \$currentReads, \$currentWrites,
			   $FILEACC, $_RAdata, $_RHtotals);
        }

        close $FILE;
        close $FILEACC;

        # dump all to a file
        open(my $FILERES, '>', "$statedir/$server.res") or die "cannot open $statedir/$server.res: $!";
        foreach my $e (@$_RAdata) {
            print $FILERES $e . "\n";
        }
        close $FILERES;

        &AFSConsoleSensors::info(VOL, "Server $server analyzed!");

        exit 0;

        # end of child code
    }

    # wait for the vos listvol and the analysis to be finished
    &AFSConsoleSensors::info(VOL, "Master sleeps now for " . WINTERVAL . " secs ...");
    sleep WINTERVAL;

    # feedback
    feedback($_RAchldpids, $_RHpid2srv, $_RHsrvWorking);
}

sub report_data {

    # send the collected data to the database.
    #
    # returns: nothing.

    my ($_RHsrvWorking, $_srv_s, $_ts, $_RHtotals) = @_;

    my $mark = POSIX::SigSet->new(SIGALRM);
    my $action = POSIX::SigAction->new(
        sub {
             &AFSConsoleSensors::warn(VOL, 'DB INSERT timed out');
             die 'DB INSERT timed out';
        },
        $mark,
    );
    my $oldaction = POSIX::SigAction->new();
    sigaction(SIGALRM, $action, $oldaction);

    eval {
        alarm(100 * $AFSConsoleSensors::NIGHT - WINTERVAL - 60);
        my $count = 0;
        my $cleanupc = 0;
        my $dbh = &AFSConsoleSensors::open($AFSConsoleSensors::dsn);

        if ($dbh) {
            &AFSConsoleSensors::info(VOL, "Starting transfer to DB");
        } else {
            &AFSConsoleSensors::warn(VOL, "Couldn't connect to DB");
            die "Couldn't connect to DB";
        }

        my $sth = &AFSConsoleSensors::prepare($dbh, 'volumes');
        unless ($sth) {
            &AFSConsoleSensors::warn(VOL, "Couldn't prepare collection query");
            die "Couldn't prepare collection query";
        }

        while (my ($server, $working) = each %$_RHsrvWorking) {
            if ($working) {
                &AFSConsoleSensors::warn(VOL, "No report for $server");
                next;
            }
            &AFSConsoleSensors::info(VOL, "Report data of $server");
            open(my $FILERES, '<', "$statedir/$server.res") or next;
            while (<$FILERES>) {
	        $count += &AFSConsoleSensors::add($sth, $_srv_s, $_ts, split(/\s+/, $_));
            }
            close $FILERES;

            # One-shot Cleanup
            if (&AFSConsoleSensors::pruneonce($dbh, $_ts, $server) == 0) {
                $cleanupc++;
            }

            # get the access rate for the next round
            open(my $FILEACC, '<', "$statedir/$server.acc") or next;
            while (<$FILEACC>) {
                my ($key, @Adata) = split(/\s+/, $_);
                $$_RHtotals{$key} = "@Adata";
            }
            close $FILEACC;
        }

        &AFSConsoleSensors::info(VOL, "Inserted $count samples into DB");
        &AFSConsoleSensors::info(VOL, "Performed one-shot cleanups on $cleanupc servers");

        if (&AFSConsoleSensors::prune($dbh, $_ts) == 0) {
            &AFSConsoleSensors::info(VOL, "Cleaned up recent data and finished transfer");
        } else {
            &AFSConsoleSensors::info(VOL, "Couldn't clean up recent volume data");
        }

        &AFSConsoleSensors::close($dbh);
        alarm(0);
    };
    if($@) {
	&AFSConsoleSensors::warn(VOL,"Exception in ".__FILE__.":".$@)
    }
    alarm(0);
    sigaction(SIGALRM, $oldaction);
}

sub availability {

    # determine the availability of a volume; in former times this
    # was done by a stat call; turned out to be expensive.
    #
    # returns: a value between 0 and 100.

    my $_status = shift;

    &AFSConsoleSensors::dbug(VOL, "availability() called");

    # offline volume, av = 0
    if ($_status eq "Off") {
        return 0;
    } else {
        return 100;
    }
}


#------------------------------------------------------------------------------
# The sample function
#------------------------------------------------------------------------------

our (%g_Htotalsa, %g_Htotalsb);
our $g_current = 0;

# Keeping track of delayed vos listvols over cycle processes
our %g_HsrvWorking;
our @g_Achldpids;
our %g_Hpid2srv;

sub sample {

    # trigger the sensor to collect and report data.
    #
    # returns: nothing.

    my $ts = time;

    &AFSConsoleSensors::info(VOL, "Metric volumes started ($ts)");

    mkdir($statedir); # ignore errors since probably exists

    # hostname
    my $host   = hostname;
    my $host_s = (split(/\./, $host))[0];

    # which servers?
    my @Aservers = &AFSConsoleSensors::get_servers();

    &AFSConsoleSensors::info(VOL, "@Aservers");

    # collect and analyze data
    # hashes for the rates and a switch for metric volumes
    my @Adata;
    if ($g_current % 2) {
        &get_data(\@Aservers, \%g_HsrvWorking, \@g_Achldpids, \%g_Hpid2srv,
                 \@Adata, $ts, \%g_Htotalsa);
    } else {
        &get_data(\@Aservers, \%g_HsrvWorking, \@g_Achldpids, \%g_Hpid2srv,
                 \@Adata, $ts, \%g_Htotalsb);
    }

    # report to the database
    &AFSConsoleSensors::info(VOL, 'Send data to database');

    if ($g_current % 2) {
        &report_data(\%g_HsrvWorking, $host_s, $ts, \%g_Htotalsb);
    } else {
        &report_data(\%g_HsrvWorking, $host_s, $ts, \%g_Htotalsa);
    }

    # prepare for the next run
    $g_current = ($g_current % 2) + 1;

    $ts = time;
    &AFSConsoleSensors::info(VOL, "Metric volumes done");
}

1;
