#!/usr/bin/env perl

use strict;
use warnings;

use POSIX ":sys_wait_h";
use POSIX qw(setsid);

use FindBin;
use lib "$FindBin::Bin";
use AFSConsoleSensors;
use Partitions;
use Volumes;
use Servers;
use List::Util qw[max];
use Getopt::Long;

my ($opt_help, $opt_foreground);
if (! GetOptions ("help" => \$opt_help,
                  "foreground" => \$opt_foreground) || $opt_help) {
    print <<EOFusage;
$0 [--foreground]
main process for the AFS console data gathering. Forks children for monitoring
servers, partitions and volumes.
EOFusage
    exit(1);
}
if (! $opt_foreground) {
    chdir '/' or die "$!";
    open(STDIN, '<', '/dev/null') or die "$!";
    open(STDOUT, '>', '/dev/null') or die "$!";
    open(STDERR, '>', '/dev/null') or die "$!";

    defined(my $pid = fork()) or die "$!";
    if ($pid) {
	exit 0;
    }
    setsid or die "$!";
}
AFSConsoleSensors::init('logfile');

my @pids = ();
$0="afsconsole-sensor-main";

foreach my $sensor (keys(%AFSConsoleSensors::sensors)) {
    my $pid = fork;
    die "fork failed: $!" unless defined $pid;

    if ($pid == 0) {
        $0="afsconsole-sensor-$sensor";
        while (1) {
            my $t = time;
            $AFSConsoleSensors::sensors{$sensor}->();
            my $sleep_time = $AFSConsoleSensors::NIGHT - (time - $t);
            AFSConsoleSensors::info('main', "Sleeping $sleep_time seconds.");
            if ($sleep_time < 1) {
                AFSConsoleSensors::warn('main', 'Sensor thread taking too long!');
                $sleep_time = 1;
            }
            sleep $sleep_time;
        }
        exit 0;
    } else {
        push(@pids, $pid);
    }
}

foreach my $pid (@pids) {
    waitpid($pid, 0);
}
@pids = ();

