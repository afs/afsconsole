#!/usr/bin/env perl

#------------------------------------------------------------------------------
# Partitions      : The partitions sensor of the AFS Console.
#
# Initial version : Jerome Belleman and Arne Wiebalck IT-DSS/FDO, Oct 2010.
#------------------------------------------------------------------------------

package Partitions;

use strict;
use warnings;

use DBI;
use Sys::Hostname;
use POSIX ':signal_h';

use lib '/usr/share/afsconsole/common';
use AFSConsoleConf;
use AFSConsoleSensors;

#------------------------------------------------------------------------------
# Configuration
#------------------------------------------------------------------------------

use constant PRT => 'partitions';


#------------------------------------------------------------------------------
# Subroutines
#------------------------------------------------------------------------------

sub get_partitions {

    # get the partitions to monitor.
    #
    # returns: a hash containing the partitions of a server in a blank
    #          separated string.

    my %Hpartitions;

    $AFSConsoleConf::get_partitions_command
        or die "Missing get_partitions command setup in config file\n";
    my @Apartitions = split(/\s+/, `$AFSConsoleConf::get_partitions_command`);
    for (@Apartitions) {
        my ($srv, $prt) = split(/\//, $_);
        if (exists $Hpartitions{$srv}) {
           $Hpartitions{$srv} = $Hpartitions{$srv} . "$prt ";
        } else {
           $Hpartitions{$srv} = "$prt ";
        }
    }

    return %Hpartitions;
}

sub analyze_partinfo {

    # analyze the output of vos partinfo
    # (instance of the Fanalyze function reference parameter)
    #
    # returns: nothing.

    my ($_RHdata, $_pipe, $_host) = @_;

    while (<$_pipe>) {
        if (/Free space on partition \/vicep(\S+): (\d+) K blocks out of total (\d+)/) {
            # $2 is the free space, $3 is the total space
            my $fillStatus = sprintf("%d", 100 - ($2 / $3) * 100);
            my $used       = $3 - $2;

            my $key = "$_host/$1";
            my $ts  = time;
            $$_RHdata{$key} = "$ts $fillStatus $3 $used ";
        } else {
            AFSConsoleSensors::warn(PRT, "Unknown partinfo output: $_");
        }
    }
}

sub analyze_listvol_partitions_fast {

    # analyze the output of vos listvol for partition information (fast version)
    # (instance of the Fanalyze function reference parameter)
    #
    # returns: nothing.

    my ($_RHdata, $_pipe) = @_;
    my $totalQuota = 0;

    my ($host, $part, $total);
    while (<$_pipe>) {
        # get the host, partition and number of volumes
        if (/Total number of volumes on server (\w+) partition \/vicep(\S+): (\d+)/) {
            $host  = $1;
            $part  = $2;
            $total = $3;

            my $key = "$host/$part";
            $$_RHdata{$key} = $$_RHdata{$key} . "$total -1 -1 0 ";
        }
    }
}

sub get_data {

    # collect the data for all partitions.
    #
    # returns: nothing.

    my ($_RAhosts, $_RHdata, $_cmd, $_RFanalyze, $_ext) = @_;
    my %Hpipes;

    # open the pipes, with a timeout in case a server is stuck
    foreach my $host (@$_RAhosts) {
        my $cmd  = "timeout $AFSConsoleSensors::VOS_TIMEOUT " . $_cmd . " -server $host -noauth $_ext";
        my $pipe;
        if(open($pipe, '-|', $cmd)) {
            $Hpipes{$host} = $pipe;
            AFSConsoleSensors::dbug(PRT, "Created pipe for $host");
        } else {
            AFSConsoleSensors::warn(PRT, "Failed to open pipe for $host: $!");
        }
    }

    # read and analyze the data, close the pipe if done
    foreach my $host (@$_RAhosts) {
        AFSConsoleSensors::dbug(PRT, "Read and analyze data for $host");
        my $pipe = $Hpipes{$host};
        &$_RFanalyze($_RHdata, $pipe, $host);
        AFSConsoleSensors::dbug(PRT, "Close pipe for $host");
        close $pipe;
    }
}


sub today {

    # returns: the date of today as used for the access time files.

    my ($sec, $min, $hrs, $day, $mon, $yr, $dow, $doy, $dst) = localtime(time);
    if ($day < 10) {
        $day = "0" . $day;
    }
    $mon++;
    if ($mon < 10) {
        $mon = "0" . $mon;
    }
    $yr += 1900;
    return "$yr-$mon-$day";
}


sub avg {

   # return the average of an array.

    my ($sum, $ctr) = (0,0);
    my @_A = @_;
    foreach my $i (@_A) {
        return if ($i !~ /^\d+$/);
        $sum += $i;
        $ctr++;
    }
    return $sum/$ctr if ($ctr > 0);
    return;
}


sub get_access_times {

    # retrieve the access times from the results file.
    #
    # returns: nothing.

    my ($_RAhosts, $_RHdata, $_RlastUpdate) = @_;
    my %Hpartitions = get_partitions();

    &AFSConsoleSensors::info(PRT, "Reading accesstimes since last update ($$_RlastUpdate)");

    foreach my $srv (@$_RAhosts) {
        if (! exists $Hpartitions{$srv}) {
            AFSConsoleSensors::warn(PRT, "get_access_times: no partitions defined for $srv, skipping.");
            next;
        }
        my @Apartitions = split(/\s/, $Hpartitions{$srv});
        my $len = scalar @Apartitions;

        # read the data from the result file
        $AFSConsoleConf::access_times_path
            or die "Missing access_times_path variable in config file\n";
        my $rfile = "$AFSConsoleConf::access_times_path/$srv." . today() . ".dat";

        my @a;
        my @b;
        if (open( my $FILE, '<', $rfile)) {
            while (<$FILE>) {
                # rule out comments
                next if ($_ =~ /^\#/);

                # recent data
                push(@a, $_);
            }
            close $FILE;
        } else {
            AFSConsoleSensors::warn(PRT, "get_access_times: $rfile ($!).");
            next;
        }

        foreach my $part (@Apartitions) {
            my (@Areads, @Awrites);
            my ($ravg, $wavg) = (0,0);

            foreach (@a) {
                my @e = split(/\s+/, $_);
                next if ($part ne $e[2]);

                if ($e[3] eq "read") {
                    push(@Areads, $e[5]) if ($e[0] > $$_RlastUpdate);

                    # take the last data point in any case
                    $ravg = $e[5];
                } elsif ($e[3] eq "write") {
                    push(@Awrites, $e[5]) if ($e[0] > $$_RlastUpdate);

                    # take the last data point in any case
                    $wavg = $e[5];
                } else {
                    &AFSConsoleSensors::warn(PRT, "get_access_times: Unexpected line: $_");
                }
            }

            my $key = "$srv/$part";

            if (scalar @Areads > 0 && $$_RlastUpdate > 0) { # take the last data point the first time
                # get the avg
                $ravg = int(avg(@Areads));
                $wavg = int(avg(@Awrites));
                $ravg = 0 if (!$ravg || $ravg < 0);
                $wavg = 0 if (!$ravg || $ravg < 0);
            }
            $$_RHdata{$key} = $$_RHdata{$key} . "$ravg $wavg ";
        }
    }
    $$_RlastUpdate = time;
}

sub prunepartitions {
    my ($_dbh, $_ts) = @_;
    if ($_dbh and $_dbh->do("CALL prunepartitions('$_ts')")) {
        return 0;
    } else {
        return 1;
    }
}

#------------------------------------------------------------------------------
# The sample function
#------------------------------------------------------------------------------

my $lastUpdate = 0;

sub sample {

    # host name
    my $host   = hostname;
    my $host_s = (split(/\./, $host))[0];

    # last run
    my $ts = time;

    AFSConsoleSensors::info(PRT, "Metric partitions started ($ts)");

    # prepare
    my @Ahosts = AFSConsoleSensors::get_servers();

    # collect and analyze data

    # partition info
    my %Hdata;
    get_data(\@Ahosts, \%Hdata, "vos partinfo", \&analyze_partinfo, '');

    # volume info
    get_data(\@Ahosts,
              \%Hdata,
              "vos listvol",
              \&analyze_listvol_partitions_fast,
              '-fast');

    # access times
    get_access_times(\@Ahosts, \%Hdata, \$lastUpdate);

    my $mark = POSIX::SigSet->new(SIGALRM);
    my $action = POSIX::SigAction->new(
        sub {
             AFSConsoleSensors::warn(PRT, "MySQL INSERT timed out");
             die "MySQL INSERT timed out";
        },
        $mark,
    );
    my $oldaction = POSIX::SigAction->new();
    sigaction(SIGALRM, $action, $oldaction);

    eval {
        alarm(210);
        my $count = 0;
        my $dbh = AFSConsoleSensors::open($AFSConsoleSensors::dsn);

        unless ($dbh) {
            AFSConsoleSensors::warn(PRT, "Couldn't connect to MySQL server");
            die "Couldn't connect to MySQL server";
        }
        my $sth = AFSConsoleSensors::prepare($dbh, 'partitions');
        unless ($sth) {
            AFSConsoleSensors:warn("Couldn't prepare collection query");
            die "Couldn't prepare collection query";
        }

        while (my ($key, $data) = each %Hdata) {
            # add the host and the partition to the data
            my ($h, $p) = split(/\//, $key);

            # use the "correct" time stamp
            my ($ts, @Arest) = split(/\s+/, $data);

            $data = "$h $p @Arest";

            # no access time data (mismatch partinfo vs. afsconf.pl)
            if (scalar @Arest != 9) {
                $data = "$h $p @Arest 0 0";
            } else {
                $data = "$h $p @Arest";
            }

            $count += AFSConsoleSensors::add($sth, $host_s, $ts, split(/\s+/, $data));
        }

        AFSConsoleSensors::info(PRT, "Inserted $count samples into MySQL DB");

        if (&prunepartitions($dbh, $ts) == 0) {
            &AFSConsoleSensors::info(PRT, "Pruned stale partition info");
        } else {
            &AFSConsoleSensors::info(PRT, "Couldn't prune stale partition info");
        }

        AFSConsoleSensors::close($dbh);
        alarm(0);
    };
    if($@) {
	&AFSConsoleSensors::warn(PRT,"Exception in ".__FILE__.":".$@)
    }
    alarm(0);
    sigaction(SIGALRM, $oldaction);

    # another after work party ...
    $ts = time;
    AFSConsoleSensors::info(PRT, "Metric partitions done ($ts)");
}

1;
