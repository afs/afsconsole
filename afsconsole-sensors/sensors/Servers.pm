#!/usr/bin/env perl

#------------------------------------------------------------------------------
# Servers      : The servers sensor of the AFS Console.
#
# Initial version : Dan van der Ster IT-DSS/FDO, June 2012.
#------------------------------------------------------------------------------

package Servers;

use strict;
use warnings;

use DBI;
use Sys::Hostname;
use POSIX ':signal_h';

use lib '/usr/share/afsconsole/common';
use AFSConsoleConf;
use AFSConsoleSensors;

#------------------------------------------------------------------------------
# Configuration
#------------------------------------------------------------------------------

use constant SRV => 'servers';


#------------------------------------------------------------------------------
# Subroutines
#------------------------------------------------------------------------------

sub analyze_rxdebug {

    # analyze the output of rxdebug
    # (instance of the Fanalyze function reference parameter)
    #
    # returns: nothing.

    my ($_RHdata, $_pipe, $_host) = @_;

    my $serverConnections;
    my $clientConnections;
    my $idleThreads;
    my $waitedCalls;

    while (<$_pipe>) {
        if (/(\d+) server connections, (\d+) client connections/) {
            $serverConnections = $1;
            $clientConnections = $2;
        } elsif (/(\d+) threads are idle/) {
            $idleThreads = $1;
        } elsif (/(\d+) calls have waited for a thread/) {
            $waitedCalls = $1;
        }
    }

    my $ts  = time;
    $$_RHdata{$_host} = "$ts $idleThreads $serverConnections $clientConnections $waitedCalls";
}


sub get_data {

    # collect the data for all hosts.
    #
    # returns: nothing.

    my ($_RAhosts, $_RHdata, $_cmd, $_RFanalyze, $_ext) = @_;
    my %Hpipes;

    # open the pipes
    foreach my $host (@$_RAhosts) {
        my $cmd  = $_cmd . " $host -noconns -rxstats $_ext";
        my $pipe;
        if(open($pipe, '-|', $cmd)) {
            $Hpipes{$host} = $pipe;
            AFSConsoleSensors::dbug(SRV, "Created pipe for $host");
        } else {
            AFSConsoleSensors::warn(SRV, "Failed to open pipe for $host: $!");
        }
    }

    # read and analyze the data, close the pipe if done
    #foreach my $host (@$_RAhosts) {
    # loop open pipes that were opened, not all hosts (in case pipe not opened)
    foreach my $host (keys %Hpipes) {
        AFSConsoleSensors::dbug(SRV, "Read and analyze data for $host");
        my $pipe = $Hpipes{$host};
        &$_RFanalyze($_RHdata, $pipe, $host);
        AFSConsoleSensors::dbug(SRV, "Close pipe for $host");
        close $pipe;
    }
}


sub today {

    # returns: the date of today as used for the access time files.

    my ($sec, $min, $hrs, $day, $mon, $yr, $dow, $doy, $dst) = localtime(time);
    if ($day < 10) {
        $day = "0" . $day;
    }
    $mon++;
    if ($mon < 10) {
        $mon = "0" . $mon;
    }
    $yr += 1900;
    return "$yr-$mon-$day";
}


sub avg {

   # return the average of an array.

    my ($sum, $ctr) = (0,0);
    my @_A = @_;
    foreach my $i (@_A) {
        return if ($i !~ /^\d+$/);
        $sum += $i;
        $ctr++;
    }
    return $sum/$ctr if ($ctr > 0);
    return;
}



#------------------------------------------------------------------------------
# The sample function
#------------------------------------------------------------------------------

sub sample {

    # host name
    my $host   = hostname;
    my $host_s = (split(/\./, $host))[0];

    # last run
    my $lastUpdate = 0;
    my $ts = time;

    AFSConsoleSensors::info(SRV, "Metric servers started ($ts)");

    # prepare
    my @Ahosts = AFSConsoleSensors::get_servers();

    # collect and analyze data

    # rxdebug info
    my %Hdata;
    get_data(\@Ahosts, \%Hdata, 'rxdebug', \&analyze_rxdebug, '');

    # use Data::Dumper;
    # AFSConsoleSensors::info(SRV, Dumper(\%Hdata));

    my $mark = POSIX::SigSet->new(SIGALRM);
    my $action = POSIX::SigAction->new(
        sub {
             AFSConsoleSensors::warn(SRV, "MySQL INSERT timed out");
             die "MySQL INSERT timed out";
        },
        $mark,
    );
    my $oldaction = POSIX::SigAction->new();
    sigaction(SIGALRM, $action, $oldaction);

    eval {
        alarm(210);
        my $dbh = AFSConsoleSensors::open($AFSConsoleSensors::dsn);

        unless ($dbh) {
            AFSConsoleSensors::warn(SRV, "Couldn't connect to MySQL server");
            die "Couldn't connect to MySQL server";
        }
        my $sth = AFSConsoleSensors::prepare($dbh, 'servers');
        unless ($sth) {
            AFSConsoleSensors:warn("Couldn't prepare collection query");
            die "Couldn't prepare collection query";
        }

        my $count = 0;
        while (my ($key, $data) = each %Hdata) {
            # add the server to the data
            my $server = $key;
            my ($ts, $idleThreads, $serverConnections, $clientConnections, $waitedCalls, $udpInDatagrams, $udpNoPorts, $udpInErrors, $udpOutDatagrams) = split(/\s+/, $data);
            unless (defined($udpInDatagrams)) {
                $udpInDatagrams = $udpNoPorts = $udpInErrors = $udpOutDatagrams = 0;  # no longer collected but need to add to DB
            }
            $count += AFSConsoleSensors::add($sth, $host_s, $ts, $server, $idleThreads, $serverConnections, $clientConnections, $waitedCalls, $udpInDatagrams, $udpNoPorts, $udpInErrors, $udpOutDatagrams);
        }

        AFSConsoleSensors::info(SRV, "Inserted $count samples into MySQL DB");

        AFSConsoleSensors::close($dbh);
        alarm(0);
    };
    if($@) {
	&AFSConsoleSensors::warn(SRV,"Exception in ".__FILE__.":".$@)
    }
    alarm(0);
    sigaction(SIGALRM, $oldaction);

    # another after work party ...
    $ts = time;
    AFSConsoleSensors::info(SRV, "Metric servers done ($ts)");
}

1;
