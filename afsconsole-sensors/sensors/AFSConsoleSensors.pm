#------------------------------------------------------------------------------
# AFSConsoleSensors: Configuration and sub routines used by the AFS Console 
#                    sensors.
#
# Initial version  : Jerome Belleman and Arne Wiebalck IT-DSS/FDO, Oct 2010.
#------------------------------------------------------------------------------

package AFSConsoleSensors;

use strict;
use warnings;

use lib '/usr/share/afsconsole/common';
use AFSConsoleConf;
use AFSConsoleConfWrite;

#------------------------------------------------------------------------------
# Configuration
#------------------------------------------------------------------------------

our $NIGHT = 1200;
our $VOS_TIMEOUT = 450;

use Partitions;
use Volumes;
use Servers;

our %sensors = ( partitions => \&Partitions::sample,
                 volumes => \&Volumes::sample,
                 servers => \&Servers::sample);

sub get_servers {

    # get the servers to monitor
    #
    # returns: an array of host names

    $AFSConsoleConf::get_servers_command
        or die "Missing get_server command setup in config file\n";
    my $h = `$AFSConsoleConf::get_servers_command`
        or die "Couldn't run get_server command\n";

    # Remove the DB servers from the list, as these are 
    # not part of the stats (to avoid warnings).
    # By convention, db servers are called afsdbXX
    my @servers = ();
    foreach my $server (split(/\s+/, $h)) {
        if (rindex($server, "afsdb", 0) == -1) {
            push (@servers, $server);
        }
    }

    return @servers;
}

sub get_partitions {

    # get the partitions to monitor
    #
    # returns: a hash (server_name => partitions string)

    $AFSConsoleConf::get_partitions_command
        or die "Missing get_partitions command setup in config file\n";

    my %Hp;
    my @Ap = split(/\s+/, `$AFSConsoleConf::get_partitions_command`);
    for (@Ap) {
        my ($s, $p) = split(/\//, $_);
        if (exists $Hp{$s}) {
           $Hp{$s} = $Hp{$s} . "$p ";
        } else {
           $Hp{$s} = "$p ";
        }
    }

    return %Hp;
}

#------------------------------------------------------------------------------
# Database setup
#------------------------------------------------------------------------------

my $db_rw_username = $AFSConsoleConfWrite::db_rw_username;
my $db_rw_passwd = $AFSConsoleConfWrite::db_rw_passwd;
my $db_name = $AFSConsoleConf::db_name;
my $db_backend = $AFSConsoleConf::db_backend;
my $db_host = $AFSConsoleConf::db_host;

our $dsn = "DBI:$db_backend:database=$db_name;host=$db_host";

# Opens a DB
# Args:
#  1. Driver string (also specifying the MySQL DB name and host)
# Returns a (possibly undef) DB connection handler
sub open {
    my $_driver = shift;

    # Connect to DB
    my $dbh = DBI->connect($_driver, $db_rw_username, $db_rw_passwd , {PrintError => 0})
        or return;

    return $dbh;
}

# Prepares a statement
# Args:
#  1. The DB handler
#  2. A string, either 'partitions' or 'volumes'
# Returns a statement handler or undef if something went wrong (e.g. anything
# else than 'partitions' or 'volumes' for the 2nd arg).
sub prepare {
    my $_dbh = shift;
    my $_what = shift;

    if (!$_dbh) {
        return;
    }

    my $c;
    if ($_what eq 'partitions') {
        $c = 13;
    } elsif ($_what eq 'volumes') {
        $c = 24;
    } elsif ($_what eq 'servers') {
        $c = 11;
    } else {
        return;
    }

    # Lay out statement with the right number of columns
    my $columns;
    for (my $i = 0; $i < $c; $i++) {
        $columns .= '?, ';
    }
    $/ = ', ';
    chomp($columns);
    $/ = "\n";

    # Prepare statement for sample collection and flush
    if ($_what eq 'volumes') {
        return $_dbh->prepare("CALL addvolumesample($columns)");
    } elsif ($_what eq 'partitions') {
        return $_dbh->prepare("CALL addpartitionsample($columns)");
    } elsif ($_what eq 'servers') {
        return $_dbh->prepare("CALL addserversample($columns)");
    }
}

# Executes the prepared INSERT statement.
# Args:
#  1. Prepared statement handle
#  2. Sensor host having issued the metrics
#  3. Timestamp
#  4. Array of sample values
# Returns:
#     If the query was executed successfully, 0 otherwise
sub add {
    my $_sth = shift;
    #info("add", "@_");
    if ($_sth and $_sth->execute(@_)) {
        return 1;
    }
    info('add',"SQL Error: $DBI::errstr");
    return 0;
}

# Nicely close the DB connection
# Args:
#  1. DB handle
# Doesn't return anything interesting.
sub close {
    my $_dbh = shift;
    if ($_dbh) {
        $_dbh->disconnect;
    }
}

sub prune {
    my ($_dbh, $_ts) = @_;
    if ($_dbh and $_dbh->do("CALL prune('$_ts')")) {
        return 0;
    } else {
        return 1;
    }
}

sub pruneonce {
    my ($_dbh, $_ts, $_server) = @_;
    if ($_dbh and $_dbh->do("CALL pruneonce('$_ts', '$_server')")) {
        return 0;
    } else {
        return 1;
    }
}

#------------------------------------------------------------------------------
# Logging
#------------------------------------------------------------------------------

use Sys::Syslog qw(:standard :macros);

use constant LOGNAME => 'sensor';

sub init {
    openlog(LOGNAME, undef, LOG_LOCAL0);
}

sub info {
    my $_src = shift;
    my $_msg = shift;

    $_msg or print "Missing message\n";
    syslog(LOG_INFO, "[INFO] $_src: $_msg");
}

sub warn {
    my $_src = shift;
    my $_msg = shift;

    $_msg or die "Missing message\n";
    syslog(LOG_WARNING, "[WARNING] $_src: $_msg");
}

sub dbug {
    my $_src = shift;
    my $_msg = shift;

    if (not $conf::debug) {
        return;
    }

    $_msg or die "Missing message\n";
    syslog(LOG_DEBUG, "[DEBUG] $_src: $_msg");
}

1;
