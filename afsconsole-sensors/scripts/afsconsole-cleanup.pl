#!/usr/bin/env perl

use strict;
use warnings;

use Sys::Syslog qw(:standard :macros);
use DBI;

use lib '/usr/share/afsconsole/common/';
use AFSConsoleConf;
use AFSConsoleConfWrite;

use FindBin;
use lib "$FindBin::Bin/../sensors";
use AFSConsoleSensors;

sub logerr($) {
    my $_msg = shift;
    syslog(LOG_WARNING, "[WARNING] cleanup failure: $_msg");
}

my $rc;
my $msg;
my $n = 300; # days to keep

# Setup logging
openlog('cleanup', undef, LOG_LOCAL0);
syslog(LOG_INFO, "[INFO] started cleanup");

# Connect to DB
my $backend = $AFSConsoleConf::db_backend;
my $host = $AFSConsoleConf::db_host;
my $dbname = $AFSConsoleConf::db_name;
my $username = $AFSConsoleConfWrite::db_rw_username;
my $passwd = $AFSConsoleConfWrite::db_rw_passwd;
my $dbh = DBI->connect("DBI:$backend:database=$dbname;host=$host",
                       $username, $passwd,
                       { PrintError => 0 });
if (not $dbh) {
    $msg = $DBI::errstr;
    logerr($msg);
    die "Couldn't connect to DB: $msg\n";
}

my $now = time;
my $cut = $now - $n * 24 * 3600;
my $s = "delete from volumes where timestamp<$cut";
my $sth = $dbh->prepare($s);
$sth->execute();
$sth->finish();
$dbh->disconnect;

# Log
syslog(LOG_INFO, "[INFO] finished cleaning up entries older than $n days");
