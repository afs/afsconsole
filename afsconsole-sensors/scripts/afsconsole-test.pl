#!/usr/bin/env perl

use strict;
use warnings;

use DBI;

use lib '/usr/share/afsconsole/common';
use AFSConsoleConf;
use AFSConsoleConfWrite;

use FindBin;
use lib "$FindBin::Bin/../sensors";
use AFSConsoleSensors;

# Connect to DB
my $backend = $AFSConsoleConf::db_backend;
my $host = $AFSConsoleConf::db_host;
my $dbname = $AFSConsoleConf::db_name;
my $username = $AFSConsoleConfWrite::db_rw_username;
my $passwd = $AFSConsoleConfWrite::db_rw_passwd;
my $dbh = DBI->connect("DBI:$backend:database=$dbname;host=$host",
                       $username, $passwd,
                       { PrintError => 0 });
if (not $dbh) {
    my $msg = $DBI::errstr;
    die "Couldn't connect to DB: $msg\n";
}

print "OK!\n";
$dbh->disconnect;

$username = $AFSConsoleConf::db_ro_username;
$passwd = $AFSConsoleConf::db_ro_passwd;
$dbh = DBI->connect("DBI:$backend:base=$dbname;host=$host",
                       $username, $passwd,
                       { PrintError => 0 });
if (not $dbh) {
    my $msg = $DBI::errstr;
    die "Couldn't connect to DB: $msg\n";
}

print "OK!\n";
$dbh->disconnect;

