#!/usr/bin/env perl

use strict;
use warnings;

use Sys::Syslog qw(:standard :macros);
use DBI;

use lib '/usr/share/afsconsole/common';
use AFSConsoleConf;
use AFSConsoleConfWrite;

use FindBin;
use lib "$FindBin::Bin/../sensors";
use AFSConsoleSensors;

sub logerr($) {
    my $_msg = shift;
    syslog(LOG_WARNING, "[WARNING] afsconsole-average failure: $_msg");
}

my $rc;
my $msg;
my $n = 3; # days to keep at full detail 

# Setup logging
openlog('average', undef, LOG_LOCAL0);
syslog(LOG_INFO, "[INFO] afsconsole-average started");

# Connect to DB
my $backend = $AFSConsoleConf::db_backend;
my $host = $AFSConsoleConf::db_host;
my $username = $AFSConsoleConfWrite::db_rw_username;
my $passwd = $AFSConsoleConfWrite::db_rw_passwd;
my $dbname = $AFSConsoleConf::db_name;
my $dbh = DBI->connect("DBI:$backend:database=$dbname;host=$host",
                       $username, $passwd,
                       { PrintError => 0 });
if (not $dbh) {
    $msg = $DBI::errstr;
    logerr($msg);
    die "Couldn't connect to DB: $msg\n";
}

my $now = time;

# go back to midnight
my @time = localtime();
my $secs = ($time[2] * 3600) + ($time[1] * 60) + $time[0];
my $mn = $now - $secs - 1;

# go back $n days (= upper)
my $upper =  $mn - $n * 86400;

# go back to min date which is not avgd (= lower)
my $s = "select min(timestamp) from volumes where avgd=0";
my $sth = $dbh->prepare($s);
$sth->execute();
my @A = ();
@A = $sth->fetchrow_array;
my $lower = $A[0];

# determine the number of days to average
use POSIX;
my $days = int(ceil(($upper - $lower) / 86400));
syslog(LOG_INFO, "[INFO] $days days to average");

# average
my $d = 0;
while ($d < $days) {

    my ($end, $start) = ($upper - $d * 86400, $upper - ($d + 1) * 86400 + 1);
    syslog(LOG_INFO, "[INFO] Averaging from ".
	   strftime('%d-%b-%Y %H:%M', localtime($start)).
	   " to ".
	   strftime('%d-%b-%Y %H:%M', localtime($end)));
    
    # find the last entry for that day
    my $t;
    $s = "select max(timestamp) from volumes where timestamp>$start and timestamp<$end";
    $sth = $dbh->prepare($s);
    $sth->execute();
    while (@A = $sth->fetchrow_array) {
	$t = $A[0];
    }

    syslog(LOG_INFO, "[INFO] average the precise data ...");
    # get the entries
    my ($id, $nodename, $timestamp, $volumeid, $volumename, $server, $partition,
	$type, $size, $files, $status, $quota, $availability, $currentAccesses,
	$currentReads, $currentWrites, $totalAccesses, $totalReads, $totalWrites,
	$project, $quality, $depth, $flag, $totalRemoteReads, $totalRemoteWrites,
	$recent, $avgd);
    my @B;
    $s = "select * from volumes where timestamp=$t";
    $sth = $dbh->prepare($s);
    $sth->execute();
    while (@A = $sth->fetchrow_array) {
	($id, $nodename, $timestamp, $volumeid, $volumename, $server, $partition,
	 $type, $size, $files, $status, $quota, $availability, $currentAccesses,
	 $currentReads, $currentWrites, $totalAccesses, $totalReads, $totalWrites,
	 $project, $quality, $depth, $flag, $totalRemoteReads, $totalRemoteWrites,
	 $recent, $avgd) = @A;

	# corrections
	$id = "NULL";
	$currentAccesses = int($totalAccesses / ($end - $start));
	$currentReads = int($totalReads / ($end - $start));
	$currentWrites = int($totalWrites / ($end - $start));
	$avgd = 1;

	# remember the averaged row
	push @B, "$id, \'$nodename\', $timestamp, $volumeid, \'$volumename\', \'$server\', \'$partition\', \'$type\', $size, $files, \'$status\', $quota, $availability, $currentAccesses, $currentReads, $currentWrites, $totalAccesses, $totalReads, $totalWrites, \'$project\', \'$quality\', $depth, $flag, $totalRemoteReads, $totalRemoteWrites, $recent, $avgd"; 
    }
    
    # insert the averaged data
    my $counter = 0;
    foreach my $row (@B) { 
	$s = "insert into volumes values($row)";
	$sth = $dbh->prepare($s);
	$sth->execute();
	$counter++;
    }
    syslog(LOG_INFO, "[INFO] precise data averaged");

    # delete the precise data
    syslog(LOG_INFO, "[INFO] delete the precise data between ".
	   strftime('%d-%b-%Y %H:%M', localtime($start)).
	   " and ".
	   strftime('%H:%M', localtime($end)));
    # loop over each second to (a) use the mysql key effectively and (b) avoid the full table lock for 3 hours.
    for (my $i = $start; $i<=$end; $i++) {
        $s = "delete from volumes where timestamp=$i and avgd=0";
        $sth = $dbh->prepare($s);
        $sth->execute();
    }
    #$s = "delete from volumes where timestamp>$start and timestamp<$end and avgd=0";
    #$sth = $dbh->prepare($s);
    #$sth->execute();
    syslog(LOG_INFO, "[INFO] precise data deleted");

    $d++;
}

$sth->finish();
$dbh->disconnect;

syslog(LOG_INFO, "[INFO] afsconsole-average finished");

