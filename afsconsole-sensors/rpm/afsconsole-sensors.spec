Name: afsconsole-sensors 
Version: 1.07
Release: 1
Summary: AFS Console Sensors
Group: Filesystems
License: GPL
BuildArch: noarch

%description
AFS Console sensors probe AFS servers, partitions and volumes before building
samples which they send to a database.

%files
/usr/share/afsconsole/sensors/AFSConsoleSensors.pm
/usr/share/afsconsole/sensors/Partitions.pm
/usr/share/afsconsole/sensors/afsconsole-sensors.pl
/usr/share/afsconsole/sensors/Volumes.pm
/usr/share/afsconsole/sensors/Servers.pm
/usr/share/afsconsole/scripts/*.pl
/etc/init.d/afsconsole-sensors
/etc/cron.d/afsconsole-average
/etc/cron.d/afsconsole-cleanup

%post
if [ -f /etc/syslog.conf ]; then
    echo "Patching syslog.conf"
    echo " " >> /etc/syslog.conf
    echo "# Save AFS Console messages (afsconsole)" >> /etc/syslog.conf
    echo "local0.*						/var/log/afsconsole.log" >> /etc/syslog.conf
    killall -HUP syslogd
elif [ -f /etc/rsyslog.conf ]
then
    echo "Adding rsyslog conf"
    echo "# Save AFS Console messages (afsconsole)" > /etc/rsyslog.d/afsconsole.conf
    echo "local0.*						/var/log/afsconsole.log" >> /etc/rsyslog.d/afsconsole.conf
    /etc/init.d/rsyslog restart
fi

%postun
echo "Cleaning up syslog.conf"
sed '/afsconsole/d' /etc/syslog.conf > /etc/syslog.conf.tmp
mv /etc/syslog.conf.tmp /etc/syslog.conf
rm -f /etc/rsyslog.d/afsconsole.conf

