#!/usr/bin/perl

################################################################################
#
#  Script      : pool_report.cgi
#
#  Original version by Arne Wiebalck IT-FIO/LA 2008
################################################################################

use DBI;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use lib "/opt/perlmodules/";
use Vos;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS
use ConsoleConstants;
require 'ConsoleMySQL.pl';
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

use ConsoleDB;

#-------------------------------------------------------------------------------
#
# Globals
#
#-------------------------------------------------------------------------------

my $debug = 0;

my %Hparts = ();
my @Apools = ();
my %Hpools2parts  = ();
my %Hpartss2pools = ();

&make_imbed_grafx(\$graphs_icons_ktimemon, "icons/ktimemon");
#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub getPools() {
	
	# get all defined pools from afsadmin.cf and fill
	#  - a hash srv/part -> pools, called %Hpartss2pools
	#  - a hash pool -> (srv/part)s, called %Hpools2parts
	#  - an array of all pools (sorted and uniqified), called @Apools
	
	open AA, "/afs/cern.ch/project/afs/adm/afsadmin.cf" || 
	    die "Unable to open afsadmin.cf: $!";

	my $flag = 0;
	my @Atmp = ();
	my @Atmp2 = ();
	my $srvprt;

	while (<AA>) {

		# ff to the right section
		if (/\[PARTITIONS\]/) {
			$flag = 1;
			next;
		}
		if (/\[\w+\]/ && $flag) {
			last;
		}
		next unless ($flag);
		next if (/\#/);

		# now we have a "partition pool options" line
		@Atmp = split /\s+/, $_;

		# partition with no pools 
		next if ((1 == @Atmp) || ($Atmp[1] =~ /=/));

		# now we have a partition which has pools assigned
		foreach $e (@Atmp) {
			if ($e =~ /\//) {     # srv/part
				$srvprt = $e;
				next;
			}
			next if ($e =~ /=/);  # option
			# $e is a pool now
			print "$e <BR>\n" if ($debug);
			$Hparts2pools{$srvprt} .= "$e ";
			$Hpools2parts{$e} .= "$srvprt ";
			push @Atmp2, $e;
		}
		# print "$_ <BR>\n" if ($debug);		
	}

	close AA;

	my %Hseen = ();
	@Apools = grep {!$seen{$_}++} sort @Atmp2;

	if ($debug) {

		while (my ($key, $val) = each %Hparts2pools) {
			print "$key: $val <BR>\n";
		} 

		while (my ($key, $val) = each %Hpools2parts) {
			print "$key: $val <BR>\n";
		} 

		print "@Apools <BR>\n";
	}

	return;
}

sub getPartitionData() {

	# get all data about the partitions 
	# from the Lemon (read in the whole
	# table)	

	my @report = ConsoleDB::partitions_poolreport();
	foreach my $row (@report) { 
		my ($srv,$prt,@data) = @$row;
		$Hparts{"$srv/$prt"} = "@data";
	}

	if ($debug) {
		while (my ($key, $val) = each %Hparts) {
			print "$key: $val <BR>\n";
		} 
	}

	return;
}


sub main_frame{
	
	my @Aparts = ();
	my @ApartData = ();
	my ($cap, $free, $used, $vols, $pused) = (0,0,0,0,0);
       
	foreach my $pool (@Apools) {

		my ($sum_cap, $sum_free, $sum_used, $sum_vols, $sum_pused) = (0,0,0,0, 0);
		&pool_bar($pool);
		&head_table();

		# get the partitions
		@Aparts = split /\s+/, $Hpools2parts{$pool};
		foreach my $part (@Aparts) {

			if (exists $Hparts{$part}) {  # pools still assigned, but partition 
				                      # may be gone already 

				@ApartData = split /\s+/, $Hparts{$part};
			
				$cap    = sprintf("%.2f", $ApartData[1] / 1024 / 1024); #GB
				$free   = sprintf("%.2f",($ApartData[1]-$ApartData[2]) / 1024 / 1024); 
				$used   = sprintf("%.2f",$ApartData[2] / 1024 / 1024);
				$vols   = $ApartData[3];
				$pused = sprintf("%.0f", $used / $cap * 100);

			} else {

				($cap, $free, $used, $vols, $pused) = (0,0,0,0,0);	
			}

			#print "$part ($cap, $free, $used, $comm, $vols, $pused, $palloc) <BR>";
			&row_table($pool, $part, $cap, $free, $used, $vols, $pused);
			$sum_cap  += $cap;
			$sum_free += $free;
			$sum_used += $used;
			$sum_vols += $vols;
		}
		my $noParts = @Aparts;

		if (0 < $sum_used) {
			if ($sum_cap > 0) {
				$sum_pused = sprintf("%.0f", $sum_used / $sum_cap * 100);
			} else {
				$sum_pused = sprintf("n.a.");
			}
		} else {
			$sum_pused = 0;
		}

		&summary_row($pool, $noParts, $sum_cap, $sum_free, $sum_used, $sum_vols, $sum_pused);
		&end_table();
		&pool_bar_closed();
	}
}

sub pool_bar() {

	my $pool = shift;
	print "<div align=center><table width='100%'><tr><td>
				<table border='0' width='96%' cellspacing ='1' cellpadding='3' border ='0'>
				<tr>
				<th><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
				<tr>
				<td bgcolor='#5FA6D7'>
				<img src='$graphs_icons_ktimemon'>
				<font size=2 face='verdana, arial, helvetica'>
				<b><font size=2 face='verdana, arial, helvetica' color='#000000'>AFS Disk Pool \"$pool\"</font></b>
				</font>
				</td>
				</tr>
				</div></table></th>
				</tr>
				</td>
				</tr>
				</table></table></div>";
	print "<div align=center><table width='100%'><tr>	<td>
				<table border='0' width='96%' cellspacing ='1' cellpadding='3' border ='0'>
				<tr>
				<th><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
				<tr>
				<td bgcolor='#ffffcc'>
				<div align=left>
				<font size=2 face='verdana, arial, helvetica' color='#465e90'>";
	return;
}

sub pool_bar_closed() {
	
	print "</font></div>
	       </td></tr>
	       </table></div>
	       </th></tr>
	       </table></td>
	       </tr></table></div>";
	return;
}


sub head_table{
	print "
<div align=center><table border='0' width='75%'>
<tr>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Pool</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Partition</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Disk Cap (GB)</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Free (GB)</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Used (GB)</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Number of Vols</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>% Used</b></font>
</th>
</tr>";
	return 1;
}

sub row_table{
	
	my ($pool, $part, $cap, $free, $used, $vols, $pused) =  @_;
	
	my $align = "center";
	print "<tr>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$pool
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$part
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$cap
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$free
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$used
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$vols
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$pused
</font></td>
</tr>";
	return 1;
}

sub summary_row() {

		my ($pool, $part, $cap, $free, $used, $vols, $pused) =  @_;
	
	my $align = "center";
	print "<tr>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>Summary</b>
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$part</b>
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$cap</b>
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$free</b>
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$used</b>
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$vols</b>
</font></td>

<td align=$align bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$pused</b>
</font></td>
</tr>";
	return 1;
}

sub end_table{
	print "</table></div>";
	return 1;
}


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query=new CGI;
print $query->header();
&heading("Pool Report");
&getPools();
&getPartitionData();
&main_frame();
&footer_simple(0);
