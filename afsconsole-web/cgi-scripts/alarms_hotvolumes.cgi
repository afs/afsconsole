#!/usr/bin/env perl

#------------------------------------------------------------------------------
# alarm_hotvolumes.cgi : for each slow server, get most active volume
#
#------------------------------------------------------------------------------

use DBI;
use JSON;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use File::stat;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
use ConsoleHTML;
use ConsoleUtils qw(today);
use ConsoleDB;
use strict;

#-------------------------------------------------------------------------------
#
# Globals
#
#-------------------------------------------------------------------------------

my $slow_server_json_dir = '/afs/cern.ch/project/afs/dev/monitoring/console/data_files/hotvolumes'; # where does each 'slow' server writes their most busy volume?
my $slow_server_json_time = 600;  # how long do we consider that data to be valid for?

#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub get_data {
    my $now = time();
    my @out;
    for my $jfile (glob("$slow_server_json_dir/*.json")) {
	if (stat($jfile)->mtime >= ($now - $slow_server_json_time)) {
	    if ( open(my $fh, '<', $jfile)) {
	        my $j_data = decode_json(<$fh>);
	        close($fh) or warn "cannot close $jfile: $!";
	        push(@out, $j_data);
            } else {
		warn "cannot open $jfile: $!";
		next;
	    }
	}
    }
    return \@out;
}

#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $outref = get_data();
my $query = new CGI;
my $do_json = $query->Accept("application/json");

# Chrome says it accepts *anything* with 0.8 ..
if ($do_json >= .9) {
    print $query->header(-type=>"application/json",
                         -content_disposition=>"attachment; filename=alarms_hotvolumes.json");
    print encode_json($outref);
} else {
    print $query->header();
    &heading( "Hottest Volume for each overloaded Server");
    &startTableTable( "Hottest Volume for each overloaded Server", 0 );
    # ugly stub. Else would need a new modules/ConsoleHTML.pm function to format this (pre-CSS)
    print "<table border='2'>\n<tr>\n<th>Server</th><th>Latency</th><th>Latency badness</th><th>Volume Name</th><th>Account</th></tr>\n";
    for my $hot (@$outref) {
	print "<tr>\n";
	for my $k ('server', 'latency_ms', 'badness', 'volume', 'account') {
	    print "<td>".$$hot{$k}."</td>";
	}
	print "</tr>\n";
    }
    print "</table>\n";
    print "<p><small>(busiest volume each, as reported by AFS fileservers that are above their latency target. Data is at most ${slow_server_json_time} seconds old)</small></p>\n";
    &endTableTable();
    &footer_simple();
}

1;
