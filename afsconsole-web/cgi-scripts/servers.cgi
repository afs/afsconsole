#!/usr/bin/perl

use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use lib "/opt/perlmodules";
use Vos;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS
use ConsoleConstants;
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';

################MODULES###########################
&make_imbed_grafx(\$graphs_icons_xmag, "icons/xmag");
&make_imbed_grafx(\$graphs_icons_kfm_home, "icons/kfm_home");
&make_imbed_grafx(\$graphs_servers_mix, "servers_mix");
&make_imbed_grafx(\$graphs_servers_switch, "servers_switch");
&make_imbed_grafx(\$graphs_servers_acc, "servers_acc");
&make_imbed_grafx(\$graphs_servers_disk, "servers_disk");
&make_imbed_grafx(\$graphs_accessTimes, "accessTimes");
&make_imbed_grafx(\$graphs_store_bytes, "store_bytes");
&make_imbed_grafx(\$graphs_fetch_bytes, "fetch_bytes");
&make_imbed_grafx(\$graphs_servers_files, "servers_files");
&make_imbed_grafx(\$graphs_connections, "connections");
&make_imbed_grafx(\$graphs_threads, "threads");
&make_imbed_grafx(\$graphs_srvconnections, "srvconnections");
&make_imbed_grafx(\$graphs_alarms, "alarms");
&make_imbed_grafx(\$graphs_users_data, "users_data");
&make_imbed_grafx(\$graphs_atlas_data, "atlas_data");
&make_imbed_grafx(\$graphs_cms_data, "cms_data");
&make_imbed_grafx(\$graphs_lhcb_data, "lhcb_data");

sub heading{
print "<html>";
print "<head><title>CERN AFS Console View - Servers Graphics</title>";
print "</head>";
print "<body bgcolor=#ffffff font=arial, helevetica>";
print "<HTML><table width=100% cellpadding=1 border=0 cellspacing=0 bgcolor=#000000><tr><td>";
print "<HTML><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeef><tr><td>";
print "<A href='index.cgi'><img border='0'
src='$graphs_icons_kfm_home'></A> &nbsp&nbsp&nbsp <font size=3 face='verdana, arial, helvetica'>CERN AFS Console View</font>";
print "</table></tr></td>";
print "</table></tr></td>";
print "<HR></HR>";
return 1;
}


sub main_frame{
my $listsrv = &get_all_servers();
$true=1;
@servers = split /\s+/, $listsrv;
#while($true eq 1){
#	if($listsrv=~/(\w+)(\s)(.*)/){
#		$listsrv = $3;
#		$server=$1;
#		push @servers, $server;
#	}elsif($listsrv=~/(\w+)/){
#		$true = 0;
#	}
#}

print "
<div align=center><table width='100%'>
<tr>
	<td>
			<table border='0' width='100%' cellspacing ='1' cellpadding='3' border ='0'>
			<tr>
			<th><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr>
			<td bgcolor='#5FA6D7'>
			<img src='$graphs_icons_xmag'>
			<font size=2 face='verdana, arial, helvetica'>
			<b><font size=2 face='verdana, arial, helvetica' color='#000000'>Additional Server Info</font></b>
			</font>
			</td>
			</tr>
			</div></table></th>
			</tr>

			<tr>
			<td><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr><td bgcolor='#FFFFCC'>
			<div align=center>
			<font face='verdana, arial, helvetica' size=2 color='#465e90'>";
	
			my $cols = 15;
			my $rows = @servers / $cols + 1;
					
		        print "<table width='100%'>"; 
			print "<tr>";
			my ($x, $y) = (0, 0);
			foreach (@servers) {
				print "<td><A href=servers_data.cgi?=$_>$_</A></td>\n";
				$x++;
				if (0 == ($x % $cols)) {
					$y++;
					print "</tr><tr>"
				}
 			}
			print "</tr>\n</table>\n";
	

			print "</font>
			</div>
			</td>
			</tr>
			</table></div>
	</td>
	</tr>
	</table></table></div>";

my $ls = fullpath("ls", "/usr/afsws/bin:/usr/afsws/etc:/usr/sbin:/usr/sue/bin/:/bin");
my $grep = fullpath("grep", "/usr/afsws/bin:/usr/afsws/etc:/usr/sbin:/usr/sue/bin/:/bin");
my $tail = fullpath("tail", "/usr/afsws/bin:/usr/afsws/etc:/usr/sbin:/usr/sue/bin/:/bin:/usr/bin");

print "
<div align=center><table>
<tr>
	<td>
			<table border='0' width='100%' cellspacing ='1' cellpadding='3' border ='0'>
			<tr>
			<th><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr>
			<td bgcolor='#5FA6D7'>
			<img src='$graphs_icons_xmag'>
			<font size=2 face='verdana, arial, helvetica'>
			<b><font size=2 face='verdana, arial, helvetica' color='#000000'>Servers Graphics Report</font></b>
			</font>
			</td>
			</tr>
			</div></table></th>
			</tr>

			<tr>
			<td><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr><td bgcolor='#FFFFCC'>
			<div align=center>
			<font face='verdana, arial, helvetica' size=1 color='#0000ff'>
				<img src='$graphs_servers_mix'>
                       
                        <a
                        onmouseover=\"austausch1.src=\'$graphs_servers_acc\';\"
                        onmouseout=\"austausch1.src=\'$graphs_servers_switch\';\"
                        <img
                        src=\'$graphs_servers_switch\' border=\"0\"
                        name=\"austausch1\">
                        </a>

				<img src='$graphs_accessTimes'>
				<img src='$graphs_servers_disk'>
				<img src='$graphs_store_bytes'>
				<img src='$graphs_fetch_bytes'>
				<img src='$graphs_servers_files'>
				<img src='$graphs_connections'>
				<img src='$graphs_threads'>
				<img src='$graphs_srvconnections'>
				<img src='$graphs_alarms'><br>
				<img src='$graphs_users_data'>
				<img src='$graphs_atlas_data'>
				<img src='$graphs_cms_data'>
				<img src='$graphs_lhcb_data'>
    			</div>
			</font>
			</td>
			</tr>
			</table></div>
	</td>
	</tr>
	</table></table></div>";

my @servers;
my $listsrv = &get_all_servers();
my $true=1;
while($true){
	if($listsrv=~/(\w+)(\s)(.*)/){
		$listsrv = $3;
		$server=$1;
		push @servers, $server;
	}else{
		$true=0;
	}
}
return 1;
}

sub foot{
print "<HR></HR>
<body bgcolor=#ffffff font=arial, helevetica>
<div align=center><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeee><tr><td>
<div align=right><font face='verdana, arial, helvetica' size=2>AFS Service - CERN IT</font></div>
</table></tr></td></div>";
return 1;
}

#****************************************************************************
############################################################################
my $query=new CGI;
print $query->header(-refresh=>120);
my $val = heading();
$val = main_frame();
&footer_simple(0);
