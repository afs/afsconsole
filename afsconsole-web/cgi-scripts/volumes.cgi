#!/usr/bin/env perl

#------------------------------------------------------------------------------
# volumes.cgi : entry page for volume information
#
# Initial version : Arne Wiebalck CERN IT-FIO/LA, Aug 2007.
#------------------------------------------------------------------------------

use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use DBI;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

sub heading2() {
	
	&make_imbed_grafx(\$graphs_icons_kfm_home, "icons/kfm_home");
	print "<html>";
	print "<head><title>CERN AFS Console View - Volumes</title>";
	print "</head>";
	print "<body bgcolor=#ffffff font=arial, helevetica>";
	print "<HTML><table width=100% cellpadding=1 border=0 cellspacing=0 bgcolor=#000000><tr><td>";
	print "<HTML><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeef><tr><td>";
	print "<A href='index.cgi'><img border='0' src='$graphs_icons_kfm_home'></A> &nbsp&nbsp&nbsp <font size=3 face='verdana, arial, helvetica'>CERN AFS Console View</font>";
	print "</table></tr></td>";
	print "</table></tr></td>";
	print "<HR></HR>";

	return 1;
}


sub main_frame() {
	
	print " <div align=center>";

	        # all items and footer
	        print "
                <table width=100% border=0>
                   <tr>
	           <td>";
                   
                      # item: hottest volumes
	              print "
		      <table border='1' width='98%' cellspacing ='0' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>
			 <tr>
			       <th><div align=left>
                               <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			          <tr>
			          <td bgcolor='#5FA6D7'>
			             <font size=2 face='verdana, arial, helvetica'>
			             <b><font size=2 face='verdana, arial, helvetica' color='#000000'>";

                                     # item header
	                             print "
                                     <HTML>
                                     <table width=100% cellpadding=2 border=0 cellspacing=1>
                                        <tr>
                                        <td>
                                            <font size=2 face='verdana, arial, helvetica'><b>Hottest volumes</b></font> 
                                         </td>
                                         </tr>
                                     </table>
                                     </font></b>
			          </td>
			          </tr>
			          </div>
                               </table>
                               </th>
		         </tr>

		         <tr>
		         <td>
                            <div align=left>
                            <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
			       <tr bgcolor='#FFFFCC'>
                               <td width='2%'><font face='verdana, arial, helvetica' size=2 color='#FFFFCC'>invisible</font></td>
                               <td bgcolor='#FFFFCC' width='60%'>
			          <div align=left>
			          <font face='verdana, arial, helvetica' size=3 color='#465e90'>";
	
	                          # item action
 	                          my $action = 'volumes_hottest.cgi';
  	                          my $currDate = &today();
	                          my $currTime = &now();
	                          print " 
                                  <form method='GET' action=$action name='entries' target='_parent'>
   	 	             
                                  Get the <input type='text' name='entries' value='20' maxlength=4 size=4> top volumes in terms of 

                                  <select name=\"sortedBy\" size=\"1\" >
				  <option value=\"size\">Volume Size</option>
				  <option value=\"files\">Number of Files</option>
				  <option value=\"quota\">Quota</option>
				  <option value=\"currentaccesses\">Current Accesses</option>
				  <option value=\"currentreads\">Current Reads</option>
				  <option value=\"currentwrites\">Current Writes</option>
				  <option value=\"currentreads + currentwrites\" selected=\"selected\">Current Reads+Writes</option>
	  			  <option value=\"totalaccesses\">Total Accesses</option>
				  <option value=\"totalreads\">Total Read Operations</option>
				  <option value=\"totalwrites\">Total Write Operations</option>

			          </select>";
  
                                  # get the current list of servers and partitions
	                          @prod_servers_S = split /\s+/, &get_all_servers();
	                          push( @prod_servers_S, split /\s+/, &get_all_partitions());	                        
                               	  @prod_servers = sort @prod_servers_S;

                                  print "    	                          
                                  for server/partition
                                  <select name=\"server\" size=\"1\" >
                                  <option value=\"size\">all</option>";
                                  foreach (@prod_servers) {
					  
					  # exclude afsdbX
					  unless (/^afsdb/) {
						  print "<option value=\"$_\">$_</option>"; 
					  }
				  }
	                          print "</select>
                               </td>
                               <td>
                                 
                               <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>

                                 <form method='GET' action=$action name='now' target='_parent'>

                                 <td align=left width='40%'>

                                 <input type='radio' name='now' value='mostRecent' checked='checked'>
                                 <font color='#465e90'>Most recent</font><br>

                                 <input type='radio' name='now' value='today'>
                                 <font color='#465e90'>Today</font><br>

                                 <input type='radio' name='now' value='interval'>
                                 <font color='#465e90'>Interval</font></td>

                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                               </tr>
                               
                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>From:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='start_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='start_time' value='00:00:00' maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
                               </tr> 

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>To:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='end_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='end_time' value=$currTime maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
  		               </tr>
 
                               <tr bgcolor='#FFFFCC'>
                               <div align=center>
                                 <td></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'><br><br></td>
                                 <td align=center><input type='submit' value='Submit'></td>
                               </tr>
                                  
                                  </form><p>
                                  </div>
 
	                      </tr>
	                    </table>

                               </td>
                               </tr>

                             </table>
                         </td>
	                 </tr>
                      </table>";
                      # end item hottest volumes                          

	print "<br>";

                      # item: volume's info
	              print "
		      <table border='1' width='98%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>
			 <tr>
			       <th><div align=left>
                               <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			          <tr>
			          <td bgcolor='#5FA6D7'>
			             <font size=2 face='verdana, arial, helvetica'>
			             <b><font size=2 face='verdana, arial, helvetica' color='#000000'>";

                                     # item header
	                             print "
                                     <HTML>
                                     <table width=100% cellpadding=2 border=0 cellspacing=1>
                                        <tr>
                                        <td>
                                            <font size=2 face='verdana, arial, helvetica'><b>Volume's info</b></font> 
                                         </td>
                                         </tr>
                                     </table>
                                     </font></b>
			          </td>
			          </tr>
			          </div>
                               </table>
                               </th>
		         </tr>

		         <tr>
		         <td>
                            <div align=left>
                            <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
			       <tr bgcolor='#FFFFCC'>
                               <td width='2%'><font face='verdana, arial, helvetica' size=2 color='#FFFFCC'>invisible</font></td>
                               <td bgcolor='#FFFFCC' width='60%'>
			          <div align=left>
			          <font face='verdana, arial, helvetica' size=3 color='#465e90'>";
	
	                          # item action
 	                          my $action = 'volumes_info.cgi';
  	                          my $currDate = &today();
	                          my $currTime = &now();
	                          print " 
                                  <form method='GET' action=$action name='info' target='_parent'>
   	 	             
                                  Get info about volume <input type='text' name='volumeName' maxlength=32 size=32> 

                               </td>
                               <td>
                                 
                               <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>

                                 <form method='GET' action=$action name='now' target='_parent'>

                                 <td align=left width='40%'>

                                 <input type='radio' name='now' value='mostRecent' checked='checked'>
                                 <font color='#465e90'>Most recent</font><br>

                                 <input type='radio' name='now' value='today'>
                                 <font color='#465e90'>Today</font><br>

                                 <input type='radio' name='now' value='interval'>
                                 <font color='#465e90'>Interval</font></td>

                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>

                               </tr>
                               
                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>From:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='start_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='start_time' value='00:00:00' maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
                               </tr> 

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>To:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='end_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='end_time' value=$currTime maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
  		               </tr>
 
                               <tr bgcolor='#FFFFCC'>
                               <div align=center>
                                 <td></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td align=center><input type='submit' value='Submit'></td>
                               </tr>
                                  
                                  </form><p>
                                  </div>
 
	                      </tr>
	                    </table>

                               </td>
                               </tr>

                             </table>
                         </td>
	                 </tr>
                      </table>";
                      # end item volume's info    

	print "<br>";

                      # item: plot data
	              print "
		      <table border='1' width='98%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>
			 <tr>
			       <th><div align=left>
                               <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			          <tr>
			          <td bgcolor='#5FA6D7'>
			             <font size=2 face='verdana, arial, helvetica'>
			             <b><font size=2 face='verdana, arial, helvetica' color='#000000'>";

                                     # item header
	                             print "
                                     <HTML>
                                     <table width=100% cellpadding=2 border=0 cellspacing=1>
                                        <tr>
                                        <td>
                                            <font size=2 face='verdana, arial, helvetica'><b>Plot volume's data</b></font> 
                                         </td>
                                         </tr>
                                     </table>
                                     </font></b>
			          </td>
			          </tr>
			          </div>
                               </table>
                               </th>
		         </tr>

		         <tr>
		         <td>
                            <div align=left>
                            <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                  <td>
                                    <br>
                                    <font face='verdana, arial, helvetica' size=3 color='#465e90'>Plot data for volume</font>
                                 </td>";

	                          # item action
 	                          my $action = 'volumes_plot.cgi';
  	                          my $currDate = &today();
	                          my $currTime = &now();
	                          print " 

                                  <form method='GET' action=$action name='plot' target='_parent'>

                                 <td colspan=2><br><input type='text' name='volumeName' maxlength=32 size=32></td>
                                 <td></td>
                               </tr>   
 
			       <tr bgcolor='#FFFFCC'>     
                               <td width='2%'><font face='verdana, arial, helvetica' size=2 color='#FFFFCC'>invisible</font></td>
                              <td align='left' width='12%'>
			          <div align=left>
			          <font face='verdana, arial, helvetica' size=3 color='#465e90'>
       


                                                <form method='GET' action=$action name='files' target='_parent'>
                                                <input type='checkbox' name='files' checked value='on'>
                                                <font color='#465e90'>Files</font><br>

                                                <form method='GET' action=$action name='quota' target='_parent'>
                                                <input type='checkbox' name='quota' checked value='on'>
                                                <font color='#465e90'>Quota</font><br>

                                                <form method='GET' action=$action name='size' target='_parent'>
                                                <input type='checkbox' name='size' checked value='on'>
                                                <font color='#465e90'>Size</font><br>
                               </td>
  
                               <td align='left' width='15%'>

                                                <form method='GET' action=$action name='current_accesses' target='_parent'>
                                                <input type='checkbox' name='current_accesses' disabled>
                                                <font color='#465e90'>Current Accesses</font><br>

                                                <form method='GET' action=$action name='current_reads' target='_parent'>
                                                <input type='checkbox' name='current_reads' disabled>
                                                <font color='#465e90'>Current Reads</font><br>

                                                <form method='GET' action=$action name='current_writes' target='_parent'>
                                                <input type='checkbox' name='current_writes' disabled>
                                                <font color='#465e90'>Current Writes</font><br>

                                                <form method='GET' action=$action name='current_all' target='_parent'>
                                                <input type='checkbox' name='current_all' checked value='on' >
                                                <font color='#465e90'>Current All</font><br>
                               </td>

                               <td align='left' width='33%'>

                                                <form method='GET' action=$action name='total_accesses' target='_parent'>
                                                <input type='checkbox' name='total_accesses' disabled>
                                                <font color='#465e90'>Total Accesses</font><br>

                                                <form method='GET' action=$action name='total_reads' target='_parent'>
                                                <input type='checkbox' name='total_reads' disabled>
                                                <font color='#465e90'>Total Reads</font><br>

                                                <form method='GET' action=$action name='total_writes' target='_parent'>
                                                <input type='checkbox' name='total_writes' disabled>
                                                <font color='#465e90'>Total Writes</font><br>

                                                <form method='GET' action=$action name='total_all' target='_parent'>
                                                <input type='checkbox' name='total_all' checked value='on'>
                                                <font color='#465e90'>Total All</font><br>
                               </td>
                               <td>
                                 
                               <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
                               
                               <tr bgcolor='#FFFFCC'>
                                 <td></td>

                                 <form method='GET' action=$action name='now' target='_parent'>

                                 <td align=left width='40%'>

                                 <input type='radio' name='now' value='today' checked='checked'>
                                 <font color='#465e90'>Today</font><br>

                                 <input type='radio' name='now' value='interval'>
                                 <font color='#465e90'>Interval&nbsp;&nbsp;</font></td>

                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                               </tr>

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>From:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='start_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='start_time' value='00:00:00' maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
                               </tr> 

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>To:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='end_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='end_time' value=$currTime maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
  		               </tr>
 
                               <tr bgcolor='#FFFFCC'>
                               <div align=center>
                                 <td></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td align=center><input type='submit' value='Submit'></td>
                               </tr>
                                  </form><p>
                                  </div>
 
	                      </tr>
	                    </table>

                               </td>
                               </tr>

                             </table>
                         </td>
	                 </tr>
                      </table>";
                      # plot data

                print "
                </table>
                </div>";
	return 1;
}


sub foot() {

	print "<HR></HR><body bgcolor=#ffffff font=arial, helevetica><div align=center><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeee><tr><td><div align=right><font face='verdana, arial, helvetica' size=2>AFS Service - CERN IT</font></div></table></tr></td></div>";
	return 1;
}

#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query=new CGI;
print $query->header();
&heading2( "Volume History", $Tonly );
$val = main_frame();
&footer_simple(0);
