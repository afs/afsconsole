#!/usr/bin/perl

################################################################################
#
#  Script      : proj_detail.cgi
#
#                Display information about a project
#
#  Change log:   
#              27.11.2007 awi Change to afssp_read account
#
#              25.04.2007 awi Outsourced ORACLE & constants & HTML (partly) & 
#                             Utils 
#
#              23.04.2007 awi Added a column for the access time
#                             during the last hour per partition
#
#  Original version by Mario Bolivar, CERN IT/FIO 2006
################################################################################

use CGI ':standard';
use DBI;
use CGI::Carp qw(fatalsToBrowser);
use lib "/opt/perlmodules/";
use Vos;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
require 'ConsoleMySQL.pl';
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

use ConsoleDB;

# Allowed pools


#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub getPools() {
    my @Apools = ();
    open(FILE, "<", $ConsoleConstants::AFS_STAT) or die "Could not open AFS.stat";
    while(<FILE>) {
        if (/Disk pool status: (\w+) .*/) {
            push @Apools, $1;
        }
    }
    close(FILE);
    return @Apools;
}

sub main_frame{

	&make_imbed_grafx(\$graphs_icons_ktimemon, "icons/ktimemon");

        # activate this again in order to see only the sorted volumes table
	read(STDIN,$encoded,$ENV{'CONTENT_LENGTH'});
	$id = $ENV{'QUERY_STRING'};

	# sanitize input
	if ($id !~ /^[\w\_]+$/) {
	    print "Pool name \"$id\" not valid.\n";
	    &footer(0, time, $Tonly);
	    exit 0;
	}
	my $flag = 0;
	foreach my $p (&getPools()) {
	    $flag = 1 if ($p =~ /([A-Z]?)$id\_?[pqsu]?/);
	}
	if (0 == $flag) {
	    print "Pool \"$id\" not known.\n";
	    &footer(0, time, $Tonly);	    
	    exit 0;
	}

	my @projects;
	open PROJ, '<', $ConsoleConstants::PROJECTS_FILE or warn "Could not open $ConsoleConstants::PROJECTS_FILE: $!";
	while(<PROJ>){
		@projects = split " ", $_;
	}
	close PROJ;

	open FILE, '<', $ConsoleConstants::AFS_STAT or warn "Could not open $ConsoleConstants::AFS_STAT: $!";
	my ($print,$count,$found)=(0,0,0);
	my ($curr,$aux);
	while(<FILE>){
		if ($_=~/Disk pool status: ([A-Z]?)$id\_?(.*)/){
			$found = 1;
		}
		if ($_=~/Disk pool status: ([A-Z]?)$id\_?(.*)/  && $found eq 1){
			($print,$found)=(0,1);
			$proj = $1;
			if ($count>0){ #closes the table
				&end_table;
				print "</font></div>
			</td></tr>
			</table></div>
			</th></tr>
			</table></td>
			</tr></table></div>";
			}
			print "<div align=center><table width='100%'><tr><td>
		<table border='0' width='100%' cellspacing ='1' cellpadding='3' border ='0'>
		<tr>
		<th><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		<tr>
		<td bgcolor='#5FA6D7'>
		<img src='$graphs_icons_ktimemon'>
		<font size=2 face='verdana, arial, helvetica'>
		<b><font size=2 face='verdana, arial, helvetica' color='#000000'>AFS Service for $_</font></b>
		</font>
		</td>
		</tr>
		</div></table></th>
		</tr>
		</td>
		</tr>
		</table></table></div>";
			print "<div align=left><table width='100%'><tr>	<td>
		<table border='0' width='100%' cellspacing ='1' cellpadding='3' border ='0'>
		<tr>
		<th><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		<tr>
		<td bgcolor='#ffffcc'>
		<div align=left>
		<font size=2 face='verdana, arial, helvetica' color='#465e90'>";
			head_table();
			$curr=$_;
			$count++;
			$print=1;
		}#closes the if($_=~/Disk pool status: ([A-Z]?)$id\_?(.*)/  && $found eq 1)
		    if($print eq 1 && $found eq 1){
			    print " " if($_=~/disk cap(.*)/);
			    row_table($_) if($_=~/afs(.*)/);
		    }
		if ($_=~/Disk pool status: (.*)/ && $found eq 1){
			my @trash = split " ",$_;
			if($trash[3] !~ /$id\_?(.*)/){
				&end_table;
				print "</font></div>
			</td></tr>
			</table></div>
			</th></tr>
			</table></td>
			</tr></table></div>";
				$found = 0;
				next;
			}
		}
	}
	print "</table></table>";
	return 1;
}

sub head_table{
	print "
<div align=center><table border='0' width='75%'>
<tr>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Server</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Disk Cap (GB)</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Free (GB)</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Used (GB)</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Alloc (GB)</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Number of Vols</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>% Used</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>% Allocated</b></font>
</th>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Accesses (Millions)</b></font>
</th>

<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Access Time 64kB (ms)</b></font>
</th>

</tr>";
	return 1;
}

sub row_table{
	my $line = @_;
	my ($server,$disk,$free,$used,$alloc,$vol,$pused,$palloc) = split " " , $_;
	$server =~ /(.*)\/(.*)/;
	my ($srv,$part) = ($1,$2);
	my $accs = ConsoleDB::volumes_projtotalaccesses($server);
	$accs = $accs/1000000;

	$accessTimes = sprintf( "%.2f", &get_accessTimes( $srv, $part ) / 1000 );
	
	$used = sprintf( "%.1f", $used / 1024 );
	$alloc = sprintf( "%.1f", $alloc / 1024 );

	print "
<tr>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<A href=servers_data.cgi?=$srv><font size=2 face='verdana, arial, helvetica' color=#465e90>
$server</font></A>
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$disk
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$free
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$used
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$alloc
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$vol
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$pused
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$palloc
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$accs
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$accessTimes
</font></td>
</tr>";
	return 1;
}

sub proj_vols{
	
	my ($parameter, $srv, $orderBy, $entries);

	$entries  = 25;
	$Tonly    = 0;	
	$sortedBy = "totalaccesses";

	foreach ( param() ) {
		
		if (param( $_ ) =~ /(.*)-(.*)/) {
			$project  = $1;
			$sortedBy = $2;
			next;
		}
		if (/entries/) {
			$entries = param( $_ );
			next;
		}
		$project = param( $_ );
	}

	my ($timestamp, $volumes_volumeid, $volumes_volumename, $volumes_server, 
		$volumes_partition, $volumes_type, $volumes_size, $volumes_files, 
		$volumes_status, $volumes_quota, $volumes_availability, 
		$volumes_currentaccesses, $volumes_currentreads, 
		$volumes_currentwrites, $volumes_totalaccesses, $volumes_totalreads, 
		$volumes_totalwrites, $volumes_totalaccesses, $volumes_project);
	
	$sortedByText = $sortedByTexts{$sortedBy};
	&startTableTable( "$project Volumes (sorted by $sortedByText)", $Tonly );	
	&headline4Table_VolumesHottest( $srv, $howManyRows );
	$data_counter = 0;
    my @detail = ConsoleDB::volumes_projdetail($project, $sortedBy, $entries);
    foreach (@detail) {
		# not completely accurate
		$lastDBUpdate = $_->[0];

		$timestamp = &convertTimestamp($_->[0]);
		&row4Table_VolumesHottest($timestamp, $_->[1], $_->[2], $_->[3], 
								  $_->[4], $_->[5], $_->[6], $_->[7], $_->[8], 
								  $_->[9], $_->[10], $_->[11], $_->[12], 
								  $_->[11] + $_[12], $_->[13], $_->[14], 
                                  $_->[15], $_->[16], $sortedBy);
		$data_counter += 1;
    }
	&endTableTable();
	
	if (!$data_counter) {
		
		print "<p align=center><font face='verdana, arial, helvetica' size=2 color=#FF0000>No data available!</font></p>";
	} 

	return 1;
}

sub end_table{
	print "</table></div>";
	return 1;
}


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query = new CGI;
print $query->header(-refresh=>120);

my $val = heading( "Project Report" );
&main_frame();
&end_table();
&proj_vols();
&footer( 0, $lastDBUpdate, $Tonly);
