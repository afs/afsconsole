#!/usr/bin/perl

use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);

my $image;
foreach ( param() ) {
	$image = param($_);
}
print "Content-type: image/png\n\n";
open(IMAGEFILE, "$image") or die "Could not open file: $!";
while(<IMAGEFILE>) { print; }
close(IMAGEFILE);
