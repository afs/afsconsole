#!/usr/bin/env perl

#------------------------------------------------------------------------------
# partitions_hottest.cgi : get the most active partitions
#
# Initial version : Arne Wiebalck CERN IT-FIO/LA, Aug 2007.
#------------------------------------------------------------------------------

use DBI;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
require 'ConsoleMySQL.pl';
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

use ConsoleDB;

#-------------------------------------------------------------------------------
#
# Globals
#
#-------------------------------------------------------------------------------

my $parameter;
my $table = "M_T_904";
my $server;
my $partition = '';
my $entries;
my $sortedBy;
my ($start_date, $start_time, $end_date, $end_time);
my %sortedByTexts = (
		     "accesses"        => "Total Accesses",
		     "currentaccesses" => "Current Accesses",
		     "fillstatus"      => "Fill Status",
		     "volumes"         => "Volumes",
		     "readaccesstime"  => "Read Access Time",
		     "writeaccesstime" => "Write Access Time",
		     );

#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub convertTimestamp() {

	my $timestamp = shift;

	my ($second, $minute, $hour, $dayOfMonth, 
	    $month, $yearOffset, $dayOfWeek, $dayOfYear, 
	    $daylightSavings) = localtime( $timestamp );

	$year  = $yearOffset + 1900;

	$month = $month + 1;	
	$month = "0$month" if ($month < 10);

	$dayOfMonth = "0$dayOfMonth" if ($dayOfMonth < 10);
	$hour       = "0$hour"       if ($hour < 10);
	$minute     = "0$minute"     if ($minute < 10);
	$second     = "0$second"     if ($second < 10);

	return "$year-$month-$dayOfMonth/$hour:$minute:$second";
}

sub calc_timestamps() {

	use Time::Local;
	
	my ($start_date, $start_time, $end_date, $end_time) = @_;
	
	# start
	my ($year, $month, $day) = split /-/, $start_date;
	my ($hours, $min, $sec)  = split /:/, $start_time;
	my $times_start = timelocal( $sec, $min, $hours, $day, $month - 1, $year );

	# end
	($year, $month, $day) = split /-/, $end_date;
	($hours, $min, $sec)  = split /:/, $end_time;
	my $times_end = timelocal( $sec, $min, $hours, $day, $month - 1, $year );

	return "$times_start/$times_end";
}

sub calc_timestampsToday() {

	use Time::Local;
	
	my ($sec, $min, $hrs, $day, $month,
	    $year, $dow, $doy, $dst) = localtime(time);

	# start	
	my $times_start = timelocal( 0, 0, 0, $day, $month, $year );

	# end
	my $times_end = timelocal( 0, 0, 0, $day + 1, $month, $year );

	return "$times_start/$times_end";
}

sub serverNameOK() {

	my $srv = shift;
	return 0 if ($srv eq '');

	my @servers = split /\s+/, &get_all_servers();
	
	return 1 if (grep(/$srv/, @servers));
	return 0;
}

sub hottest {
	
	my ($timestamp, $partitions_server, $partitions_partition,
		$partitions_fillstatus, $partitions_size, $partitions_used, 
		$partitions_volumes, $partitions_online, $partitions_offline, 
		$partitions_committed, $partitions_readaccesstime, 
		$partitions_writeaccesstime);
	
	if (&serverNameOK($server)) {
		$serverText = $server;
	} else {
		$serverText = "Global";
	}

	my $times;
	if ($table eq "M_T_904") {
		if ($today) {
			$times = &calc_timestampsToday();
		} else {
			$times = &calc_timestamps($start_date, $start_time,
									  $end_date, $end_time);
		}
	}

	# XXX Can't be right: the SQL statement wouldn't be valid, then
    #if ($time =~ /^time$/) {
	#	$select = "$select ORDER BY TIMESTAMP";
	#}

	$sortedByText = $sortedByTexts{$sortedBy};
	my $text = "$serverText Hottest Partitions (in terms of $sortedByText";
	$text = "$text, sorted by time" if ($time =~ /^time$/);
	$text = "$text)";

	my $srvprt;
	if (defined $server and $server ne '') {
		$srvprt = $server;
		if (defined $partition and $partition ne '') {
			$srvprt .= "/$partition";
		}
	}

	&startTableTable( "$text", $Tonly );	
	&headline4Table_PartitionsHottest( $srv, $howManyRows );
	$data_counter = 0;
	my @hottest = ConsoleDB::partitions_hottest($srvprt, $times,
												$sortedBy, $entries);
	my $timestamp;
	foreach (@hottest) {
		$timestamp = &convertTimestamp($_->[0]);
		&row4Table_PartitionsHottest($timestamp,
									 $_->[1],
									 $_->[2],
									 $_->[3],
									 $_->[4],
									 $_->[5],
									 $_->[6],
									 $_->[7],
									 $_->[8],
									 $_->[9],
									 $_->[10],
									 $_->[11],
									 $sortedBy);
		$data_counter += 1;
	}
	&endTableTable();
	
	if (!$data_counter) {
		print "<p align=center><font face='verdana, arial, helvetica' size=2 color=#FF0000>No data available!</font></p>";
	} 

	return 1;
}


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query = new CGI;
print $query->header();

# Parameter evaluation
foreach ( param() ) {

	# text-only
	if ($_ eq 'T') { 
		
		$Tonly = 1; 
		next;
	}

	# number of lines
	if (/^entries$/) {

		$entries = param($_);
  	        unless (($entries =~ /^\d+$/) && ($entries >= 1) && ($entries <= 100000)) {
		    $entries = 10;
		}
		next;
	}

	# sorted by
	if (/^sortedBy$/) {
		
		$sortedBy = param($_);
		my $found = 0;
		foreach my $s (@ApartitionFields) {
		    if (lc $s eq $sortedBy) {$found = 1;}
		}
		if (!$found) {$sortedBy = 'size';}
			#print "$sortedBy\n";
			next;
	}

	# sorted by time?
	if (/^time$/) {
		
		$time = param($_);
		$time = "time";
		next;
	}

	# select the data table
	if (/^now$/) {
		
		my $when = param($_);
		if ($when eq "mostRecent") {
			$table = "M_L_904";
		}
		if ($when eq "today") {
			$today = 1;
		}
		next;
	}

        # date & time
	if (/^start_date$/) {
		
		$start_date = param($_);
                unless ($start_date =~ /^\d\d\d\d-\d\d-\d\d$/) {
		    print "Input error: Invalid start date $start_date\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^end_date$/) {
		
		$end_date = param($_);
                unless ($start_date =~ /^\d\d\d\d-\d\d-\d\d$/) {
		    print "Input error: Invalid end date $start_date\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^start_time$/) {
		
		$start_time = param($_);
                unless ($start_time =~ /^\d\d:\d\d:\d\d$/) {
		    print "Input error: Invalid start time $start_time\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^end_time$/) {
		
		$end_time = param($_);
                unless ($end_time =~ /^\d\d:\d\d:\d\d$/) {
		    print "Input error: Invalid end time $end_time\n";
		    &footer_simple( $Tonly );
		    exit 1;
	        }
		next;
	}

	# server name
	if (/^server$/) {
		
		$server = param($_);
		if ($server =~ /\//) {
		    unless (($server =~ /^afs\d\d\d?$/) || 
			    ($server =~ /^afs\d\d\d?\/\w\w+$/) || 
			    ($server =~ /^afsdb\d$/) || 
			    ($server =~ /^afs\d\d\d?\/\w$/)) {
			print "Input error: server $server not known\n";
			&footer_simple( $Tonly );
			exit 1;
		    }
		    ($server, $partition) = split /\//, $server;
		}
		next;
	}	
}


# determine the number of rows to be displayed on the page
$howManyRows = $display;

# HTML creation
&heading( "Hottest Partitions", $Tonly );
&hottest();


&footer_simple( $Tonly );
