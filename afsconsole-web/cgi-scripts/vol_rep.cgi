#!/usr/bin/perl

################################################################################
#
#  Script      : vol_rep.cgi
#
#                Display the most busy volumes (in terms of accesses) per
#                  - server 
#                  - project
#                  - globally 
#
#  Change log  :
#                27.11.2007 awi Changed to afssp_read account
# #                24.04.2007 awi Outsourced ORACLE & constants & HTML #                               & Utils 
#
#  Original version by Mario Bolivar Caba
################################################################################


use DBI;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use lib "/opt/perlmodules/";
use Vos;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
require 'ConsoleMySQL.pl';
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

use ConsoleDB;

#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub vols{

	$table = $VOLUMES_TABLE_CURR;

	$entries = 25;
	$Tonly   = 0;
	$sortedBy = "totalaccesses";

	$action;
	foreach (param()) {
	    
	    if (param($_) =~ /(.*)-(.*)/) {
		$action  = $1;
		$sortedBy = $2;
		next;
	    }

	    if (/entries/) {
		$entries = param($_);		
		next;
	    }
	    $action = param($_);	       
	}
	
	# sanitize input
	unless (($entries =~ /^\d+$/) && ($entries >= 1) && ($entries <= 100000)) {
	    $entries = 10;
	}
	
	my $found = 0;
	foreach my $s (@AvolumeFileds) {
		    if (lc $s eq $sortedBy) {$found = 1;}
	}
	if (!$found) {$sortedBy = 'currentaccesses';}
	
	unless ($action eq "server" || $action eq "project") {
	    $action = "global";
	}
	
	if ($action eq "server") {

		my @srvs;
		my $listsrv = &get_all_servers();
		my $true = 1;
		while ($true){
			if ($listsrv=~/(\w+)(\s)(.*)/) {

				$listsrv = $3;
				$server=$1;
				push @srvs, $server;

			} else {
				$true=0;
			}
		}
		
		foreach (@srvs) {
			
			my $srv = $_;
			
			my ($timestamp, $volumes_volumeid, $volumes_volumename, 
				$volumes_server, $volumes_partition, $volumes_type, 
				$volumes_size, $volumes_files, $volumes_status, $volumes_quota,
				$volumes_availability, $volumes_currentaccesses, 
				$volumes_currentreads, $volumes_currentwrites, 
				$volumes_totalaccesses, $volumes_totalreads, 
				$volumes_totalwrites, $volumes_totalaccesses, $volumes_project);
			
			$sortedByText = $sortedByTexts{$sortedBy};
			&startTableTable("$srv Volumes (sorted by $sortedByText)", $Tonly);	
			&headline4Table_VolumesHottest( $srv, $howManyRows );

			$data_counter = 0;
            @detail = ConsoleDB::volumes_servdetail($srv, $sortedBy, $entries);
            foreach (@detail) {
				# not completely accurate
				$lastDBUpdate = $_->[0];

				$timestamp = &convertTimestamp($_->[0]);
				&row4Table_VolumesHottest($timestamp, $_->[1], $_->[2], $_->[3],
										  $_->[4], $_->[5], $_->[6], $_->[7], 
										  $_->[8], $_->[9], $_->[10], $_->[11],
										  $_->[12], $_->[11] + $_->[12], 
										  $_->[13], $_->[14], $_->[15], 
										  $_->[16], $sortedBy);
				$data_counter += 1;
            }
			&endTableTable();
		}

	} elsif ($action eq "project") {

		$path = $ConsoleConstants::PROJECTS_FILE;
		open FILE, $path or die "Could not open file $path: $!";
		
		my @projects;
		foreach(<FILE>){
			@projects = split " ", $_;
		}
		close FILE;

		foreach(@projects){
		
			my $proj = $_;
			
			my ($timestamp, $volumes_volumeid, $volumes_volumename, 
				$volumes_server, $volumes_partition, $volumes_type, 
				$volumes_size, $volumes_files, $volumes_status, $volumes_quota,
				$volumes_availability, $volumes_currentaccesses, 
				$volumes_currentreads, $volumes_currentwrites, 
				$volumes_totalaccesses, $volumes_totalreads, 
				$volumes_totalwrites, $volumes_totalaccesses, $volumes_project);
			
			$sortedByText = $sortedByTexts{$sortedBy};
			&startTableTable("$proj Volumes (sorted by $sortedByText)", $Tonly);	
			&headline4Table_VolumesHottest( $srv, $howManyRows );

			$data_counter = 0;
            @detail = ConsoleDB::volumes_projdetail($proj, $sortedBy, $entries);
            foreach (@detail) {
				# not completely accurate
				$lastDBUpdate = $_->[0];

				$timestamp = &convertTimestamp($_->[0]);
				&row4Table_VolumesHottest($timestamp, $_->[1], $_->[2], $_->[3],
										  $_->[4], $_->[5], $_->[6], $_->[7], 
										  $_->[8], $_->[9], $_->[10], $_->[11],
										  $_->[12], $_->[11] + $_->[12], 
										  $_->[13], $_->[14], $_->[15], 
										  $_->[16], $sortedBy);
				$data_counter += 1;
            }
			&endTableTable();
		}

	} elsif ($action eq "global") {

		my ($timestamp, $volumes_volumeid, $volumes_volumename, 
			$volumes_server, $volumes_partition, $volumes_type, $volumes_size, 
			$volumes_files, $volumes_status, $volumes_quota, 
			$volumes_availability, $volumes_currentaccesses, 
			$volumes_currentreads, $volumes_currentwrites, 
			$volumes_totalaccesses, $volumes_totalreads, $volumes_totalwrites, 
			$volumes_totalaccesses, $volumes_project);
		
		$sortedByText = $sortedByTexts{$sortedBy};
		&startTableTable( "Global Volumes (sorted by $sortedByText)", $Tonly );	
		&headline4Table_VolumesHottest( $srv, $howManyRows );

		$data_counter = 0;
        @detail = ConsoleDB::volumes_detail($sortedBy, $entries);
		foreach (@detail) {
			# not completely accurate
			$lastDBUpdate = $_->[0];

			$timestamp = &convertTimestamp($_->[0]);
			&row4Table_VolumesHottest($timestamp, $_->[1], $_->[2], $_->[3],
									  $_->[4], $_->[5], $_->[6], $_->[7], 
									  $_->[8], $_->[9], $_->[10], $_->[11],
									  $_->[12], $_->[11] + $_->[12], 
									  $_->[13], $_->[14], $_->[15], 
									  $_->[16], $sortedBy);
			$data_counter += 1;
		}
		&endTableTable();		
	}	
	return 1;
}


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query = new CGI;
print $query->header();

&heading( "Volumes Report", $Tonly );
&vols();
&footer( $moved_counter, $lastDBUpdate, $Tonly );
