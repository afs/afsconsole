#!/usr/bin/env perl

#------------------------------------------------------------------------------
# alarm_hotvolumes.cgi : for each slow server, get most active volume
#
#------------------------------------------------------------------------------

use DBI;
use JSON;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
use ConsoleHTML;
use ConsoleUtils qw(today);
use ConsoleDB;
use strict;

#-------------------------------------------------------------------------------
#
# Globals
#
#-------------------------------------------------------------------------------

my $parameter;
my $table = "M_T_905";
my $Tonly = 0;
my $timedelta = 3600; # look for busiest volume over last hour (data updates only every 20min, and some slack..)
my $server;
my $sortedBy = "currentserverimpact"; # magic formula that estimates how ops add to slowness, see ConsoleDB.pm

my $data_path  = '/tmp/afs-console'; # also used by other scripts
my $cache_file = $data_path . '/alarms_hotvolumes.json';

my $date = &today();
my $slowfilename = join '-', $ConsoleConstants::VOLSLOGS_DIR . '/volumes_log', $date;

#
#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub calc_timestamps() {

    use Time::Local;
    my $nowsec = time();
    my @now = localtime($nowsec);
    my $times_end = timelocal(@now);
    my @before= localtime($nowsec - $timedelta);
    my $times_start = timelocal(@before);
    return "$times_start/$times_end";
}

sub get_cached_data_maybe {
    my @data_s = stat($slowfilename) or die "cannot stat '$slowfilename: $!";;
    my @cache_s = stat($cache_file);
    if (@cache_s and $cache_s[9] > @data_s[9]) { # compare mtime
	open (my $jh, '<', $cache_file) or die "cannot open '$cache_file': $!";
	my $outref = decode_json(<$jh>);
	close($jh);
	return $outref;
    }
}

sub write_cache {
    # write cache - errors as of here are not very important.
    my $outref = shift;
    mkdir($data_path, 0700); # ignore EEXITS
    open (my $jh, '>', $cache_file) or die "WARN: cannot write cache '$cache_file': $!";
    print $jh encode_json($outref);
    close($jh) or warn "cannot close cache '$cache_file': $!";
    warn "updated cache in '$cache_file'\n";
}

sub get_slowservers {
    open (my $fh , '<', $slowfilename) or die "Could not open '$slowfilename': $!";
    my @Asrvs = ();
    while (<$fh>) {
	next if (/No problems/);
	# Servers - 2021/12/02 @ 08:25:02 - afs939 has a local response time for 64Kb of 194.22 ms (avg last hour)
	if (m;^Servers\s+-\s+\d{4}/\d{2}/\d{2}\s*@\s*\d{2}:\d{2}:\d{2}\s+- (\w+) has a local response time for \w+ of (\d+\.\d+) ms;) {
	    push (@Asrvs, [$1, $2]);
	}
    }
    close($fh);
    return @Asrvs;
}

sub classify_slowness {
    my $latency_ms = shift;
    if($latency_ms > 1000) {
	return 'horrible';
    } elsif($latency_ms > 300) {
	return 'bad';
    } elsif($latency_ms > 50) {
	return 'slow';
    } else {
	die "unexpected 'slow' server with less than 50ms latency?";
    }
}

sub get_hottest_on_server {

    my $server = shift;

    die "no known timestamp method for table $table" unless $table eq "M_T_905";
    my $times = calc_timestamps();

    my @hottest = ConsoleDB::volumes_hottest($server, $times, $sortedBy, 1);
    my $voldata = { 'server' => $server };
    if ( @hottest ) {
        $voldata = { 'server' => $server,
			'volume' => $hottest[0][2],
			'current_accesses' =>  $hottest[0][10] + 0.0, # want numbers, for JSON
			'current_read' => $hottest[0][11] + 0.0,
			'current_write' => $hottest[0][12] + 0.0 };
    } else {
        warn "did not get a single hottest volume for server=$server";
    }
    return $voldata;
}

sub guess_account {
    my $volname = shift;
    my $account;
    if ($volname =~ /^(user|work)\.([^.]+)$/) {
	$account = $2;
    }
    return $account;
}

sub get_fresh_data {
    my @out;
    for my $entry (get_slowservers()) {
	my ($server, $latency) = @$entry;
	my $voldata = get_hottest_on_server($server);
	$$voldata{'account'} = guess_account($$voldata{'volume'});
	$$voldata{'badness'} = classify_slowness($latency);
	$$voldata{'latency_ms'} = $latency + 0.0; # want numbers, for JSON
	$$voldata{'impact'} = $$voldata{'current_accesses'}/10 + $$voldata{'current_read'} + 10 * $$voldata{'current_write'};
	push(@out, $voldata);
    }
    return \@out;
}

#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $outref;
my $cacheref = get_cached_data_maybe();
if ( $cacheref ) {
    warn "read from cache\n";
    $outref = $cacheref;
} else {
    warn "not cached\n";
    $outref = get_fresh_data();
}

my $query = new CGI;
my $do_json = $query->Accept("application/json");

# Chrome says it accepts *anything* with 0.8 ..
if ($do_json >= .9) {
    print $query->header(-type=>"application/json",
                         -content_disposition=>"attachment; filename=alarms_hotvolumes.json");
    print encode_json($outref);
} else {
    print $query->header();
    &heading( "Hottest Volume for each overloaded Server");
    &startTableTable( "Hottest Volume for each overloaded Server", $Tonly );
    # ugly stub. Else would need a new modules/ConsoleHTML.pm function to format this (pre-CSS)
    print "<table border='2'>\n<tr>\n<th>Server</th><th>Latency</th><th>Latency badness</th><th>Volume Name</th><th>Account</th><th>Current Accesses [1/s]</th><th>Current Reads [1/s]</th><th>Current Writes [1/s]</th><th>Impact Score</th></tr>\n";
    for my $hot (@$outref) {
	print "<tr>\n";
	for my $k ('server', 'latency_ms', 'badness', 'volume', 'account', 'current_accesses', 'current_read', 'current_write', 'impact') {
	    print "<td>".$$hot{$k}."</td>";
	}
	print "</tr>\n";
    }
    print "</table>\n";
    &endTableTable();
    &footer_simple();
}

if ( ! $cacheref ) {
    write_cache($outref);
}
1;
