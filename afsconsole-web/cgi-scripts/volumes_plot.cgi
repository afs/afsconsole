#!/usr/bin/env perl

#------------------------------------------------------------------------------
# volumes_plot.cgi : plot volume information
#
# Initial version : Arne Wiebalck CERN IT-FIO/LA, Aug 2007.
#------------------------------------------------------------------------------

use DBI;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);

use File::Temp qw/ :mktemp  /;
use File::stat;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
require 'ConsoleMySQL.pl';
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

use ConsoleDB;

#-------------------------------------------------------------------------------
#
# Globals
#
#-------------------------------------------------------------------------------

my $data_path  = '/tmp/afs-console';

my $parameter;
my $server;
my $entries;
my ($start_date, $start_time, $end_date, $end_time);
my %HsortedByTexts = (
		     "volumes_size"            => "Volume Size",
		     "volumes_files"           => "Number of Files",
		     "volumes_quota"           => "Quota",
		     "volumes_currentaccesses" => "Current Accesses",
		     "volumes_currentreads"    => "Current Reads",
		     "volumes_currentwrites"   => "Current Writes",
		     "volumes_totalaccesses"   => "Total Accesses",
		     "volumes_totalreads"      => "Total Read Accesses",
		     "volumes_totalwrites"     => "Total Write Accesses",
		     );
my %Hfiles = ();

my %Hsize  = ();
my %Hquota = ();

my %Hcurrent_accesses = ();
my %Hcurrent_reads    = ();
my %Hcurrent_writess  = ();

my %Htotal_accesses = ();
my %Htotal_reads    = ();
my %Htotal_writes  = ();


#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub getSortedKeys() {

	$_Hhash = shift;
	
	my @Aunsorted = ();

	# get the keys
	while (($key, $value) = each %$_Hhash) {
	
		push @Aunsorted, $key;
	}
	
	# sort'em
	my @Asorted = sort( @Aunsorted );

	return @Asorted;
}


sub shortTime() {

	my $ts = shift;

	($second, $minute, $hour, $dayOfMonth, $month, 
	 $yearOffset, $dayOfWeek, $dayOfYear, $daylightSavings) = localtime( $ts );

	$month = $month + 1;

	$dayOfMonth = "0$dayOfMonth" if ($dayOfMonth < 10);
	$month      = "0$month" if ($month < 10);
	$hour       = "0$hour" if ($hour < 10);
	$minute     = "0$minute" if ($minute < 10);

	return "$dayOfMonth-$month $hour:$minute";
}


sub findNiceInterval() {

	my $intvl = shift;

	my $tmp = $intvl % 3600;
	if ($tmp<1800) {
		$intvl = $intvl - $tmp;
	} else {
		$intvl = $intvl + (3600 - $tmp);
	}

	return $intvl;
}


sub createXtics() {

	my ($start, $end) = @_;
	my $intvl = ($end - $start) / 10;
	
	$intvl = &findNiceInterval( $intvl );
	$start = &findNiceInterval( $start );

	my $xtics = "(";
	
	$i = $start;
	while ($i < $end) {
		
		$xtics = "$xtics \"" . &shortTime( $i ) . "\" $i,"; 
		$i = $i + $intvl;
	} 
	
	#$xtics = "$xtics \"" . &shortTime( $end ) . "\" $end,"; 

	# remove the last ','
	chop $xtics;

	return "$xtics )";
}

sub generate_files_plot() {

	my ($volumeName, $start, $end) = @_;

	# fill the data file
	my ($fhd, $datafile) = mkstemps( "$data_path/tmpfileXXXXXX", ".dat");

	my @Asorted = &getSortedKeys( \%Hfiles );

	foreach $t (@Asorted) {

		my $value = $Hfiles{$t};
		print $fhd "$t\t$value\n"; 
	}
        # end fill data file

	# generate the plot
	my ($fhp, $plotfile) = mkstemps( "$data_path/tmpfileXXXXXX", ".png");

	# FIXME
	my $gnuplot = "/usr/bin/gnuplot";

	my $xtics = &createXtics( $start, $end );

	open GNUPLOT, "|$gnuplot" or die "Unable to start gnuplot: $!";
	print GNUPLOT <<gnuplot_Commands_Done;
	
	    set term png font ",10" size 640,380
	    set output "$plotfile"
	    set title "Number of files of volume $volumeName" 
	    set xlabel "Timestamp" offset 0,-1 
	    set ylabel "Number of Files" 
	    set border
	    set key outside samplen 1.75 height 0.1
	    set xtics $xtics
	    set xtics rotate by 90 right
	    set xrange[$start:$end]
	plot "$datafile" title " files" with linesp

gnuplot_Commands_Done

close (GNUPLOT);

	return $plotfile;
}

sub generate_sizequota_plot() {

	my ($volumeName, $start, $end, $size, $quota) = @_;

	# the data file
	my ($fhd, $datafile) = mkstemps( "$data_path/tmpfileXXXXXX", ".dat");

	# generate the plot
	my ($fhp, $plotfile) = mkstemps( "$data_path/tmpfileXXXXXX", ".png");	

	# FIXME
	my $gnuplot = "/usr/bin/gnuplot";
		
	my $xtics = &createXtics( $start, $end );

	# plot size and quota
	if (($size  =~ /on/) && ($quota =~ /on/)) {
		
		my @AsortedSize  = &getSortedKeys( \%Hsize ); 
		my @AsortedQuota = &getSortedKeys( \%Hquota ); 
		
		foreach $t (@AsortedSize) {
			
			my $valueSize  = $Hsize{$t};
			my $valueQuota = $Hquota{$t};
			print $fhd "$t\t$valueSize\t$valueQuota\n"; 
		}
				
		open GNUPLOT, "|$gnuplot" or die "Unable to start gnuplot: $!";
		print GNUPLOT <<gnuplot_Commands_Done;
	
		    set term png font ",10" size 640,380
		    set output "$plotfile"
		    set title "Size/Quota of volume $volumeName" 
		    set xlabel "Timestamp" offset 0,-1
		    set ylabel "Quota & Size [kByte]" 
		    set border
		    set key outside samplen 1.75 height 0.1
		    set xtics $xtics
		    set xtics rotate by 90 right
		    set xrange[$start:$end]
		    plot "$datafile" using 1:2 title " size" with linesp, "$datafile" using 1:3 title " quota" with linesp

gnuplot_Commands_Done

close (GNUPLOT);
		

	# plot size only
	} elsif (($size  =~ /on/)) {
		
		my @AsortedSize  = &getSortedKeys( \%Hsize ); 
		
		foreach $t (@AsortedSize) {
			
			my $valueSize  = $Hsize{$t};
			print $fhd "$t\t$valueSize\n"; 
		}

		open GNUPLOT, "|$gnuplot" or die "Unable to start gnuplot: $!";
		print GNUPLOT <<gnuplot_Commands_Done;
	
		    set term png font ",10" size 640,380
		    set output "$plotfile"
		    set title "Size of volume $volumeName" 
		    set xlabel "Timestamp" offset 0,-1
		    set ylabel "Size [kByte]" 
		    set border
		    set key outside samplen 1.75 height 0.1
		    set xtics $xtics
		    set xtics rotate by 90 right
		    set xrange[$start:$end]
		    plot "$datafile" using 1:2 title " size" with linesp

gnuplot_Commands_Done

close (GNUPLOT);

	# plot quota only	
	} else {

		my @AsortedQuota = &getSortedKeys( \%Hquota ); 
				
		foreach $t (@AsortedQuota) {
			
			my $valueQuota  = $Hquota{$t};
			print $fhd "$t\t$valueQuota\n"; 
		}

		open GNUPLOT, "|$gnuplot" or die "Unable to start gnuplot: $!";
		print GNUPLOT <<gnuplot_Commands_Done;
	
		    set term png font ",10" size 640,380
		    set output "$plotfile"
		    set title "Quota of volume $volumeName" 
		    set xlabel "Timestamp" offset 0,-1
		    set ylabel "Quota [kByte]" 
		    set border
		    set key outside samplen 1.75 height 0.1
		    set xtics $xtics
		    set xtics rotate by 90 right
		    set xrange[$start:$end]
		    plot "$datafile" using 1:2 title "quota" with linesp

gnuplot_Commands_Done

close (GNUPLOT);
	}

	return $plotfile;
}

sub generate_totalall_plot() {

	my ($volumeName, $start, $end) = @_;

	# fill the data file
	my ($fhd, $datafile) = mkstemps( "$data_path/tmpfileXXXXXX", ".dat");

	my @Asorted_totalaccesses = &getSortedKeys( \%Htotal_accesses );
	my @Asorted_totalreads    = &getSortedKeys( \%Htotal_reads );
	my @Asorted_totalwrites   = &getSortedKeys( \%Htotal_writes );

	foreach $t (@Asorted_totalaccesses) {

		my $value_totalAccesses = $Htotal_accesses{$t};
		my $value_totalReads    = $Htotal_reads{$t};
		my $value_totalWrites   = $Htotal_writes{$t};
		print $fhd "$t\t$value_totalAccesses\t$value_totalReads\t$value_totalWrites\n"; 
	}
        # end fill data file

	# generate the plot
	my ($fhp, $plotfile) = mkstemps( "$data_path/tmpfileXXXXXX", ".png");	

	# FIXME
	my $gnuplot = "/usr/bin/gnuplot";
	
	my $xtics = &createXtics( $start, $end );

	open GNUPLOT, "|$gnuplot" or die "Unable to start gnuplot: $!";
	print GNUPLOT <<gnuplot_Commands_Done;
	
	    set term png font ",10" size 640,380
	    set output "$plotfile"
	    set title "Total Accesses/Reads/Writes for volume $volumeName" 
	    set xlabel "Timestamp" offset 0,-1 
	    set ylabel "Total Acc./Reads/Writes" 
	    set key outside samplen 1.75 height 0.1
	    set xtics $xtics
	    set xtics rotate by 90 right
	    set xrange[$start:$end]
	plot "$datafile" using 1:2 title " Accesses" with linesp, "$datafile" using 1:3 title " Reads" with linesp, "$datafile" using 1:4 title " Writes" with linesp

gnuplot_Commands_Done

close (GNUPLOT);

	return $plotfile;
}

sub generate_currentall_plot() {

	my ($volumeName, $start, $end) = @_;

	# fill the data file
	my ($fhd, $datafile) = mkstemps( "$data_path/tmpfileXXXXXX", ".dat");

	my @Asorted_currentaccesses = &getSortedKeys( \%Hcurrent_accesses );
	my @Asorted_currentreads    = &getSortedKeys( \%Hcurrent_reads );
	my @Asorted_currentwrites   = &getSortedKeys( \%Hcurrent_writes );

	foreach $t (@Asorted_currentaccesses) {

		my $value_currentAccesses = $Hcurrent_accesses{$t};
		my $value_currentReads    = $Hcurrent_reads{$t};
		my $value_currentWrites   = $Hcurrent_writes{$t};
		print $fhd "$t\t$value_currentAccesses\t$value_currentReads\t$value_currentWrites\n"; 
	}
        # end fill data file

	# generate the plot
	my ($fhp, $plotfile) = mkstemps( "$data_path/tmpfileXXXXXX", ".png");	

	# FIXME
	my $gnuplot = "/usr/bin/gnuplot";
	
	my $xtics = &createXtics( $start, $end );

	open GNUPLOT, "|$gnuplot" or die "Unable to start gnuplot: $!";
	print GNUPLOT <<gnuplot_Commands_Done;
	
	    set term png font ",10" size 640,380
	    set output "$plotfile"
	    set title "Current Accesses/Reads/Writes for volume $volumeName" 
	    set xlabel "Timestamp" offset 0,-1 
	    set ylabel "Cur. Acc./Reads/Writes [1/s]" 
	    set key outside samplen 1.75 height 0.1
	    set xtics $xtics
	    set xtics rotate by 90 right
	    set xrange[$start:$end]
	plot "$datafile" using 1:2 title " Accesses" with linesp, "$datafile" using 1:3 title " Reads" with linesp, "$datafile" using 1:4 title " Writes" with linesp

gnuplot_Commands_Done

close (GNUPLOT);

	return $plotfile;
}

sub convertTimestamp() {

	my $timestamp = shift;

	my ($second, $minute, $hour, $dayOfMonth, 
	    $month, $yearOffset, $dayOfWeek, $dayOfYear, 
	    $daylightSavings) = localtime( $timestamp );

	$year  = $yearOffset + 1900;

	$month = $month + 1;	
	$month = "0$month" if ($month < 10);

	$dayOfMonth = "0$dayOfMonth" if ($dayOfMonth < 10);
	$hour       = "0$hour"       if ($hour < 10);
	$minute     = "0$minute"     if ($minute < 10);
	$second     = "0$second"     if ($second < 10);

	return "$year-$month-$dayOfMonth/$hour:$minute:$second";
}

# FIXME Centralise
sub calc_timestamps() {

	use Time::Local;
	
	my ($start_date, $start_time, $end_date, $end_time) = @_;
	
	# start
	my ($year, $month, $day) = split /-/, $start_date;
	my ($hours, $min, $sec)  = split /:/, $start_time;
	my $times_start = timelocal( $sec, $min, $hours, $day, $month - 1, $year );

	# end
	($year, $month, $day) = split /-/, $end_date;
	($hours, $min, $sec)  = split /:/, $end_time;
	my $times_end = timelocal( $sec, $min, $hours, $day, $month - 1, $year );

        # adjust the time window to 5 hours at least
	if ($times_end - $times_start < 5 * 3600) {
		
		$times_start = $times_end - 5 * 3600;
	}

	return "$times_start/$times_end";
}

# FIXME Centralise
sub calc_timestampsToday() {

	use Time::Local;
	
	my ($sec, $min, $hrs, $day, $month,
	    $year, $dow, $doy, $dst) = localtime(time);

	# start	
	my $times_start = timelocal( 0, 0, 0, $day, $month, $year );

	# end
	my $times_end = $times_start + 86400;

	return "$times_start/$times_end";
}

sub todayTilltomorrow() {
	
	my ($sec, $min, $hrs, $day, $month,
	    $year, $dow, $doy, $dst) = localtime(time);

	$day = "0$day" if ($day<10);
	$month = $month + 1;
	$month = "0$month" if ($month<10);

	$start_date = 1900 + $year . "-" .  $month . "-" . "$day";
	$start_time = "00:00:00";

	($sec, $min, $hrs, $day, $month,
	 $year, $dow, $doy, $dst) = localtime(time + 86400);
	
	$day = "0$day" if ($day<10);
	$month = $month + 1;
	$month = "0$month" if ($month<10);
	
	
	$end_date = 1900 + $year . "-" .  $month . "-" . "$day";
	$end_time = "00:00:00";
}

sub plot {
	
	my ($timestamp, $volumes_volumeid, $volumes_volumename, $volumes_server, 
		$volumes_partition, $volumes_type, $volumes_size, $volumes_files, 
		$volumes_status, $volumes_quota, $volumes_availability, 
		$volumes_currentaccesses, $volumes_currentreads, $volumes_currentwrites,
		$volumes_totalaccesses, $volumes_totalreads, $volumes_totalwrites, 
		$volumes_totalaccesses, $volumes_project); 
	unless ($volumeName) {
		print "no volume name given\n";
		return 1;
	}

	my $times;
	if ($today) {
		$times = &calc_timestampsToday();
	} else {
		$times = &calc_timestamps($start_date, $start_time,
								  $end_date, $end_time );
	}
    ($start, $end) = split(/\//, $times);

	$startPrep = time();
	# FIXME Well...
	$endPrep = time();

	$startExec = time();
	my @data = ConsoleDB::volumes_plot($volumeName, $times);
	$endExec = time();

	# FIXME
	&startTableTable2( "Volume's Data Plot for $volumeName", $Tonly );	

	$data_counter = 0;

	# get the data
	$startFetch = time();
	# FIXME Well...
	$endFetch = time();
	
	$startFill = time();
	foreach my $row (@data) {
		$timestamp = $$row[1];
		$Hfiles{$timestamp} = $$row[8];
		$Hsize{$timestamp}  = $$row[7];
		$Hquota{$timestamp} = $$row[10];  

		if ($$row[12]>0) {
			$Hcurrent_accesses{$timestamp} = $$row[12];
		} else {
			$Hcurrent_accesses{$timestamp} = 0;
		}
		if ($$row[13]>0) {
			$Hcurrent_reads{$timestamp} = $$row[13];
		} else {
			$Hcurrent_reads{$timestamp} = 0;
		}
		if ($$row[14]>0) {
			$Hcurrent_writes{$timestamp} = $$row[14];
		} else {
			$Hcurrent_writes{$timestamp} = 0;
		}
		if ($$row[15]>0) {
			$Htotal_accesses{$timestamp} = $$row[15];
		} else {
			$Htotal_accesses{$timestamp} = 0;
		}
		if ($$row[16]>0) {
			$Htotal_reads{$timestamp} = $$row[16];
		} else {
			$Htotal_reads{$timestamp} = 0;
		}
		if ($$row[17]>0) {
			$Htotal_writes{$timestamp} = $$row[17];
		} else {
			$Htotal_writes{$timestamp} = 0;
		}
		
		$data_counter += 1;	
	}
	$endFill = time();

	$endScript = time();

	if (!$data_counter) {
		
		print "<p align=center><font face='verdana, arial, helvetica' size=2 color=#FF0000>No data available!</font></p>";
		print "<br>";
		&endTableTable();
		return 1;
	}

	$startPlot = time();

	# generate the plot(s), there are 4 groups:
	# (1) Files
	my $files_plot = &generate_files_plot( $volumeName, $start, $end ) if ($files =~ /on/);

	# (2) Quota, Size [kBytes]
	my $sizequota_plot = &generate_sizequota_plot( $volumeName, $start, $end, $size, $quota ) if (($size =~ /on/) || ($quota =~ /on/));

        # (3) Current Accesses, Reads, Writes [1/s]
	my $currentall_plot = &generate_currentall_plot( $volumeName, $start, $end, $size, $quota ) if ($current_all =~ /on/);

        # (3) Total Accesses, Reads, Writes 
	my $totalall_plot = &generate_totalall_plot( $volumeName, $start, $end, $size, $quota ) if ($total_all =~ /on/);

	# embed the graph(s)
	print "<div align='center'>";

	my $plotctr = 0;
	if ($current_all =~ /on/) { 

		&make_imbed_grafx(\$graphs_currentall_plot, $currentall_plot);
		print "<img border='1' style=\"border:1px solid #165480\" src='$graphs_currentall_plot'>";
		$plotctr += 1;
	} 

	if ($total_all =~ /on/) { 
		&make_imbed_grafx(\$graphs_totalall_plot, $totalall_plot);

		print "<img border='1' style=\"border:1px solid #165480\" src='$graphs_totalall_plot'>";
		$plotctr += 1;
	} 

	if ($files =~ /on/) {
		&make_imbed_grafx(\$graphs_files_plot, $files_plot);
		print "<img border='1' style=\"border:1px solid #165480\" src='$graphs_files_plot'>" ;
		$plotctr += 1;
	}

	&make_imbed_grafx(\$graphs_sizequota_plot, $sizequota_plot);
	print "<img border='1' style=\"border:1px solid #165480\" src='$graphs_sizequota_plot'>" 
	    if (($size =~ /on/) || ($quota =~ /on/));

	print "</div>";

	print "<br>";
	&endTableTable();

	$endPlot = time();
	
	my $delta = $endScript - $startScript;
	my $deltaPlot = $endPlot - $startPlot;
	my $deltaExec = $endExec - $startExec;
	my $deltaFetch = $endFetch - $startFetch;
	my $deltaPrep = $endPrep - $startPrep;
	my $deltaFill = $endFill - $startFill;

	my $action = 'volumes_plot.cgi';

	print "<form method='GET' action=$action name='info' target='_parent'>";

	# set the day's span
	unless ($start_date) {
		&todayTilltomorrow();
	}

	print "<div align='center'>
                 <font face='verdana, arial, helvetica' size=2 color=#FF0000>Found $data_counter items in $delta secs (prep: $deltaPrep, exec: $deltaExec, fetch: $deltaFetch, fill: $deltaFill), plotted in $deltaPlot secs</font>
                 &nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp; From :  <input type='text' name='start_date' value=$start_date maxlength=10 size=10>
                                    <input type='text' name='start_time' value=$start_time maxlength=8 size=8>
                              To :  <input type='text' name='end_date' value=$end_date maxlength=10 size=10>
                                  <input type='text' name='end_time' value=$end_time maxlength=8 size=8>

                                                <form method='GET' action=$action name='files' target='_parent'>
                                                <input type='hidden' name='files' checked value=$files>

                                                <form method='GET' action=$action name='quota' target='_parent'>
                                                <input type='hidden' name='quota' checked value=$quota>

                                                <form method='GET' action=$action name='size' target='_parent'>
                                                <input type='hidden' name='size' checked value=$size>

                                                <form method='GET' action=$action name='current_accesses' target='_parent'>
                                                <input type='hidden' name='current_accesses' disabled>

                                                <form method='GET' action=$action name='current_reads' target='_parent'>
                                                <input type='hidden' name='current_reads' disabled>

                                                <form method='GET' action=$action name='current_writes' target='_parent'>
                                                <input type='hidden' name='current_writes' disabled>

                                                <form method='GET' action=$action name='current_all' target='_parent'>
                                                <input type='hidden' name='current_all' checked value=$current_all >

                                                <form method='GET' action=$action name='total_accesses' target='_parent'>
                                                <input type='hidden' name='total_accesses' disabled>

                                                <form method='GET' action=$action name='total_reads' target='_parent'>
                                                <input type='hidden' name='total_reads' disabled>

                                                <form method='GET' action=$action name='total_writes' target='_parent'>
                                                <input type='hidden' name='total_writes' disabled>

                                                <form method='GET' action=$action name='total_all' target='_parent'>
                                                <input type='hidden' name='total_all' checked value=$total_all>

                                                <input type='hidden' name='volumeName' value=$volumeName maxlength=23 size=23>"; 

	
	print "<input type='submit' value='Submit'>
	       </div>";
	return 1;
}


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query = new CGI;
print $query->header();

$startScript = time();
$endScript   = 0;

# Parameter evaluation
foreach (param()) {

	# text-only
	if ($_ eq 'T') { 
		
		$Tonly = 1; 
		next;
	}

	# select the data table
	if (/^now$/) {
		
		my $when = param($_);
		if ($when eq "today") {
			$today = 1;
		}
		next;
	}

	# date & time
	if (/^start_date$/) {
		
		$start_date = param($_);
                unless ($start_date =~ /^\d\d\d\d-\d\d-\d\d$/) {
		    print "Input error: Invalid start date $start_date\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^end_date$/) {
		
		$end_date = param($_);
                unless ($start_date =~ /^\d\d\d\d-\d\d-\d\d$/) {
		    print "Input error: Invalid end date $start_date\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^start_time$/) {
		
		$start_time = param($_);
                unless ($start_time =~ /^\d\d:\d\d:\d\d$/) {
		    print "Input error: Invalid start time $start_time\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^end_time$/) {
		
		$end_time = param($_);
                unless ($end_time =~ /^\d\d:\d\d:\d\d$/) {
		    print "Input error: Invalid end time $end_time\n";
		    &footer_simple( $Tonly );
		    exit 1;
	        }
		next;
	}

	# volume name
	if (/^volumeName$/) {
		
		$volumeName = param($_);
                unless ($volumeName =~ /^[a-zA-Z\.\_\-\d]+$/) {
                   print "Input error: Invalid volume name $volumeName\n";
		    &footer_simple( $Tonly );
		    exit 1;
                }
		next;
	}	

	# what to plot
	if (/^size$/) {
		
		$size = param($_);
                if ($size ne "on") {$size = "on";}
		next;
	}	
	if (/^files$/) {
		
		$files = param($_);
                if ($files ne "on") {$files = "on";}
		next;
	}	
	if (/^quota$/) {
		
		$quota = param($_);
                if ($quota ne "on") {$quota = "on";}
		next;
	}	
	if (/^current_accesses$/) {
		
		$current_accesses = param($_);
                if ($current_accesses ne "on") {$current_accesses = "on";}
		next;
	}	
	if (/^current_reads$/) {
		
		$current_reads = param($_);
                if ($current_reads ne "on") {$current_reads = "on";}
		next;
	}	
	if (/^current_writes$/) {
		
		$current_writes = param($_);
                if ($current_writes ne "on") {$current_writes = "on";}
		next;
	}	
	if (/^current_all$/) {
		
		$current_all = param($_);
                if ($current_all ne "on") {$current_all = "on";}
		next;
	}	
	if (/^total_accesses$/) {
		
		$total_accesses = param($_);
                if ($total_accesses ne "on") {$total_accesses = "on";}
		next;
	}	
	if (/^total_reads$/) {
		
		$total_reads = param($_);
                if ($total_reads ne "on") {$total_reads = "on";}
		next;
	}	
	if (/^total_writes$/) {
		
		$total_writes = param($_);
                if ($total_writes ne "on") {$total_writes = "on";}
		next;
	}	
	if (/^total_all$/) {
		
		$total_all = param($_);
                if ($total_all ne "on") {$total_all = "on";}
		next;
	}	
}


# determine the number of rows to be displayed on the page
$howManyRows = $display;

# HTML creation
&heading( "Plot Volume's Data", $Tonly );

# create the dat/png dir
mkdir($data_path);

# cleanup files older than 5 minutes
@files = <$data_path/*>;
$time = time;
foreach $f (@files) {
	if (stat($f)->ctime < $time - 300) {
		unlink($f);
	}
}

&plot();

&footer_simple( $Tonly );

#&footer_simple2("$msg files: @files");
#@files = <$data_path/*>;
#foreach $f (@files) {
#	$tmp = ctime(stat($f)->ctime);
#	&footer_simple2("$tmp");
#}
