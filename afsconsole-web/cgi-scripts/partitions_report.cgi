#!/usr/bin/perl

################################################################################
#
#  Script      : pool_report.cgi
#
#  Original version by Mario Bolivar Caba, CERN IT-FIO/LA, 2006 
################################################################################

use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use lib "/opt/perlmodules/";
use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS
use Vos;

use ConsoleConstants;
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

#-------------------------------------------------------------------------------
#
# Globals
#
#-------------------------------------------------------------------------------

&make_imbed_grafx(\$graphs_icons_kfm_home, "icons/kfm_home");
&make_imbed_grafx(\$graphs_icons_contents, "icons/contents");
#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub heading{
	print "<html>";
	print "<head><title>CERN AFS Console View - Partitions Report</title>";
	print "</head>";
	print "<body bgcolor=#ffffff font=arial, helevetica>";
	print "<HTML><table width=100% cellpadding=1 border=0 cellspacing=0 bgcolor=#000000><tr><td>";
	print "<HTML><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeef><tr><td>";
	print "<A href='index.cgi'>
               <img border='0' src='$graphs_icons_kfm_home'></A>
               &nbsp&nbsp&nbsp <font size=3 face='verdana, arial, helvetica'><A name='init'>CERN AFS Console View</A></font>";
	print "</table></tr></td>";
	print "</table></tr></td>";
	print "<HR></HR>";
	return 1;
}

sub main_frame() {

	print "
<div align=center><table width='100%'>
<tr>
<td>
	<table border='0' width='100%' cellspacing ='1' cellpadding='3' border ='0'>
		<tr>
		<th><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		<tr>
		<td bgcolor='#5FA6D7'>
		<img src='$graphs_icons_xmag'>
		<font size=2 face='verdana, arial, helvetica'>
		<b><font size=2 face='verdana, arial, helvetica' color='#000000'>Partitions Data Report</font></b>
		</font>
		</td>
		</tr>
		</div></table></th>
		</tr>

		<tr>
		<td><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		<tr><td bgcolor='#FFFFCC'>
		<div align=center>
		<font color=#465e90 size=2 face='verdana, arial, helvetica'>
		<img src='$graphs_icons_contents'>
		<A href=partitions_hottest.cgi?entries=10000&sortedBy=fillstatus&now=mostRecent>Get data Report</A><br>";
	print "</font>
		</div>
		</font>
		</td>
		</tr>
		</table></div>
	</td>
	</tr>
	</table></table></div>";


	print "
<div align=center><table width='100%'>
<tr>
<td>
	<table border='0' width='100%' cellspacing ='1' cellpadding='3' border ='0'>
		<tr>
		<th><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		<tr>
		<td bgcolor='#5FA6D7'>
		<img src='$graphs_icons_xmag'>
		<font size=2 face='verdana, arial, helvetica'>
		<b><font size=2 face='verdana, arial, helvetica' color='#000000'>Partitions Report</font></b>
		</font>
		</td>
		</tr>
		</div></table></th>
		</tr>

		<tr>
		<td><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		<tr><td bgcolor='#FFFFCC'>
		<div align=center>
		<font color=#465e90 size=2 face='verdana, arial, helvetica'>";
	&images();
	print "</font>
		</div>
		</font>
		</td>
		</tr>
		</table></div>
	</td>
	</tr>
	</table></table></div>";

	return 1;

}


sub images() {
	
	opendir DIR1, $ConsoleConstants::GRAPHS or die "Cannot opendir $ConsoleConstants::GRAPHS:$!";
	my @Ause = grep(/partitions-\d+-use.png/, readdir(DIR1));
	closedir(DIR1);
	foreach (@Ause) {
		s/\.png//;
		my $fn = $_; $fn =~tr/.-/__/;
		&make_imbed_grafx(qq(graphs_$fn), $_);
		print "<img src='${qq(graphs_$fn)}'>\n";
	}

	opendir DIR2, $ConsoleConstants::GRAPHS or die "Cannot opendir $ConsoleConstants::GRAPHS:$!";
	my @Aaccs = grep(/partitions-\d+-accs.png/, readdir(DIR2));
	closedir(DIR2);
	foreach (@Aaccs) {
		s/\.png//;
		my $fn = $_; $fn =~tr/.-/__/;
		&make_imbed_grafx(qq(graphs_$fn), $_);
		print "<img src='${qq(graphs_$fn)}'>\n";
	}

	return 1;
}


sub foot{

	print "<HR></HR>
               <body bgcolor=#ffffff font=arial, helevetica>
               <div align=right><font face='verdana, arial, helvetica' size=2>AFS Service - CERN IT</font></div>
               </table></tr></td></div>";
	return 1;
}

sub timestamp{
	my($second,$minute,$hour,$day,$month,$year,$date);
	my @timedata = localtime(time);
	$second=$timedata[0];
	$minute=$timedata[1];
	$hour=$timedata[2];
	$day=$timedata[3];
	$month=$timedata[4]+1;
	my($lsec,$lmin,$lhour,$lday,$lmonth);

	$month = "0".$month if(($lmonth=length($month)) eq 1);
	$day = "0".$day if(($lday=length($day)) eq 1);
	$hour = "0".$hour if(($lhour=length($hour)) eq 1);
	$minute = "0".$minute if(($lmin=length($minute)) eq 1);
	$second = "0".$second if(($lsec=length($second)) eq 1);
	$year=($timedata[5] + 1900 );
	$date="$year"."-"."$month"."-"."$day";

	return $date;
}


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query=new CGI;
print $query->header();
&heading("Partition Report");
&main_frame();
&footer_simple(0);



