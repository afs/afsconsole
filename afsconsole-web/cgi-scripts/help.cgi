#!/usr/bin/perl

use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use lib "/opt/perlmodules/";
use Vos;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS
use ConsoleConstants;
require 'ConsoleAccessTimes.pl';
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';

################MODULES###########################

sub heading{
print "<html>";
print "<head><title>CERN AFS Console View - Help</title>";
print "</head>";
print "<body bgcolor=#ffffff font=arial, helevetica>";
print "<HTML><table width=100% cellpadding=1 border=0 cellspacing=0 bgcolor=#000000><tr><td>";
print "<HTML><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeef><tr><td>";
print "<A href='index.cgi'><img border='0'
src='../icons/kfm_home.png'></A> &nbsp&nbsp&nbsp <font size=3 face='verdana, arial, helvetica'>CERN AFS Console View</font>";
print "</td>
<td><div align=right>
</div></td></tr></table>";
print "</table></tr></td>";
print "<HR></HR>";
return 1;
}

sub main_frame{
my $listsrv = &get_all_servers();
$true=1;
while($true eq 1){
	if($listsrv=~/(\w+)(\s)(.*)/){
		$listsrv = $3;
		$server=$1;
		push @servers, $server;
	}elsif($listsrv=~/(\w+)/){
		$true = 0;
	}
}

my $ls = fullpath("ls", "/usr/afsws/bin:/usr/afsws/etc:/usr/sbin:/usr/sue/bin/:/bin");
my $grep = fullpath("grep", "/usr/afsws/bin:/usr/afsws/etc:/usr/sbin:/usr/sue/bin/:/bin");
my $tail = fullpath("tail", "/usr/afsws/bin:/usr/afsws/etc:/usr/sbin:/usr/sue/bin/:/bin:/usr/bin");

print "
<div align=center><table width='100%'>
<tr>
	<td>
	<table border='0' width='100%' cellspacing ='2' cellpadding='3' border ='1' valign='top'>
	<tr>
	<th><div align=left><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
		<tr>
		<td bgcolor='#5FA6D7'>
		<img border='0' src='../icons/kcmpartitions.png'>
		<font size=2 face='verdana, arial, helvetica color='#000000''>
		<b>AFS Availability</font></b>
		</font>
		</td>
		</tr>
	</div></table></th>
	</tr>

	<tr>
	<td><div align=left><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr><td bgcolor='#FFFFCC'>
		<font face='verdana, arial, helvetica' size=2 color='#465e90'>
			Represents in a bar the availability of the service. The value is updated every five minutes.
		</font>
	</td>
	</tr>
	</table></div>
	</td>
	</tr>



	<tr>
	<th><div align=right><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr>
	<td bgcolor='#5FA6D7'>
		<img src='../icons/klaptopdaemon.png'>
		<font size=2 face='verdana, arial, helvetica'>
		<b>AFS Available Performance</b>
		</font>
	</td>
	</tr>
	</table></div></th>
	</tr>";
print "<tr>
	<th><div align=right><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr><td bgcolor='#ffffcc'>
		<font face='verdana, arial, helvetica' size=2 color=#465e90>
		The AFS Performance bar indicates the fraction of volumes not affected by 'slow' partitions, i.e. partitions with increased access times. The calculation of the AFS Performance takes into account how much a partition's access time is increased: the slower the partition, the greater the impact on the performance. The performance information is currently updated every 5 minutes. 
		</font></td></tr></table></td></tr>";

print "
<tr><th><div align=right><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	</table></div></th>
	</tr>


        

	<tr>
	<th><div align=right><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr>
	<td bgcolor='#5FA6D7'>
		<img src='../icons/folder_important.png'>
		<font size=2 face='verdana, arial, helvetica'>
		<b>AFS Alarms / Warnings</b>
		</font>
	</td>
	</tr>
	</table></div></th>
	</tr>";
print "<tr>
	<th><div align=right><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr><td bgcolor='#ffffcc'>
		<font face='verdana, arial, helvetica' size=2 color=#465e90>
		This field shows the current detected alarms in the service. The alarm set is color coded. Alarms will be blue, orange, or red depending on their severity. Alarms are shown for two minutes after they are detected.
		</font></td></tr></table></div></th></tr>";

print "
<tr><th><div align=right><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr>
	<td bgcolor='#5FA6D7'>
		<img src='../icons/tab_remove.png'>
		<font size=2 face='verdana, arial, helvetica'>
		<b>Service Incidents</b>
		</font>
	</td>
	</tr>
	</table></div></th>
	</tr>";

print "<tr>
	<th><div align=right><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr><td bgcolor='#ffffcc'>
	<font face='verdana, arial, helvetica' size=1 color=#465e90>";
	print "<div align=left><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#ffffcc'>";

	print "<tr><font face='verdana, arial, helvetica' size=2 color=#465e90> It shows a list of the last incidents of the AFS service by chronological time. The last ones are at the bottom of the page.</font></tr>";
	print "</table></div>
	</font></td></tr></table></div></th>
</tr>";

print "<tr>
	<th><div align=left><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr>
	<td bgcolor='#5FA6D7'>
		<img src='../icons/arts.png'>
		<font size=2 face='verdana, arial, helvetica'>
		<b>AFS Statistics</b>
		</font></td></tr>
	</table></div></th>
	</tr>

	<tr>
	<td><div align=left><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr><td bgcolor='#ffffcc'>
	<font face='verdana, arial, helvetica' size=2 color=#465e90>
		Links to AFS information in Meter.
	</font>
		</td>
		</tr>
		</table></div></td>
	</tr>";

print "<tr>
<th><div align=right><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr>
	<td bgcolor='#5FA6D7'>
		<img src='../icons/list.png'>
		<font size=2 face='verdana, arial, helvetica'>
		<b>Report Generator</b>
		</font>
	</td>
	</tr>
	</table></div></th>
</tr>";
print "<tr>
<td><div align=right><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr><td bgcolor='#ffffcc'>
	<font face='verdana, arial, helvetica' size=2 color=#465e90>
		<b>Volumes Reports</b>:<br> Sorted by server -> Shows a list of the top 10 volumes by  server ordered by accesses. Sorting by Size and files are also available.
		Sorted by project -> Shows a list of the top 10 volumes by  server ordered by accesses. Sorting by Size and files are also available.
				 Sorted globally -> Shows a list of the top 20 volumes ordered by accesses. Sorting by Size and files are also available.<br>
		<b>Report for projects</b>: Shows a list of the servers and partitions containing volumes for the specified project. Detailed data of partition is provided.<br>
		<b>Report for servers</b>: Shows a list of all the servers in production.<br> Information displayed includes size, files, accesses in the current day, daily average of accesses, and average of accesses in the past hour.<br>
		<b>Report for partitions</b>: It shows graphs with size and used space per partition as well as accesses and files.
	</font>
	</td>
	</tr>
	</table></div></td>
</tr>";


#Closes first column starts second one
print "
</table>
</td>
<td>
<table border='0' width='96%' cellspacing ='2' cellpadding='3' border ='1'>";
print "<tr>
	<th><div align=left><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr>
	<td bgcolor='#5FA6D7'>
		<A href='servers2.cgi'>
		<img border='0' src='../icons/xmag.png'>
		</A>
		<font size=2 face='verdana, arial, helvetica'color='#000000'><b>
		Servers Overview</b></font>
		</font>
	</td>
	</tr>
	</table></div></th>
	</tr>

	<tr>
	<td><div align=left><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr><td bgcolor='#ffffcc'>
		<font face='verdana, arial, helvetica' size=2 color=#465e90>
			Displays information about servers in prodcution.<br>Shows a graph representing servers size and access times as of the last hour. The length of a bar shows the server's size, while the color of the bars represents current access time for a 64kB file.<br>Clicking the plot leads to a page with more detailed graphs. In the upper part of the page, a list of all the servers in production is displayed, clicking on the server name, a new page containing essential server information will be shown. This information includes server partition structure and content, volumes list and rxdebug information.
		</font>
		</td>
		</tr>
		</table></div></td>
		</tr>

		<tr>
		<th><div align=left><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
		<tr>
		<td bgcolor='#5FA6D7'>
			<A href='projects2.cgi'>
			<img border='0' src='../icons/xmag.png'>
			</A>
			<font size=2 face='verdana, arial, helvetica'>
			<b>Projects Overview</b>
			</font>
		</td>
		</tr>
		</table></div></th>
		</tr>

		<tr>
		<td><div align=left><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
		<tr><td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
				It shows a list of the main projects. Leads you to a new page displaying the project location in AFS servers.
		</font>
		</td>
		</tr>
		</table></div></td>
		</tr>

		<tr>
		<th><div align=left><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
		<tr>
		<td bgcolor='#5FA6D7'>
			<A href='users2.cgi'>
			<img border='0' src='../icons/xmag.png'>
			<font size=2 face='verdana, arial, helvetica'>
			</A>
			<b>Users Overview</b>
			</font></td></tr>
		</table></div></th>
		</tr>

		<tr>
		<td><div align=left><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
		<tr><td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
				This forms returns all the information related to any AFS user volume.
			</font>
		</td>
		</tr>
		</table></div></td>
		</tr>";

print "<tr>
<th><div align=right><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr>
	<td bgcolor='#5FA6D7'>
		<img src='../icons/contents.png'>
		<font size=2 face='verdana, arial, helvetica'>
		<b>AFS CERN Cell Logs</b>
		</font>
	</td>
	</tr>
	</table></div></th>
</tr>
<tr>
<td><div align=right><table width='100%' cellspacing='1' cellpadding='3' border='0' bgcolor='#165480'>
	<tr><td bgcolor='#ffffcc'>
	<font face='verdana, arial, helvetica' size=2 color=#465e90>
		There are two different types of logs. Detailed Log shows the complete list of problems registered during the day, sorted by volume and server. Simple log only shows the amount of times that a volume has suffered any type of problem. Using the form we can get the AFS problems log from any of the past 10 days.
	</font>
	</td>
	</tr>
	</table></div></td>
</tr>

</td>
</table></table></div>";
return 1;
}

sub foot{
print "<HR></HR>
<body bgcolor=#ffffff font=arial, helevetica>
<div align=center><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeee><tr><td>
<div align=right><font face='verdana, arial, helvetica' size=2>AFS Service - CERN IT</font></div>
</table></tr></td></div>";
return 1;
}

#****************************************************************************
############################################################################
my $query=new CGI;
print $query->header(-refresh=>120);
my $val = heading();
$val = main_frame();
$val = foot();
