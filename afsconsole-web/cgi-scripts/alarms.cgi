#!/usr/bin/perl

################################################################################
#
#  Script      : alarms.cgi
#
#  Change log  :
#                04.04.2008 - awi - Some cleanup
#
#  Original version by Mario Bolivar, CERN IT/FIO 2006
################################################################################


use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use lib "/opt/perlmodules/";
use Vos;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";


#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub main() {
	&make_imbed_grafx(\$graphs_icons_xmag, "icons/xmag");

	print " <div align=center>

                <table width='100%'>
                <tr>
                   <td>

	              <table border='0' width='100%' cellspacing ='1' cellpadding='3' border ='0'>
		      <tr>
		         <th>
                         <div align=left>

                         <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		         <tr>
		            <td bgcolor='#5FA6D7'>
		              <img src='$graphs_icons_xmag'>
		              <font size=2 face='verdana, arial, helvetica'>
		              <b><font size=2 face='verdana, arial, helvetica' color='#000000'>AFS Service Alarms / Warnings</font></b>
		              </font>
		            </td>
		         </tr>
                         </table>

		         </div>
		      </tr>
  		      <tr>
		         <td>
                         <div align=left>
                         <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		         <tr>
                            <td bgcolor='#FFFFCC'>
		            <div align=left>
		            <font color=#465e90 size=2 face='verdana, arial, helvetica'>";
	
	# add the log lines
	#
	my $date = &today();
	my $file = join '-', $ConsoleConstants::VOLSLOGS_DIR . '/volumes_log', $date;

	open FILE , $file or die "Could not open the file: $!";
	my ($dctr,$sctr,$srvctr) = (0,0,0);
	my @Asrvmsg = ();
	while (<FILE>) {
		if (/^Salvage/ && $sctr<10) {
			print "$_<br>";
			if (/No problems detected/) {
				$sctr = 10;
			}
			$sctr++;
		} elsif (/^Delayed/ && $dctr<10) {
			print "$_<br>";
			$dctr++;
		} elsif (/^Server/) {
			unless (/No problems/) {
				push @Asrvmsg, $_;
				$srvctr++;
				next;
			}
		}
	}
	
	if ($srvctr) {
		@Asrvmsg = reverse @Asrvmsg;
		my $ctr=0;
		while ($ctr<10) {
			print $Asrvmsg[$ctr] . "<br>";
			$ctr++;
		}
	} else {
		print "Servers - No problems detected in the past hours";
	}

	close FILE;
	#
	# end add log lines


	print "             </font>
		            </div>
		            </td>
		         </tr>
		         </table>
                         </div>
	                 </td>
	              </tr>
                      </table>
                   </td>
                </tr>
                </table>

                </div>";

	return 1;
}


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query=new CGI;
print $query->header();

&heading("AFS Service Alarms / Warnings", 0);
&main();
&footer_simple();



