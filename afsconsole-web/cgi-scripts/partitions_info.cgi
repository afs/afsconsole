#!/usr/bin/env perl

#------------------------------------------------------------------------------
# partitions_info.cgi : get partition information in table form
#
# Initial version : Arne Wiebalck CERN IT-FIO/LA, Aug 2007.
#------------------------------------------------------------------------------

use DBI;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
require 'ConsoleMySQL.pl';
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

use ConsoleDB;

#-------------------------------------------------------------------------------
#
# Globals
#
#-------------------------------------------------------------------------------

my $parameter;
my $table = "M_T_904";
my $server;
my $partition;
my $server_partition;
my $entries;
my $orderBy;
my ($start_date, $start_time, $end_date, $end_time);
my %sortedByTexts = (
		     "size"            => "Volume Size",
		     "files"           => "Number of Files",
		     "quota"           => "Quota",
		     "currentaccesses" => "Current Accesses",
		     "currentreads"    => "Current Reads",
		     "currentwrites"   => "Current Writes",
		     "totalaccesses"   => "Total Accesses",
		     "totalreads"      => "Total Read Accesses",
		     "totalwrites"     => "Total Write Accesses",
		     );

#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub convertTimestamp() {

	my $timestamp = shift;

	my ($second, $minute, $hour, $dayOfMonth, 
	    $month, $yearOffset, $dayOfWeek, $dayOfYear, 
	    $daylightSavings) = localtime( $timestamp );

	$year  = $yearOffset + 1900;

	$month = $month + 1;	
	$month = "0$month" if ($month < 10);

	$dayOfMonth = "0$dayOfMonth" if ($dayOfMonth < 10);
	$hour       = "0$hour"       if ($hour < 10);
	$minute     = "0$minute"     if ($minute < 10);
	$second     = "0$second"     if ($second < 10);

	return "$year-$month-$dayOfMonth/$hour:$minute:$second";
}

# FIXME Centralise
sub calc_timestamps() {

	use Time::Local;
	
	my ($start_date, $start_time, $end_date, $end_time) = @_;
	
	# start
	my ($year, $month, $day) = split /-/, $start_date;
	my ($hours, $min, $sec)  = split /:/, $start_time;
	my $times_start = timelocal( $sec, $min, $hours, $day, $month - 1, $year );

	# end
	($year, $month, $day) = split /-/, $end_date;
	($hours, $min, $sec)  = split /:/, $end_time;
	my $times_end = timelocal( $sec, $min, $hours, $day, $month - 1, $year );

	return "$times_start/$times_end";
}

# FIXME Centralise
sub calc_timestampsToday() {

	use Time::Local;
	
	my ($sec, $min, $hrs, $day, $month,
	    $year, $dow, $doy, $dst) = localtime(time);

	# start	
	my $times_start = timelocal( 0, 0, 0, $day, $month, $year );

	# end
	my $times_end = timelocal( 0, 0, 0, $day + 1, $month, $year );

	return "$times_start/$times_end";
}

sub info {
	
	my ($timestamp, $partitions_server, $partitions_partition, 
		$partitions_fillstatus, $partitions_size, $partitions_used, 
		$partitions_volumes, $partitions_online, $partitions_offline, 
		$partitions_committed, $partitions_readaccesstime, 
		$partitions_writeaccesstime);
	
	# time window given
	my $times;
	if ($table eq "M_T_904") {
		if ($today) {
			$times = &calc_timestampsToday();
		} else {
			$times = &calc_timestamps($start_date, $start_time,
									  $end_date, $end_time );
		}
	}

	my $sortedBy;
	if ($orderBy =~ /^server$/) {
		$sortedBy = 'server/partitions';
	} elsif ($orderBy =~ /^time$/) {
		$sortedBy = 'timestamp';
	} elsif ($orderBy =~ /^partition$/) {
		$sortedBy = 'partition';
	}
	
	# XXX Not used
	#$sortedByText = $sortedByTexts{$sortedBy};

	&startTableTable( "Partition's Info for $server_partition", $Tonly );	
	if ($server_partition =~ /^all$/) {
		$server_partition = undef;
	}
	&headline4Table_PartitionsHottest( $srv, 100 );
	$data_counter = 0;
	my @info = ConsoleDB::partitions_info($server_partition, $times,
										  $sortedBy);
	foreach (@info) {
		$timestamp = &convertTimestamp($_->[0]);
		&row4Table_PartitionsHottest($timestamp,
									 $_->[1],
									 $_->[2],
									 $_->[3],
									 $_->[4],
									 $_->[5],
									 $_->[6],
									 $_->[7],
									 $_->[8],
									 $_->[9],
									 $_->[10],
									 $_->[11]);
		$data_counter += 1;
	}	
	&endTableTable();

	if (!$data_counter) {
		print "<p align=center><font face='verdana, arial, helvetica' size=2 color=#FF0000>No data available!</font></p>";
	} 

	return 1;
}


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query = new CGI;
print $query->header();

# Parameter evaluation
foreach ( param() ) {

	# text-only
	if ($_ eq 'T') { 
		
		$Tonly = 1; 
		next;
	}

	# select the data table
	if (/now/) {
		
		my $when = param($_);
		if ($when eq "mostRecent") {
			$table = "M_L_904";
		}
		if ($when eq "today") {
			$today = 1;
		}
		next;
	}

	# date & time
	if (/^start_date$/) {
		
		$start_date = param($_);
                unless ($start_date =~ /^\d\d\d\d-\d\d-\d\d$/) {
		    print "Input error: Invalid start date $start_date\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^end_date$/) {
		
		$end_date = param($_);
                unless ($start_date =~ /^\d\d\d\d-\d\d-\d\d$/) {
		    print "Input error: Invalid end date $start_date\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^start_time$/) {
		
		$start_time = param($_);
                unless ($start_time =~ /^\d\d:\d\d:\d\d$/) {
		    print "Input error: Invalid start time $start_time\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^end_time$/) {
		
		$end_time = param($_);
                unless ($end_time =~ /^\d\d:\d\d:\d\d$/) {
		    print "Input error: Invalid end time $end_time\n";
		    &footer_simple( $Tonly );
		    exit 1;
	        }
		next;
	}

	# partition name
	if (/^partition$/) {
		$server_partition = param($_);
		unless (($server_partition =~ /^afs\d\d\d?(\/\w{1,2})?$/) ||
				($server_partition =~ /^afsdb\d\/(\w{1,2})?$/) ||
				($server_partition =~ /^all$/)) {
			print "Input error: Invalid partition identifier $partition\n";
			&footer_simple( $Tonly );
			exit 1;
		}
		next;
	}

	# order by
	if (/^orderBy$/) {
		
		$orderBy = param($_);
		my $found = 0;
		foreach my $s (@ApartitionFields) {
		    if (lc $s eq $orderBy) {$found = 1;}
		}
		if (!$found) {$orderBy = 'size';}
		next;
	}
}


# determine the number of rows to be displayed on the page
$howManyRows = $display;

# HTML creation
&heading( "Partition's Info", $Tonly );
&info();

&footer_simple( $Tonly );
