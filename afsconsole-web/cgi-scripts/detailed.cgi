#!/usr/bin/perl

use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use DBI;

use FindBin;
use lib "$FindBin::Bin/../modules";
use ConsoleConstants;
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

################MODULES###########################
sub heading{
print "<html>";
print "<head><title>CERN AFS Console View - AFS Today's Log</title>";
print "</head>";
print "<body bgcolor=#ffffff font=arial, helevetica>";
print "<HTML><table width=100% cellpadding=1 border=0 cellspacing=0 bgcolor=#000000><tr><td>";
print "<HTML><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeef><tr><td>";
print "<A href='index.cgi'><img border='0' src='../icons/kfm_home.png'></A> &nbsp&nbsp&nbsp <font size=3 face='verdana, arial, helvetica'><A name='init'>CERN AFS Console View</A></font>";
print "</table></tr></td>";
print "</table></tr></td>";
print "<HR></HR>";
return 1;
}

sub main{

# read the whole encoded string into a var
read(STDIN,$encoded,$ENV{'CONTENT_LENGTH'});
my $date = today();

my $file = join "-", $ConsoleConstants::VOLSLOGS_DIR.'/volumes_log', $date;
open FILE, $file or die "Could not open file $file: $!";
my (%servers,%volumes,%salvage,%threads,%other);
while(<FILE>){
	$volumes{$2}=$2 if($_=~/(.*)Volume: (.*) delayed(.*)/);
	$servers{$2}=$2 if($_=~/(.*)Server: (.*) Volume(.*)/);
	$salvage{$3}=$3 if($_=~/(.*)\/(.*)\/(.*).vol;(.*)/);
	$threads{$1}=$1 if($_=~/Server-Server (.*) - (.*)/);
}
close FILE;

open FILE, $file or die "Could not open file: $!";
print "
<div align=left><table width='100%'>
<tr>
	<td>
			<table border='0' width='96%' cellspacing ='1' cellpadding='3' border ='0'>
			<tr>
			<th><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr>
			<td bgcolor='#5FA6D7'>
			<img src='../icons/xmag.png'>
			<div align=center>
			<b><font size=2 face='verdana, arial, helvetica' color='#000000'>Salvage Required volumes the $date</font></b>
			</font></div>
			</td>
			</tr>
			</table></div></th>
			</tr>

			<tr>
			<td><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr><td bgcolor='#FFFFCC'>
			<b><font face='verdana, arial, helvetica' size=2 color='#465e90'>";
			my $elem = keys %salvage;
			print "<b><font face='verdana, arial, helvetica' size=2 color='#465e90'>No salvage alarms today</font><b><br>" if ($elem eq 0);
			print "<br>Volumes presenting problems<br>" if ($elem > 0);
			while(my ($key,$value) = each %salvage){
				print "<a href='#$value'><font face='verdana, arial, helvetica' size=2 color='#465e90'> $value </font></a> &nbsp&nbsp&nbsp";
			}

			print "</font><b>
			</font>
			</td>
			</tr>
			</table></div></td></tr></table>
	</td>
	</tr>
	</table></div>";

close FILE;
open FILE , $file or die "Could not open file: $!";

#Delayed table
print "
<div align=left><table width='100%'>
<tr>
	<td>
			<table border='0' width='96%' cellspacing ='1' cellpadding='3' border ='0'>
			<tr>
			<th><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr>
			<td bgcolor='#5FA6D7'>
			<img src='../icons/xmag.png'>
			<div align=center>
			<b><font size=2 face='verdana, arial, helvetica' color='#000000'>Delayed volumes the $date</font></b>
			</font></div>
			</td>
			</tr>
			</table></div></th>
			</tr>

			<tr>
			<td><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr><td bgcolor='#FFFFCC'>
			<b><font face='verdana, arial, helvetica' size=2 color='#465e90'>";
			my $elem = keys %volumes;
			print "<b><font face='verdana, arial, helvetica' size=2 color='#465e90'>No alarms regarding volumes today</font><b><br>" if ($elem eq 0);
			print "Servers presenting problems<br>" if ($elem > 0);
			while(my ($key,$value) = each %servers){
				print "<a href='#$value'> <font face='verdana, arial, helvetica' size=2 color='#465e90'>$value </font></a> &nbsp&nbsp&nbsp";
			}
			print "<br>Volumes presenting problems<br>" if ($elem > 0);
			while(my ($key,$value) = each %volumes){
				print "<a href='#$value'><font face='verdana, arial, helvetica' size=2 color='#465e90'> $value </font> </a> &nbsp&nbsp&nbsp";
			}
			my $elem = keys %servers;

			print "</font><b>
			</font>
			</td>
			</tr>
			</table></div></td></tr></table>
	</td>
	</tr>
	</table></div>";
close FILE;
print "
<div align=left><table width='100%'>
<tr>
	<td>
			<table border='0' width='96%' cellspacing ='1' cellpadding='3' border ='0'>
			<tr>
			<th><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr>
			<td bgcolor='#5FA6D7'>
			<img src='../icons/xmag.png'>
			<div align=center>
			<b><font size=2 face='verdana, arial, helvetica' color='#000000'>Other system Alarms the $date</font></b>
			</font></div>
			</td>
			</tr>
			</table></div></th>
			</tr>

			<tr>
			<td><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr><td bgcolor='#FFFFCC'>
			<b><font face='verdana, arial, helvetica' size=2 color='#465e90'>";
			my $any=0;
			my $file = join "-", $ConsoleConstants::DAYALARM_PREFIX , $date;
			if(open OTHER, $file){
				my (%others);
				while(<OTHER>){
					my $line = $_;
					print "<b><font face='verdana, arial, helvetica' size=2 color='#465e90'>$line</font></b><br>";
					$any++;
				}
			close OTHER;
			}
			print "<b><font face='verdana, arial, helvetica' size=2 color='#465e90'>No alarms today</font></b><br>" if ($any eq 0);
			print "
			</font></b>
			</td>
			</tr>
			</table></div></td></tr></table>
	</td>
	</tr>
	</table></div>";

#Displays the whole list per volume for salvage
while(my ($key,$value) = each %salvage){
my $file = join '-', $ConsoleConstants::VOLSLOGS_DIR.'/volumes_log', $date;
open FILE, $file or die "Could not open file: $!";
print "
<div align=left><table width='100%'>
<tr>
	<td>
			<table border='0' width='96%' cellspacing ='1' cellpadding='3' border ='0'>
			<tr>
			<th><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr>
			<td bgcolor='#5FA6D7'>
			<A href='#init'><img border='0' src='../icons/3uparrow.png'></A>
			<img src='../icons/xmag.png'>
			<div align=center>
			<b><font size=2 face='verdana, arial, helvetica' color='#000000'><a name='$value'>Alarms regarding volume $value the $date &nbsp&nbsp&nbsp&nbsp <A href='vol_data.cgi?=$value' target='_blank'><font size=2 face='verdana, arial, helvetica' color='#000000'>Volume detailed info</font></A></a></font></b>
			</font></div>
			</td>
			</tr>
			</table></div></th>
			</tr>

			<tr>
			<td><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr><td bgcolor='#FFFFCC'>
			<b><font face='verdana, arial, helvetica' size=2 color='#465e90'>";
			while(<FILE>){
				my $line = $_;
				print "$line<br>" if ($line=~/ (.*)$value.vol(.*)/);
			}
			print "</font><b>
			</font>
			</td>
			</tr>
			</table></div></td></tr></table>
	</td>
	</tr>
	</table></div><br>";
}

#Displays the whole list per server
close FILE;
while(my ($key,$value) = each %servers){
my $file = join '-', $ConsoleConstants::VOLSLOGS_DIR . '/volumes_log', $date;
open FILE, $file or die "Could not open file: $!";
print "
<div align=left><table width='100%'>
<tr>
	<td>
			<table border='0' width='96%' cellspacing ='1' cellpadding='3' border ='0'>
			<tr>
			<th><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr>
			<td bgcolor='#5FA6D7'>
			<A href='#init'><img border='0' src='../icons/3uparrow.png'></A>
			<img src='../icons/xmag.png'>
			<div align=center>
			<b><font size=2 face='verdana, arial, helvetica' color='#000000'><a name='$value'>Alarms regarding Server $value the $date &nbsp&nbsp&nbsp&nbsp <A href='servers_data.cgi?=$value' target='_blank'><font size=2 face='verdana, arial, helvetica' color='#000000'>Server detailed info</font></A></a></font></b>
			</font></div>
			</td>
			</tr>
			</table></div></th>
			</tr>

			<tr>
			<td><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr><td bgcolor='#FFFFCC'>
			<b><font face='verdana, arial, helvetica' size=2 color='#465e90'>";
			while(<FILE>){
				my $line = $_;
				print "$line<br>" if ($line=~/(.*) $value (.*)/);
			}
			print "
			</b></font>
			</td>
			</tr>
			</table></div></td></tr></table>
	</td>
	</tr>
	</table></div>";

close FILE;
}

#Displays the whole list per volume
while(my ($key,$value) = each %volumes){
my $file = join '-', $ConsoleConstants::VOLSLOGS_DIR .'/volumes_log', $date;
open FILE, $file or die "Could not open file: $!";
print "
<div align=left><table width='100%'>
<tr>
	<td>
			<table border='0' width='96%' cellspacing ='1' cellpadding='3' border ='0'>
			<tr>
			<th><div align=center><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr>
			<td bgcolor='#5FA6D7'>
			<A href='#init'><img border='0' src='../icons/3uparrow.png'></A>
			<img src='../icons/xmag.png'>
			<div align=center>
			<b><font size=2 face='verdana, arial, helvetica' color='#000000'><a name='$value'>Alarms regarding volume $value the $date &nbsp&nbsp&nbsp&nbsp <A href='vol_data.cgi?=$value' target='_blank'><font size=2 face='verdana, arial, helvetica' color='#000000'>Volume detailed info</font></A></a></font></b>
			</font></div>
			</td>
			</tr>
			</table></div></th>
			</tr>

			<tr>
			<td><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr><td bgcolor='#FFFFCC'>
			<b><font face='verdana, arial, helvetica' size=2 color='#465e90'>";
			while(<FILE>){
				my $line = $_;
				print "$line<br>" if ($line=~/(.*) Volume: $value (.*)/);
			}
			print "</font><b>
			</font>
			</td>
			</tr>
			</table></div></td></tr></table>
	</td>
	</tr>
	</table></div><br>";
close FILE;
}

return 1;
}

sub foot{
print "<HR></HR>
<body bgcolor=#ffffff font=arial, helevetica>
<div align=center><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeee><tr><td>
<div align=right><font face='verdana, arial, helvetica' size=2>AFS Service - CERN IT</font></div>
</table></tr></td></div>";
return 1;
}


#****************************************************************************
my $query=new CGI;
print $query->header(-refresh=>120);
my $val = heading();
$val = main();
$val = foot();
