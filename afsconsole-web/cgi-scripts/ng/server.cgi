#!/usr/bin/python3

import cgi
import cgitb
import re
from sqlobject import *
from sqlobject.sqlbuilder import Select
from datetime import datetime
from string import Template
import sys
import codecs

secrets_path = '/usr/share/afsconsole/common/AFSConsoleConf.pm'

def get_db_uri():
    with open(secrets_path) as f:
        secrets = f.read()
        values = {}
        for needle in ['db_host', 'db_name', 'db_ro_username', 'db_ro_passwd']:
            p = re.compile("%s = '(.*)'" % needle)
            values[needle] = p.search(secrets).group(1)
        return "mysql://%s:%s@%s/%s" % (values['db_ro_username'],
                                        values['db_ro_passwd'],
                                        values['db_host'],
                                        values['db_name'])

class Servers(SQLObject):
    nodename = StringCol(length=256)
    timestamp = IntCol()
    server = StringCol(length=64)
    idleThreads = IntCol(dbName="idleThreads")
    serverConnections = IntCol(dbName="serverConnections")
    clientConnections = IntCol(dbName="clientConnections")
    waitedCalls = IntCol(dbName="waitedCalls")
    recent = BoolCol()
    avgd = BoolCol()


cgitb.enable()
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach()) # to allow printing utf8
print("Content-Type: text/html")     # HTML is following
print()                              # blank line, end of headers

# connect to the DB
sqlhub.processConnection = connectionForURI(get_db_uri())

form = cgi.FieldStorage()
server = form['server'].value
try:
    count = int(form['count'].value)
except:
    count = 500

entries = Servers.select(Servers.q.server==server,
    orderBy=-Servers.q.timestamp,limit=count)
entries = list(entries)
entries.reverse()

idleThreadsJS = ''
serverConnectionsJS = ''
clientConnectionsJS = ''
waitedCallsJS = ''
for entry in entries:
    d = datetime.fromtimestamp(entry.timestamp)
    # convert the entry data to JS
    idleThreadsJS += '[Date.UTC(%d, %d, %d, %d, %d, %d), %d],' % \
        (d.year, d.month-1, d.day, d.hour, d.minute, d.second, 
        entry.idleThreads)
    serverConnectionsJS += '[Date.UTC(%d, %d, %d, %d, %d, %d), %d],' % \
        (d.year, d.month-1, d.day, d.hour, d.minute, d.second,
        entry.serverConnections)
    clientConnectionsJS += '[Date.UTC(%d, %d, %d, %d, %d, %d), %d],' % \
        (d.year, d.month-1, d.day, d.hour, d.minute, d.second,
        entry.clientConnections)
    if entry.waitedCalls != None:
        waitedCallsJS += '[Date.UTC(%d, %d, %d, %d, %d, %d), %d],' % \
            (d.year, d.month-1, d.day, d.hour, d.minute, d.second,
            entry.waitedCalls)


# print the HTML from the template files
t = form['type'].value

if t in ['connections', 'threads', 'waited-calls']:
    with open('templates/server-%s.html' % t, encoding='utf-8') as t:
        template = Template(t.read())
        print(template.substitute(server=server, 
                                waitedCallsJS=waitedCallsJS,
                                serverConnectionsJS=serverConnectionsJS,
                                clientConnectionsJS=clientConnectionsJS,
                                idleThreadsJS=idleThreadsJS))
