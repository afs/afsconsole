#!/usr/bin/env perl

#------------------------------------------------------------------------------
# partitions_plot.cgi : plot partition data
#
# Initial version : Arne Wiebalck CERN IT-FIO/LA, Aug 2007.
#------------------------------------------------------------------------------

use DBI;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);

use File::Temp qw/ :mktemp  /;
use File::stat;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
require 'ConsoleMySQL.pl';
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

use ConsoleDB;

#-------------------------------------------------------------------------------
#
# Globals
#
#-------------------------------------------------------------------------------

my $data_path  = '/tmp/afs-console';

my $parameter;
my $table = "M_T_904";
my $server;
my $entries;
my ($start_date, $start_time, $end_date, $end_time);
my %HsortedByTexts = (
		     "size"            => "Volume Size",
		     "files"           => "Number of Files",
		     "quota"           => "Quota",
		     "currentaccesses" => "Current Accesses",
		     "currentreads"    => "Current Reads",
		     "currentwrites"   => "Current Writes",
		     "totalaccesses"   => "Total Accesses",
		     "totalreads"      => "Total Read Accesses",
		     "totalwrites"     => "Total Write Accesses",
		     );

my %HnumberOfVolumes = ();
my %Hsize  = ();
my %HfillStatus = ();
my %HreadAccessTimes  = ();
my %HwriteAccessTimes = ();


#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub getSortedKeys() {

	$_Hhash = shift;
	
	my @Aunsorted = ();

	# get the keys
	while (($key, $value) = each %$_Hhash) {
	
		push @Aunsorted, $key;
	}
	
	# sort'em
	my @Asorted = sort( @Aunsorted );

	return @Asorted;
}


sub shortTime() {

	my $ts = shift;

	($second, $minute, $hour, $dayOfMonth, $month, 
	 $yearOffset, $dayOfWeek, $dayOfYear, $daylightSavings) = localtime( $ts );

	$month = $month + 1;

	$dayOfMonth = "0$dayOfMonth" if ($dayOfMonth < 10);
	$month      = "0$month" if ($month < 10);
	$hour       = "0$hour" if ($hour < 10);
	$minute     = "0$minute" if ($minute < 10);

	return "$dayOfMonth-$month $hour:$minute";
}


sub findNiceInterval() {

	my $intvl = shift;

	my $tmp = $intvl % 3600;
	if ($tmp<1800) {
		$intvl = $intvl - $tmp;
	} else {
		$intvl = $intvl + (3600 - $tmp);
	}

	return $intvl;
}


sub createXtics() {

	my ($start, $end) = @_;
	my $intvl = ($end - $start) / 10;
	
	$intvl = &findNiceInterval( $intvl );
	$start = &findNiceInterval( $start );

	my $xtics = "(";
	
	$i = $start;
	while ($i < $end) {
		
		$xtics = "$xtics \"" . &shortTime( $i ) . "\" $i,"; 
		$i = $i + $intvl;
	} 
	

	# remove the last ','
	chop $xtics;

	return "$xtics )";
}

sub generate_numberOfVolumes_plot() {

	my ($server, $partition, $start, $end) = @_;

	# fill the data file
	my ($fhd, $datafile) = mkstemps( "$data_path/tmpfileXXXXXX", ".dat");

	my @Asorted = &getSortedKeys( \%HnumberOfVolumes );

	foreach $t (@Asorted) {

		my $value = $HnumberOfVolumes{$t};
		print $fhd "$t\t$value\n"; 
	}
        # end fill data file

	# generate the plot
	my ($fhp, $plotfile) = mkstemps( "$data_path/tmpfileXXXXXX", ".png");

	# FIXME
	my $gnuplot = "/usr/bin/gnuplot";

	my $xtics = &createXtics( $start, $end );

	open GNUPLOT, "|$gnuplot" or die "Unable to start gnuplot: $!";
	print GNUPLOT <<gnuplot_Commands_Done;
	
	    set term png font ",10" size 640,380
	    set output "$plotfile"
	    set title "Number of Volumes for $server/$partition" 
	    set xlabel "Timestamp" offset 0,-1 
	    set ylabel "Number of Volumes" 
	    set border
	    set key outside samplen 1.75 height 0.1
	    set xtics $xtics
	    set xtics rotate by 90 right
	    set xrange[$start:$end]
	plot "$datafile" title " volumes" with linesp

gnuplot_Commands_Done

close (GNUPLOT);

	return $plotfile;
}

sub generate_fillStatus_plot() {

	my ($server, $partition, $start, $end) = @_;

	# fill the data file
	my ($fhd, $datafile) = mkstemps( "$data_path/tmpfileXXXXXX", ".dat");

	my @Asorted = &getSortedKeys( \%HfillStatus );

	foreach $t (@Asorted) {

		my $value = $HfillStatus{$t};
		print $fhd "$t\t$value\n"; 
	}
        # end fill data file

	# generate the plot
	my ($fhp, $plotfile) = mkstemps( "$data_path/tmpfileXXXXXX", ".png");

	# FIXME
	my $gnuplot = "/usr/bin/gnuplot";

	my $xtics = &createXtics( $start, $end );

	open GNUPLOT, "|$gnuplot" or die "Unable to start gnuplot: $!";
	print GNUPLOT <<gnuplot_Commands_Done;
	
	    set term png font ",10" size 640,380
	    set output "$plotfile"
	    set title "Fill Status for $server/$partition" 
	    set xlabel "Timestamp" offset 0,-1 
	    set ylabel "Fill Status [%]" 
	    set border
	    set key outside samplen 1.75 height 0.1
	    set xtics $xtics
	    set xtics rotate by 90 right
	    set xrange[$start:$end]
	plot "$datafile" title " fill status" with linesp

gnuplot_Commands_Done

close (GNUPLOT);

	return $plotfile;
}

sub generate_size_plot() {

	my ($server, $partition, $start, $end) = @_;

	# fill the data file
	my ($fhd, $datafile) = mkstemps( "$data_path/tmpfileXXXXXX", ".dat");

	my @Asorted = &getSortedKeys( \%Hsize );

	foreach $t (@Asorted) {

		my $value = $Hsize{$t};
		print $fhd "$t\t$value\n"; 
	}
        # end fill data file

	# generate the plot
	my ($fhp, $plotfile) = mkstemps( "$data_path/tmpfileXXXXXX", ".png");

	# FIXME
	my $gnuplot = "/usr/bin/gnuplot";

	my $xtics = &createXtics( $start, $end );

	open GNUPLOT, "|$gnuplot" or die "Unable to start gnuplot: $!";
	print GNUPLOT <<gnuplot_Commands_Done;
	
	    set term png font ",10" size 640,380
	    set output "$plotfile"
	    set title "Size of $server/$partition" 
	    set xlabel "Timestamp" offset 0,-1 
	    set ylabel "Size [kByte]" 
	    set border
	    set key outside samplen 1.75 height 0.1
	    set xtics $xtics
	    set xtics rotate by 90 right
	    set xrange[$start:$end]
	plot "$datafile" title " size" with linesp

gnuplot_Commands_Done

close (GNUPLOT);

	return $plotfile;
}

sub generate_accessTimes_plot() {

	my ($server, $partition, $start, $end) = @_;
       

	# fill the data file
	my ($fhd, $datafile) = mkstemps( "$data_path/tmpfileXXXXXX", ".dat");
	
	my @AsortedRead  = &getSortedKeys( \%HreadAccessTimes );
	my @AsortedWrite = &getSortedKeys( \%HwriteAccessTimes );

	foreach $t (@AsortedRead) {

		my $readValue  = $HreadAccessTimes{$t};
		my $writeValue = $HwriteAccessTimes{$t};
		print $fhd "$t\t$readValue\t$writeValue\n"; 		
	}
        # end fill data file

	# generate the plot
	my ($fhp, $plotfile) = mkstemps( "$data_path/tmpfileXXXXXX", ".png");

	$msg = "$plotfile " . " $datafile"; 

	# FIXME
	my $gnuplot = "/usr/bin/gnuplot";

	$xtics = &createXtics( $start, $end );
	
	if ($cutoff !~ /\d+/) {
		$cutoff = '';	
	}

	my $plot = '';

	$plot = "plot \"$datafile\" using 1:2 title \" read\" with linesp" 
	    if ($readAccessTimes =~ /on/ && $writeAccessTimes !~ /on/);
	$plot = "plot \"$datafile\" using 1:3 title \" write\" with linesp" 
	    if ($readAccessTimes !~ /on/ && $writeAccessTimes =~ /on/);
	$plot = "plot \"$datafile\" using 1:2 title \" read\" with linesp, \"$datafile\" using 1:3 title \" write\" with linesp" 
	    if ($readAccessTimes =~ /on/ && $writeAccessTimes =~ /on/);

	open GNUPLOT, "|$gnuplot" or die "Unable to start gnuplot: $!";
	print GNUPLOT <<gnuplot_Commands_Done;
	
	    set term png font ",10" size 640,380
	    set output "$plotfile"
	    set title "Access Times of $server/$partition" 
	    set xlabel "Timestamp" offset 0,-1 
	    set ylabel "Access Times [micro secs]" 
	    set border
	    set key outside samplen 1.75 height 0.1
	    set xtics $xtics
	    set xtics rotate by 90 right
	    set xrange[$start:$end]
	    set yrange[0:$cutoff]
	    $plot


gnuplot_Commands_Done

close (GNUPLOT);

	return $plotfile;
}


sub convertTimestamp() {

	my $timestamp = shift;

	my ($second, $minute, $hour, $dayOfMonth, 
	    $month, $yearOffset, $dayOfWeek, $dayOfYear, 
	    $daylightSavings) = localtime( $timestamp );

	$year  = $yearOffset + 1900;

	$month = $month + 1;	
	$month = "0$month" if ($month < 10);

	$dayOfMonth = "0$dayOfMonth" if ($dayOfMonth < 10);
	$hour       = "0$hour"       if ($hour < 10);
	$minute     = "0$minute"     if ($minute < 10);
	$second     = "0$second"     if ($second < 10);

	return "$year-$month-$dayOfMonth/$hour:$minute:$second";
}

# FIXME Centralise
sub calc_timestamps() {

	use Time::Local;
	
	my ($start_date, $start_time, $end_date, $end_time) = @_;
	
	# start
	my ($year, $month, $day) = split /-/, $start_date;
	my ($hours, $min, $sec)  = split /:/, $start_time;
	my $times_start = timelocal( $sec, $min, $hours, $day, $month - 1, $year );

	# end
	($year, $month, $day) = split /-/, $end_date;
	($hours, $min, $sec)  = split /:/, $end_time;
	my $times_end = timelocal( $sec, $min, $hours, $day, $month - 1, $year );

        # adjust the time window to 5 hours at least
	if ($times_end - $times_start < 5 * 3600) {
		
		$times_start = $times_end - 5 * 3600;
	}

	return "$times_start/$times_end";
}

# FIXME Centralise
sub calc_timestampsToday() {

	use Time::Local;
	
	my ($sec, $min, $hrs, $day, $month,
	    $year, $dow, $doy, $dst) = localtime(time);

	# start	
	my $times_start = timelocal( 0, 0, 0, $day, $month, $year );

	# end
	my $times_end = $times_start + 86400;

	return "$times_start/$times_end";
}

sub plot {
	
	my ($timestamp, $partitions_server, $partitions_partition, 
        $partitions_fillstatus, $partitions_size, $partitions_used, 
		$partitions_volumes, $partitions_online, $partitions_offline, 
		$partitions_committed, $partitions_readaccesstime, 
		$partitions_writeaccesstime); 
	if ($partition) {
		($server, $partition) = split /\//, $partition;
	}
	
	my $times;
	if ($today) {
		$times = &calc_timestampsToday();
	} else {
		$times = &calc_timestamps($start_date, $start_time,
								  $end_date, $end_time);
	}
	# Not needed for DB but for gnuplot display below -- Just keep it
	($start, $end) = split /\//, $times;

	# FIXME
	&startTableTable2( "Partition's Data Plot for $server/$partition", $Tonly );	
	$data_counter = 0;
	my @data = ConsoleDB::partitions_plot("$server/$partition", $times);

	# get the data
	foreach (@data) {
		$HnumberOfVolumes{$_->[0]}  = $_->[6];
		$Hsize{$_->[0]}             = $_->[4];
		$HfillStatus{$_->[0]}       = $_->[3];
		$HreadAccessTimes{$_->[0]}  = $_->[10];
		$HwriteAccessTimes{$_->[0]} = $_->[11];

		$data_counter += 1;
    }

	$endScript = time();

	if (!$data_counter) {
		
		print "<p align=center><font face='verdana, arial, helvetica' size=2 color=#FF0000>No data available!</font></p>";
		print "<br>";
		&endTableTable();
		return 1;
	}

	$startPlot = time();

	# generate the plot(s)

	# (1) Number of Volumes 
	my $numberOfVolumes_plot = &generate_numberOfVolumes_plot( $server, $partition, $start, $end )  
	    if ($numberOfVolumes =~ /on/);

	# (2) Size
	my $size_plot = &generate_size_plot( $server, $partition, $start, $end ) 
	    if ($size =~ /on/);

	# (3) Fill Status
	my $fillStatus_plot = &generate_fillStatus_plot( $server, $partition, $start, $end ) 
	    if ($fillStatus =~ /on/);

	# (4) Read/Write Access Times
	my $accessTimes_plot = &generate_accessTimes_plot( $server, $partition, $start, $end ) 
	    if ($readAccessTimes =~ /on/ || $writeAccessTimes =~ /on/);

	# embed the graph(s)
	print "<div align='center'>";

	my $plotctr = 0;

	if ($numberOfVolumes =~ /on/) {
		print "<img border='1' style=\"border:1px solid #165480\" src='./png.cgi?=$numberOfVolumes_plot'>";
		$plotctr += 1;
	}
	if ($size =~ /on/) {
		print "<img border='1' style=\"border:1px solid #165480\" src='./png.cgi?=$size_plot'>" ;
		$plotctr += 1;
	}
	if ($fillStatus =~ /on/) {
		print "<img border='1' style=\"border:1px solid #165480\" src='./png.cgi?=$fillStatus_plot'>" ;
		$plotctr += 1;
	}
	if ($readAccessTimes =~ /on/ || $writeAccessTimes =~ /on/) {
		print "<img border='1' style=\"border:1px solid \#165480\" src='./png.cgi?=$accessTimes_plot'>";
		$plotctr += 1;
	}

	print "</div>";

	print "<br>";
	&endTableTable();

	$endPlot = time();
	
	my $delta = $endScript - $startScript;
	my $deltaPlot = $endPlot - $startPlot;

	my $action = 'partitions_plot.cgi';

	print "<form method='GET' action=$action name='info' target='_parent'>";

	print "<div align='center'>
                 <font face='verdana, arial, helvetica' size=2 color=#FF0000>Found $data_counter items in $delta secs, plotted in $deltaPlot secs</font>
                 &nbsp;&nbsp;&nbsp;
                 &nbsp;&nbsp;&nbsp; From :  <input type='text' name='start_date' value=$start_date maxlength=10 size=10>
                                    <input type='text' name='start_time' value=$start_time maxlength=8 size=8>
                              To :  <input type='text' name='end_date' value=$end_date maxlength=10 size=10>
                                  <input type='text' name='end_time' value=$end_time maxlength=8 size=8>

                              &nbsp;&nbsp;&nbsp;cut off (&mu;s) <input type='text' name='cutoff' value='' maxlength=7 size=7>

                                                <form method='GET' action=$action name='files' target='_parent'>
                                                <input type='hidden' name='size' checked value=$size>

                                                <form method='GET' action=$action name='quota' target='_parent'>
                                                <input type='hidden' name='numberOfVolumes' checked value=$numberOfVolumes>

                                                <form method='GET' action=$action name='size' target='_parent'>
                                                <input type='hidden' name='fillStatus' checked value=$fillStatus>

                                                <form method='GET' action=$action name='current_accesses' target='_parent'>
                                                <input type='hidden' name='readAccessTimes' checked value=$readAccessTimes>

                                                <form method='GET' action=$action name='current_reads' target='_parent'>
                                                <input type='hidden' name='writeAccessTimes' checked value=$writeAccessTimes>

                                                <input type='hidden' name='partition' value=$server/$partition maxlength=23 size=23>"; 
	
	print "<input type='submit' value='Submit'>
	       </div>";
	return 1;
}


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query = new CGI;
print $query->header();

$startScript = time();
$endScript   = 0;

# Parameter evaluation
foreach (param()) {

	# text-only
	if ($_ eq 'T') { 
		
		$Tonly = 1; 
		next;
	}

	# select the data table
	if (/^now$/) {
		
		my $when = param($_);
		if ($when eq "mostRecent") {
			$table = "M_L_904";
		}
		if ($when eq "today") {
			$today = 1;
		}
		next;
	}

	# date & time
	if (/^start_date$/) {
		
		$start_date = param($_);
                unless ($start_date =~ /^\d\d\d\d-\d\d-\d\d$/) {
		    print "Input error: Invalid start date $start_date\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^end_date$/) {
		
		$end_date = param($_);
                unless ($start_date =~ /^\d\d\d\d-\d\d-\d\d$/) {
		    print "Input error: Invalid end date $start_date\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^start_time$/) {
		
		$start_time = param($_);
                unless ($start_time =~ /^\d\d:\d\d:\d\d$/) {
		    print "Input error: Invalid start time $start_time\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^end_time$/) {
		
		$end_time = param($_);
                unless ($end_time =~ /^\d\d:\d\d:\d\d$/) {
		    print "Input error: Invalid end time $end_time\n";
		    &footer_simple( $Tonly );
		    exit 1;
	        }
		next;
	}

	# partition name
	if (/^partition$/) {
		
		$partition = param($_);
                unless (($partition =~ /^afs\d\d\d?\/\w{1,2}$/) ||
                        ($partition =~ /^afsdb\d\/\w{1,2}$/)) {
                   print "Input error: Invalid partition identifier $partition\n";
		   &footer_simple( $Tonly );
		   exit 1;
                }
		next;
	}	

	# cut off time
	if (/^cutoff$/) {
		
		$cutoff = param($_);
                unless ($cutoff =~ /^\d+$/) {
                  $cutoff = 100000;
                }
		next;
	}	

	# what to plot
	if (/^size$/) {
		
		$size = param($_);
                if ($size ne "on") {$size = "on";}
		next;
	}	
	if (/^fillStatus$/) {
		
		$fillStatus = param($_);
                if ($fillStatus ne "on") {$fillStatus = "on";}
		next;
	}	
	if (/^numberOfVolumes$/) {
		
		$numberOfVolumes = param($_);
                if ($numberOfVolumes ne "on") {$numberOfVolumes = "on";}
		next;
	}	
	if (/^readAccessTimes$/) {
		
		$readAccessTimes = param($_);
                if ($readAccessTimes ne "on") {$readAccessTimes = "on";}
		next;
	}	
	if (/^writeAccessTimes$/) {
		
		$writeAccessTimes = param($_);
                if ($writeAccessTimes ne "on") {$writeAccessTimes = "on";}
		next;
	}	
}


# determine the number of rows to be displayed on the page
$howManyRows = $display;

# HTML creation
&heading( "Plot Partition's Data", $Tonly );

# create the dat/png dir
mkdir($data_path);

# cleanup files older than 5 minutes
@files = <$data_path/*>;
$time = time;
foreach $f (@files) {
	if (stat($f)->ctime < $time - 300) {
		unlink($f);
	}
}

# generate the plots
&plot();
&footer_simple($Tonly);


