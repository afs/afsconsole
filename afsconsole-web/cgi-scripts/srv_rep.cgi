#!/usr/bin/perl

################################################################################
#
#  Script      : srv_rep.cgi
#
#                The report over all servers
#
#  Change log:   
#                20.03.2008 awi Change to Lemon DB  
#
#                27.11.2007 awi Outsourced ORACLE & constants 
#
#  Original version by Mario Bolivar, CERN IT/FIO 2006
################################################################################


use DBI;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use lib "/opt/perlmodules/";
use Vos;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
require 'ConsoleMySQL.pl';
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

use ConsoleDB;

my $ctr = 0;

#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------


sub heading{
	&make_imbed_grafx(\$graphs_icons_kfm_home, "icons/kfm_home");
	print "<html>";
	print "<head><title>CERN AFS Console View - Servers Report</title>";
	print "</head>";
	print "<body bgcolor=#ffffff font=arial, helevetica>";
	print "<HTML><table width=100% cellpadding=1 border=0 cellspacing=0 bgcolor=#000000><tr><td>";
	print "<HTML><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeef><tr><td>";
	print "<A href='index.cgi'><img border='0'
src='$graphs_icons_kfm_home'></A> &nbsp&nbsp&nbsp <font size=3 face='verdana, arial, helvetica'>CERN AFS Console View</font>";
	print "</table></tr></td>";
	print "</table></tr></td>";
	print "<HR></HR>";
	return 1;
}

sub get_seconds{
	my @timeData = localtime(time);
	my($second,$minute,$hour);
	$second=$timeData[0];
	$minute=$timeData[1];
	$hour=$timeData[2];
	my $total = ($hour*3600)+($minute*60)+$second;
	return $total;
}

sub main_frame{
	&make_imbed_grafx(\$graphs_icons_xmag, "icons/xmag");
	my ($tsize,$tused,$tfree,$tperc,$tfiles,$tvols,$taccs,$srv)=0;
	print "
<div align=center><table width='100%'>
<tr>
<td>
	<table border='0' width='100%' cellspacing ='1' cellpadding='3' border ='0'>
		<tr>
		<th><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		<tr>
		<td bgcolor='#5FA6D7'>
		<img src='$graphs_icons_xmag'>
		<font size=2 face='verdana, arial, helvetica'>
		<b><font size=2 face='verdana, arial, helvetica' color='#000000'>Servers Report</font></b>
		</font>
		</td>
		</tr>
		</div></table></th>
		</tr>

		<tr>
		<td><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		<tr><td bgcolor='#FFFFCC'>
		<div align=center>
		<font color=#465e90 size=2 face='verdana, arial, helvetica'>";
	head_table();
	open FILE, $ConsoleConstants::CONSOLE_FILE or die "Could not open file: $!";
	while(<FILE>){
		last if ($_=~/PROJECTS/);
		if($_=~/afs(.*)/ || $_=~/lxfsrd4008/){
			row_table();
		}
	}
	total();
	print "</tr></table></div>";

	table();
	
	print "</font>
		</div>
		</font>
		</td>
		</tr>
		</table></div>
	</td>
	</tr>
	</table></table></div>";



	return 1;
	close FILE;
}

sub timestamp{
	my($second,$minute,$hour,$day,$month,$year,$date);
	my @timedata = localtime(time);
	$second=$timedata[0];
	$minute=$timedata[1];
	$hour=$timedata[2];
	$day=$timedata[3];
	$month=$timedata[4]+1;
	my($lsec,$lmin,$lhour,$lday,$lmonth);

	$month = "0".$month if(($lmonth=length($month)) eq 1);
	$day = "0".$day if(($lday=length($day)) eq 1);
	$hour = "0".$hour if(($lhour=length($hour)) eq 1);
	$minute = "0".$minute if(($lmin=length($minute)) eq 1);
	$second = "0".$second if(($lsec=length($second)) eq 1);
	$year=($timedata[5] + 1900 );
	$date="$year"."-"."$month"."-"."$day";

	return $date;
}


sub table{
	
        # add the access time images
	my $rpath_prefix = $ConsoleConstants::GRAPHS;
	my $date         = &timestamp;

	opendir DIR, $rpath_prefix;
	my @access_times_files = grep( /afs\w*\d\d?\.$date\.dat\.(read|write).png/, readdir(DIR) );
	closedir(DIR);
	
	@access_times_files = sort( @access_times_files );

	my $ctr = 1;
	my $srv = '';
	foreach my $i (@access_times_files) {

		if( $i =~ /(afs\w*\d\d?)\.$date\.dat\..*.png$/ ) {
			
			# change of server
			if( $srv ne $1 ) {
				
				$srv = $1;
				print "<hr noshade size=\"3\">\n";
				printf "<div align=center>
                           			        <font face='verdana, arial, helvetica' size=2 color='#465e90'>";
				print "<h2>Access time summary for $srv</h2>";
                                printf "</font></div>\n";
			}			
		}

		print "<img src='../graphs/$i'>\n";   # FIXME - embed
		if( $ctr % 2 == 0 ) {
			print "<br>";
		}
		$ctr++;
	}
	
	return 1;
}

sub midnight_seconds() {
   my @time = localtime();
   my $secs = ($time[2] * 3600) + ($time[1] * 60) + $time[0];

   return $secs;
}

sub head_table{
	print "
<div align=center><table border='0' width='90%'>
<tr>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Server</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Size [GB]</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Free [GB]</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Free [%]</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Files [Millions]</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Volumes</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Accesses [Millions]</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Avg. access rate [1/s]</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>File Server Version</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Volserverrr Version</b></font>
</th>

</tr>";
	return 1;
}

sub row_table{
	my ($server,$disk,$free,$perc,$file, $vols, $acc,$old_acc) = split " ", $_;

    $file = ConsoleDB::volumes_servsumfiles($server);
    $vols = ConsoleDB::volumes_servcountvols($server);

	$tsize+=$disk;
	$tfree+=$free;
	$tperc+=$perc;
	$tfiles+=$file;
	$tvols+=$vols;
	$taccs+=$acc;
	my $total = get_seconds();
	my $avg = $acc/$total;

	$file=$file/1000000;
	$acc=$acc/1000000;
	$srv++;

	$file = sprintf "%.1f" , $file;
	$acc = sprintf "%.1f" , $acc;
	$avg = sprintf "%.0f" , $avg;
	$disk = sprintf "%.0f" , $disk;
	$free = sprintf "%.0f" , $free;

	my $rx = "/usr/sbin/rxdebug";
	my ($file_server,$vos_server,$file_built,$vol_built);
	my $file_server = `$rx -server $server -port 7000 -version`;
	my $vos_server = `$rx -server $server -port 7005 -version`;
	if($file_server=~/AFS version: (.*)/){
		my @parts = split " ",$file_server;
		$file_server = $parts[7];
		$file_built= $parts[9];
	}
	if($vos_server=~/AFS version: (.*)/){
		my @parts = split " ",$vos_server;
		$vos_server = $parts[7];
		$vos_built= $parts[9];
	}

	my $color = '#ffffcc';
	$color = '#d8d8d8' if ($ctr % 2);

	my $acc_rate =  sprintf "%.1f", 1000000.0 * $acc / &midnight_seconds();

	print "<tr>
<td bgcolor=$color>
<A href=servers_data.cgi?=$server><font face='verdana, arial, helvetica' size=2 color=#465e90>$server</font><?A>
</font></td>
<td bgcolor=$color>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$disk
</font></td>
<td bgcolor=$color>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$free
</font></td>
<td bgcolor=$color>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$perc
</font></td>
<td bgcolor=$color>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$file
</font></td>
<td bgcolor=$color>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$vols
</font></td>
<td bgcolor=$color>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$acc
</font></td>
<td bgcolor=$color>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$acc_rate
</font></td>
<td bgcolor=$color>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$file_server - $file_built
</font></td>
<td bgcolor=$color>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
$vos_server - $vos_built
</font></td>

</tr>";
	$ctr++;
	return 1;
}

sub total{
	$tfiles=$tfiles/1000000;	
	$tfiles = sprintf "%.1f" , $tfiles;
	$taccs=$taccs/1000000;
	$taccs = sprintf "%.1f" , $taccs;
	# $tperc=$tperc/$srv;
	if ($tsize>0) {
		$tperc = $tfree/$tsize*100;
	} else {
		$tperc = 0;
	} 
	$tperc = sprintf "%.1f" , $tperc;
	$tsize = sprintf "%.1f" , $tsize;
	$tfree = sprintf "%.1f" , $tfree;

	print "<tr>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90><b>
Total</b>
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$tsize GB</b>
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$tfree GB</b>
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$tperc %</b>
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$tfiles Millions</b>
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$tvols</b>
</font></td>
<td bgcolor='#ffffcc'>
<font face='verdana, arial, helvetica' size=2 color=#465e90>
<b>$taccs Millions</b>
</font></td>
</b></tr>";
}

sub foot{
	print "<HR></HR>
<body bgcolor=#ffffff font=arial, helevetica>
<div align=center><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeee><tr><td>
<div align=right><font face='verdana, arial, helvetica' size=2>AFS Service - CERN IT</font></div>
</table></tr></td></div>";
	return 1;
}

#****************************************************************************
############################################################################
my $query=new CGI;
print $query->header(-refresh=>360);
my $val = heading();
$val = main_frame();
&footer_simple();
