#!/usr/bin/perl

################################################################################
#
#  Script      : servers_data.cgi
#
#                Display information about a given server
#
#  Change log:   
#                20.03.2008 Change from volf_full to lemon
#
#                27.11.2007 awi Outsourced ORACLE & constants 
#
#  Original version by Mario Bolivar, CERN IT/FIO 2006
################################################################################

use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use DBI;
use lib "/opt/perlmodules";
use Vos;


use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS
use ConsoleConstants;
require 'ConsoleMySQL.pl';
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';

use ConsoleDB;

&make_imbed_grafx(\$graphs_icons_kfm_home, "icons/kfm_home");


#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub heading2{
	print "<html>";
	print "<head><title>CERN AFS Console View - Server Global View</title>";
	print "</head>";
	print "<body bgcolor=#ffffff font=arial, helevetica>";
	print "<HTML><table width=100% cellpadding=1 border=0 cellspacing=0 bgcolor=#000000><tr><td>";
	print "<HTML><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeef><tr><td>";
	print "<A href='index.cgi'><img border='0' src='$graphs_icons_kfm_home'></A> &nbsp&nbsp&nbsp <font size=3 face='verdana, arial, helvetica'>CERN AFS Console View</font>";
	print "</table></tr></td>";
	print "</table></tr></td>";
	print "<HR></HR>";
	return 1;
}

sub timestamp{
	my($second,$minute,$hour,$day,$month,$year,$date);
	my @timedata = localtime(time);
	$second=$timedata[0];
	$minute=$timedata[1];
	$hour=$timedata[2];
	$day=$timedata[3];
	$month=$timedata[4]+1;
	my($lsec,$lmin,$lhour,$lday,$lmonth);

	$month = "0".$month if(($lmonth=length($month)) eq 1);
	$day = "0".$day if(($lday=length($day)) eq 1);
	$hour = "0".$hour if(($lhour=length($hour)) eq 1);
	$minute = "0".$minute if(($lmin=length($minute)) eq 1);
	$second = "0".$second if(($lsec=length($second)) eq 1);
	$year=($timedata[5] + 1900 );
	$date="$year"."-"."$month"."-"."$day";

	return $date;
}


sub main_frame{
	read(STDIN,$encoded,$ENV{'CONTENT_LENGTH'});
	my $server;
	my $given_date = '';
	my $ctr = 0;
	foreach (param()) {
		$server = param($_) if ($ctr == 0);
		if ($_ =~ /^\d\d\d\d-\d\d-\d\d$/) {
			$given_date = $_;
		}
		$ctr++;
	}
	if (0 == $ctr) {
	    # no server given
	    print "No server given.";
	    return;
	}
	unless (($server =~ /^afs\d\d\d?$/) || 
		($server =~ /^afs\d\d\d?\/\w\w+$/) || 
		($server =~ /^afsdb\d$/) || 
		($server =~ /^afs\d\d\d?\/\w$/)) {
	    print "Server name \'$server\' unknown.";
	    return;
	} 
		
	my $rxdebug = fullpath("rxdebug", "/usr/afsws/bin:/usr/afsws/etc:/usr/sbin:/usr/sue/bin/:/bin");
	my @output=`$rxdebug $server 7000 -noconn -rxstats`;

	my ($file_server,$file_built);
	my $file_server = `$rxdebug -server $server -port 7000 -version`;
	if($file_server=~/AFS version: (.*)/){
		my @parts = split " ",$file_server;
		$file_server = $parts[7];
		$file_built= $parts[9];
	}


	# block 
	print "<p>
	       <table border='1' width='100%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>

                 <tr>
		   <th>
                     <div align=left>
                       <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			 <tr>
			   <td bgcolor='#5FA6D7'>
			     <font size=2 face='verdana, arial, helvetica'>
			     <b><font size=2 face='verdana, arial, helvetica' color='#000000'>
                             <HTML>
                             <table width=100% cellpadding=2 border=0 cellspacing=1>
                               <tr>
                                 <td align=left>";
	                           #
	                           # item header
	                           #
	                           print "
                                   <font size=2 face='verdana, arial, helvetica'><b>Get the top volumes of $server</b> - File Server Version: $file_server  built: $file_built</font> 
                                 </td>
                               </tr>
                             </table>
                           </td>
			 </tr>
                       </table>
                     </div>
                   </th>
		 </tr>

		 <tr>
		   <td>
                     <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
                       <tr bgcolor='#FFFFCC'>
                         <td  align=center>"; 
                           #
	                   # content
	                   #
                           	  # item action
 	                          my $action = 'volumes_hottest.cgi';
  	                          my $currDate = &today();
	                          my $currTime = &now();
	                          print " 
                                  <form method='GET' action=$action name='entries' target='_parent'>

                                  <input type='hidden' name='now' checked value='mostRecent'>
                                  <br>
                                  <font size=2 face='verdana, arial, helvetica' color='#465e90'>
                                  Get the <input type='text' name='entries' value='20' maxlength=4 size=4> top volumes in terms of 

                                  <select name=\"sortedBy\" size=\"1\" >
				  <option value=\"size\">Volume Size</option>
				  <option value=\"files\">Number of Files</option>
				  <option value=\"quota\">Quota</option>
				  <option value=\"currentaccesses\">Current Accesses</option>
				  <option value=\"currentreads\">Current Reads</option>
				  <option value=\"currentwrites\">Current Writes</option>
				  <option value=\"currentreads + currentwrites\" selected=\"selected\">Current Reads+Writes</option>
	  			  <option value=\"totalaccesses\">Total Accesses</option>
				  <option value=\"totalreads\">Total Read Operations</option>
				  <option value=\"totalwrites\">Total Write Operations</option>

			          </select>";
  
                                  # get the current list of servers and partitions
	                          @prod_servers_S = split /\s+/, &get_all_servers();
	                          push( @prod_servers_S, split /\s+/, &get_all_partitions());	                        
                               	  @prod_servers = sort @prod_servers_S;

                                  print "    	                          
                                  for server/partition
                                  <select name=\"server\" size=\"1\" >
                                  <option value=\"size\">all</option>";
                                  foreach (@prod_servers) {
					  
					  # exclude afsdbX
					  unless (/^afsdb/) {
						  # preselect current server
						  if (/^$server$/) {
							  print "<option value=\"$_\" selected=\"selected\">$_</option>";
						  } else {
							  print "<option value=\"$_\">$_</option>";
						  } 
					  }

					  
				  }
	                          print "</select>&nbsp;
                                  <input type='submit' value='Submit'>
                                  </form><p>

                         </td>
                       </tr>   
 	             </table>
                   </td>
	         </tr>

               </table>";
	       # block end

	# block 
	print "<p>
	       <table border='1' width='100%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>

                 <tr>
		   <th>
                     <div align=left>
                       <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			 <tr>
			   <td bgcolor='#5FA6D7'>
			     <font size=2 face='verdana, arial, helvetica'>
			     <b><font size=2 face='verdana, arial, helvetica' color='#000000'>
                             <HTML>
                             <table width=100% cellpadding=2 border=0 cellspacing=1>
                               <tr>
                                 <td align=left>";
	                           #
	                           # item header
	                           #
	                           print "
                                   <font size=2 face='verdana, arial, helvetica'><b>Server's Data Overview</b></font> 
                                 </td>
                               </tr>
                             </table>
                           </td>
			 </tr>
                       </table>
                     </div>
                   </th>
		 </tr>

		 <tr>
		   <td>
                     <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
                       <tr bgcolor='#FFFFCC'>
                         <td  align=center>"; 
                           #
	                   # content
	                   #
                           ################################################################################################ 
                           my $param = join "-",$server,"all"; 
                           print "<br>";
	                   table_ext();
                      	   print "<br>";
                           ################################################################################################ 
	                   #
	                   # end content
                           #
	                   print "
                         </td>
                       </tr>   
 	             </table>
                   </td>
	         </tr>

               </table>";
	       # block end


	# block 
	print "<p>
	       <table border='1' width='100%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>

                 <tr>
		   <th>
                     <div align=left>
                       <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			 <tr>
			   <td bgcolor='#5FA6D7'>
			     <font size=2 face='verdana, arial, helvetica'>
			     <b><font size=2 face='verdana, arial, helvetica' color='#000000'>
                             <HTML>
                             <table width=100% cellpadding=2 border=0 cellspacing=1>
                               <tr>
                                 <td align=left>";
	                           #
	                           # item header
	                           #

	                           my $date;
                               	   if ($given_date eq '' ) {
					   $date = &timestamp;
	                           } else {
					   $date = $given_date;
				   }

	                           my $t = today;
				   my $y = yesterday;
				   print "<font size=2 face='verdana, arial, helvetica'><b>Access Time Information: $server summary for $date -- ";
				   if ($date eq $y) {
					   print "<a href='servers_data.cgi?=$server&$t'>Show Today</a></b><br>";
				   } else {
					   print "<a href='servers_data.cgi?=$server&$y'>Show Yesterday</a></b><br>";
				   }


	                         print "
                                 </td>
                               </tr>
                             </table>
                           </td>
			 </tr>
                       </table>
                     </div>
                   </th>
		 </tr>

		 <tr>
		   <td>
                     <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
                       <tr bgcolor='#FFFFCC'>
                         <td  align=center>"; 
                           #
	                   # content
	                   #
                           ################################################################################################ 

	# add the access time images (server summary)
	my $rpath_prefix = $ConsoleConstants::GRAPHS;
	opendir DIR, $rpath_prefix;
	my @access_times_files = grep( /$server\.$date\.dat\.(read|write).slidingWindow.png/, readdir(DIR) );
	closedir(DIR);
	
	@access_times_files = sort( @access_times_files );

	my $flag = 0;
	foreach my $i (@access_times_files) {
	    if( $flag == 0 ) {
				
		printf "<div align=center>
                           			        <font face='verdana, arial, helvetica' size=2 color='#465e90'>";

		$flag = 1;
	    }

	    my $radix = substr($i, 0, -4); # remove .png
	    $Radix = $radix; $Radix =~ tr/.-/__/;
	    &make_imbed_grafx("graphs_${Radix}", $radix);
            print "<!-- radix=$radix Radix=$Radix madea graphs_${Radix}=${qq(graphs_${Radix})} -->";

			print "
                                               <script
                                                language=\"JavaScript\">
                                                <!--
                                                button1= new Image();
                                                button1.src = \"${qq(graphs_${Radix})}\"
                                                //-->
                                                </script>";
			
			
			print "
					        <a
						  <img
						   src=\"${qq(graphs_${Radix})}\" border=\"0\"
						   name=\"austausch$ctr\">
					       </a>";
			$ctr++;
			print "<a><img src=\"${qq(graphs_${Radix})}\" border=\"0\"></a>";

		
	}
	print "</font></div>";


                           ################################################################################################
	                   #
	                   # end content
                           #
	                   print "
                         </td>
                       </tr>
 	             </table>
                   </td>
	         </tr>

               </table>";
	       # block end


	# block
	print "<p>
	       <table border='1' width='100%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>

                 <tr>
		   <th>
                     <div align=left>
                       <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			 <tr>
			   <td bgcolor='#5FA6D7'>
			     <font size=2 face='verdana, arial, helvetica'>
			     <b><font size=2 face='verdana, arial, helvetica' color='#000000'>
                             <HTML>
                             <table width=100% cellpadding=2 border=0 cellspacing=1>
                               <tr>
                                 <td align=left>";
	                           #
	                           # item header
	                           #

				   print "<font size=2 face='verdana, arial, helvetica'><b>Access Time Information: $server partitions for $date</b>";

	                         print "
                                 </td>
                               </tr>
                             </table>
                           </td>
			 </tr>
                       </table>
                     </div>
                   </th>
		 </tr>

		 <tr>
		   <td>
                     <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
                       <tr bgcolor='#FFFFCC'>
                         <td  align=center>";
                           #
	                   # content
	                   #
                           ################################################################################################



	opendir DIR, $rpath_prefix;
	my @access_times_files = grep( /\w+\.$date\.dat\.\w\w?\..*.png/, readdir(DIR) );
	closedir(DIR);
	
	@access_times_files = sort( @access_times_files );

	my $srv  = '';
	my $flag = 0;
	foreach my $i (@access_times_files) {

		if( $i =~ /(\w+)\.$date\.dat\.(.*).png$/ ) {
			
			# change of server
			if( $srv ne $1 ) {
				
				$srv = $1;
			}
			
		}
		
		if ($srv eq $server && $2 !~ /overTime/) {


			my $radix;
			if ( $i =~ /(.*)\.png$/) {
				
				$radix = $1;
			}

			$Radix = $radix; $Radix =~ tr/.-/__/;
			&make_imbed_grafx("graphs_${Radix}", "$radix");
#print "<pre> radix=$radix Radix=$Radix madex graphs_$Radix=${qq(graphs_${Radix})}</pre>";
			&make_imbed_grafx("graphs_${Radix}_overTime", "$radix.overTime");
#print "<pre> radix=$radix Radix=$Radix madey graphs_${Radix}_overTime=${qq(graphs_${Radix}_overTime)}</pre>";
			print "
                                               <script
                                                language=\"JavaScript\">
                                                <!--
                                                button1= new Image();
                                                button1.src = \"${qq(graphs_${Radix})}\"
                                                button2= new Image();
                                                button2.src = \"${qq(graphs_${Radix}_overTime)}\"
                                                //-->
                                                </script>";
			
			
			print "
					        <a
						  onmouseout=\"austausch$ctr.src=\'${qq(graphs_${Radix}_overTime)}\';\"
						  onmouseover=\"austausch$ctr.src=\'${qq(graphs_${Radix})}\';\"
						  <img
						   src=\"${qq(graphs_${Radix}_overTime)}\" border=\"0\"
						   name=\"austausch$ctr\">
					       </a> ";
			$ctr++;
			print "<a><img src=\"${qq(graphs_${Radix}_overTime)}\" border=\"0\"></a>";

		}
	}
	print "</font></div>";



                           ################################################################################################
	                   #
	                   # end content
                           #
	                   print "
                         </td>
                       </tr>
 	             </table>
                   </td>
	         </tr>

               </table>";
	       # block end


	# block
	print "<p>
	       <table border='1' width='100%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>

                 <tr>
		   <th>
                     <div align=left>
                       <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			 <tr>
			   <td bgcolor='#5FA6D7'>
			     <font size=2 face='verdana, arial, helvetica'>
			     <b><font size=2 face='verdana, arial, helvetica' color='#000000'>
                             <HTML>
                             <table width=100% cellpadding=2 border=0 cellspacing=1>
                               <tr>
                                 <td align=left>";
	                           #
	                           # item header
	                           #

				   print "<font size=2 face='verdana, arial,
helvetica'><b>Client/Server Connections for $server</b> (<a
href=\"ng/server.cgi?type=connections&server=$server&count=1000000\">all</a>)";

	                         print "
                                 </td>
                               </tr>
                             </table>
                           </td>
			 </tr>
                       </table>
                     </div>
                   </th>
		 </tr>

		 <tr>
		   <td>
                     <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
                       <tr bgcolor='#FFFFCC'>
                         <td  align=center>";
                           #
	                   # content
	                   #
                           ################################################################################################
	#print "<tr><td bgcolor='#FFFFCC' align=left><font face='verdana, arial, helvetica' size=2 color='#465e90'>";
	#print "</td></tr>";
                       print "<iframe src=\"ng/server.cgi?type=connections&server=$server\" width=100% height=400 frameborder=0 marginheight=0 marginwidth=0></iframe>";
                           ################################################################################################
	                   #
	                   # end content
                           #
	                   print "
                         </td>
                       </tr>
 	             </table>
                   </td>
	         </tr>

               </table>";
	       # block end
	# block
	print "<p>
	       <table border='1' width='100%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>

                 <tr>
		   <th>
                     <div align=left>
                       <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			 <tr>
			   <td bgcolor='#5FA6D7'>
			     <font size=2 face='verdana, arial, helvetica'>
			     <b><font size=2 face='verdana, arial, helvetica' color='#000000'>
                             <HTML>
                             <table width=100% cellpadding=2 border=0 cellspacing=1>
                               <tr>
                                 <td align=left>";
	                           #
	                           # item header
	                           #

				   print "<font size=2 face='verdana, arial, helvetica'><b>Idle
Threads for $server (<a
href=\"ng/server.cgi?type=threads&server=$server&count=1000000\">all</a>)</b>";

	                         print "
                                 </td>
                               </tr>
                             </table>
                           </td>
			 </tr>
                       </table>
                     </div>
                   </th>
		 </tr>

		 <tr>
		   <td>
                     <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
                       <tr bgcolor='#FFFFCC'>
                         <td  align=center>";
                           #
	                   # content
	                   #
                           ################################################################################################
	#print "<tr><td bgcolor='#FFFFCC' align=left><font face='verdana, arial, helvetica' size=2 color='#465e90'>";
	#print "</td></tr>";
                       print "<iframe src=\"ng/server.cgi?type=threads&server=$server\" width=100% height=400 frameborder=0 marginheight=0 marginwidth=0></iframe>";
                           ################################################################################################
	                   #
	                   # end content
                           #
	                   print "
                         </td>
                       </tr>
 	             </table>
                   </td>
	         </tr>

               </table>";
	       # block end

	# block
	print "<p>
	       <table border='1' width='100%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>

                 <tr>
		   <th>
                     <div align=left>
                       <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			 <tr>
			   <td bgcolor='#5FA6D7'>
			     <font size=2 face='verdana, arial, helvetica'>
			     <b><font size=2 face='verdana, arial, helvetica' color='#000000'>
                             <HTML>
                             <table width=100% cellpadding=2 border=0 cellspacing=1>
                               <tr>
                                 <td align=left>";
	                           #
	                           # item header
	                           #

				   print "<font size=2 face='verdana, arial,
helvetica'><b>Waited Calls for $server (<a
href=\"ng/server.cgi?type=waited-calls&server=$server&count=1000000\">all</a>)</b>";

	                         print "
                                 </td>
                               </tr>
                             </table>
                           </td>
			 </tr>
                       </table>
                     </div>
                   </th>
		 </tr>

		 <tr>
		   <td>
                     <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
                       <tr bgcolor='#FFFFCC'>
                         <td  align=center>";
                           #
	                   # content
	                   #
                           ################################################################################################
	#print "<tr><td bgcolor='#FFFFCC' align=left><font face='verdana, arial, helvetica' size=2 color='#465e90'>";
	#print "</td></tr>";
                       print "<iframe src=\"ng/server.cgi?type=waited-calls&server=$server\" width=100% height=400 frameborder=0 marginheight=0 marginwidth=0></iframe>";
                           ################################################################################################
	                   #
	                   # end content
                           #
	                   print "
                         </td>
                       </tr>
 	             </table>
                   </td>
	         </tr>

               </table>";
	       # block end
	# block
	print "<p>
	       <table border='1' width='100%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>

                 <tr>
		   <th>
                     <div align=left>
                       <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			 <tr>
			   <td bgcolor='#5FA6D7'>
			     <font size=2 face='verdana, arial, helvetica'>
			     <b><font size=2 face='verdana, arial, helvetica' color='#000000'>
                             <HTML>
                             <table width=100% cellpadding=2 border=0 cellspacing=1>
                               <tr>
                                 <td align=left>";
	                           #
	                           # item header
	                           #

				   print "<font size=2 face='verdana, arial, helvetica'><b>Rxdebug information for $server</b>";

	                         print "
                                 </td>
                               </tr>
                             </table>
                           </td>
			 </tr>
                       </table>
                     </div>
                   </th>
		 </tr>

		 <tr>
		   <td>
                     <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
                       <tr bgcolor='#FFFFCC'>
                         <td  align=center>";
                           #
	                   # content
	                   #
                           ################################################################################################
	print "<tr><td bgcolor='#FFFFCC' align=left><font face='verdana, arial, helvetica' size=2 color='#465e90'>";
	print "<b>Rxdebug Information</b><br>";
	print "Server $server is ";
	my $counter=0;
	foreach(@output){
		print "$_<br>" if($counter>1);
		$counter++;
	}
	print "</td></tr>";
                           ################################################################################################
	                   #
	                   # end content
                           #
	                   print "
                         </td>
                       </tr>
 	             </table>
                   </td>
	         </tr>

               </table>";
	       # block end
	print "</font></div>
			</td>
			</tr>
			</table></div>";

	
	print "</td>
	</tr>
	</table></table></div>";
	return 1;
}

sub table_ext{
	read(STDIN,$encoded,$ENV{'CONTENT_LENGTH'});
	my $server;
	my $ctr = 0;
	foreach ( param() ) {
#	$server = param($_);
		$server = param($_) if ($ctr == 0);
		if ($_ =~ /\d\d\d\d-\d\d-\d\d/) {
			$given_date = $_;
		}
		$ctr++;
	}

	print "<div align=center><table border='2' frame='box' width='95%'>
<tr>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Server</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Part</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Size (GB)</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Used (GB)</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Full (%)</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Comm (GB)</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Volumes</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>64kB Read [ms]</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>64kB Write [ms]</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Total Accesses</b></font>
</th>
<th bgcolor='#5FA6D7'>
	<font size=2 face='verdana, arial, helvetica' color=#465e90>
	<b>Categories</b></font>
</th>
</tr>";
	my $cat;
	open my $FILE , '<', $ConsoleConstants::AFS_STAT; # or die "Could not open file: $!";
	my ($found,$valid,$total,$elem,$count,$tacc)=(0,0,0,0,0,0);
	while(<$FILE>){
		$valid = 1 if($_=~/(.*)AFS Disk space status by server(.*)/);
		$valid = 0 if($_=~/(.*)The most busy volumes per partition are(.*)/);
		if ($valid eq 1){
			if($_=~/(.*)(GB)(.*)/){
				next;
			}
			if ($_ =~ /$server(.*)/){
				($server,$part,$size,$used,$full,$comm,$vols,$accs,$cat) = split " " , $_;
				my ($fillstatus, $size, $used, $volumes, $online,
					$offline, $committed, $readAccessTime, $writeAccessTime) =
						ConsoleDB::partitions_servdata("$server/$part");

				$size = $size / (1024 * 1024);
				$size = sprintf "%.2f", $size;

				$used = $used / (1024 * 1024);
				$used = sprintf "%.2f", $used;

				if ($size > 0) {
				    $full = $used / $size * 100;
				} else {
				    $full = "-1";
				}
				$full = sprintf "%.2f", $full;
				
				if (0 == $readAccessTime) {
					$readAccessTime = "n.a.";
				} else {
					$readAccessTime = $readAccessTime / 1000;
					$readAccessTime = sprintf "%.2f", $readAccessTime;
				}

				if (0 == $writeAccessTime) {
					$writeAccessTime = "n.a.";
				} else {
					$writeAccessTime = $writeAccessTime / 1000;
					$writeAccessTime = sprintf "%.2f", $writeAccessTime;
				}
				
				my ($totalAccesses,$committed) =
					ConsoleDB::volumes_servaccquota("$server/$part");

				if ($totalAccesses !~ /\d+/) {
					$totalAccesses = 0;
				}

				$committed = $committed / (1024 * 1024);
				$committed = sprintf "%.2f", $committed;
				
				$part = "vicep".$part;
				
				print "<tr><td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			$server
			</font></td>
			<td bgcolor='#ffffcc'>";
				$part =~/vicep(\w+)/;
				$value = $server."/"."$1";
				print "<A href=volumes_hottest.cgi?entries=10000&sortedBy=totalaccesses&server=$value&now=mostRecent>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			$part
			</font></A></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			$size
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			$used
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			$full
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			$committed
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			$volumes
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			$readAccessTime
			</font></td>
			<td bgcolor='#ffffcc' align=left>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			$writeAccessTime
			</font></td>
                        <td bgcolor='#ffffcc' align=left>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			$totalAccesses
			</font></td>";
				$total=1;
				$found=1;
				$tacc+=$totalAccesses/1000000;
				$tacc = sprintf "%.2f", $tacc;
				$tused+=$used;
				$tsize+=$size;
				$tvolumes+=$volumes;
				$tcommitted+=$committed;
				print "<td bgcolor='#ffffcc'><font face='verdana, arial, helvetica' size=2 color=#465e90>$cat &nbsp&nbsp ";
			}elsif($_=~/(.*)\((.*)\)(.*)/ && $found eq 1){
				$count=0,print "<br>" if $count eq 2;
				$_=~/(\s+)(.*)/;
				$pool =$2;
				print "$pool &nbsp&nbsp ";
				$count++

			    }elsif($_=~/Total(.*)/ && $total eq 1 && $found eq 1){
				    ($total,$part,$size,$XXused,$comm,$vols,$accs) = split " " , $_;
				    #$part = "vicep"."$part";
				    #my $perc_full = sprintf( "%.2f", ($tused * 100) / ($size * 1024) );
					my $perc_full;
					if ($tsize > 0) {
				    	$perc_full = sprintf( "%.2f", $tused / $tsize * 100);
					} else {
						$perc_full = "Error";
					}
				    print "</font></td></tr>";
				    print "<tr><td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			<b>$total</b>
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			<b>$part</b>
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			<b>$size</b>
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			<b>$tused</b>
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			<b>$perc_full</b>
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			<b>$tcommitted</b>
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			<b>$tvolumes</b>
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			<b>&nbsp;</b>
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			<b>&nbsp;</b>
			</font></td>
			<td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			<b>$tacc Millions</b>
			</font></td>
                        <td bgcolor='#ffffcc'>
			<font face='verdana, arial, helvetica' size=2 color=#465e90>
			<b>&nbsp;</b>
			</font></td>
			</tr>";
				    $found=0;
				    last;
			    }
		} #if starts eq 1
	}
	#close TEMP;
	print "</table>";
	return 1;
	close $FILE;
}





sub foot{
	print "<HR></HR>
<body bgcolor=#ffffff font=arial, helevetica>
<div align=center><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeee><tr><td>
<div align=right><font face='verdana, arial, helvetica' size=2>AFS Service - CERN IT</font></div>
</table></tr></td></div>";
	return 1;
}

#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query=new CGI;
print $query->header();
my $val = heading2();
$val = main_frame();
&footer_simple(0);
print '</body></html>';
