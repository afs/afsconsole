#!/usr/bin/env perl

#------------------------------------------------------------------------------
# volumes_hottest.cgi : get most active volumes
#
# Initial version : Arne Wiebalck CERN IT-FIO/LA, Aug 2007.
#------------------------------------------------------------------------------

use DBI;
use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
require 'ConsoleMySQL.pl';
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";

use ConsoleDB;

#-------------------------------------------------------------------------------
#
# Globals
#
#-------------------------------------------------------------------------------

my $parameter;
my $table = "M_T_905";
my $today = 0;
my $server;
my $partition = '';
my $entries;
my $sortedBy;
my ($start_date, $start_time, $end_date, $end_time);
my %sortedByTexts = (
		     "size"            => "Volume Size",
		     "files"           => "Number of Files",
		     "quota"           => "Quota",
		     "currentaccesses" => "Current Accesses",
		     "currentreads"    => "Current Reads",
		     "currentwrites"   => "Current Writes",
		     "currentreads + currentwrites" => "Sum of Current Reads and Writes",
		     "totalaccesses"   => "Total Accesses",
		     "totalreads"      => "Total Read Accesses",
		     "totalwrites"     => "Total Write Accesses",
		     );

#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

sub convertTimestamp() {

	my $timestamp = shift;

	my ($second, $minute, $hour, $dayOfMonth, 
	    $month, $yearOffset, $dayOfWeek, $dayOfYear, 
	    $daylightSavings) = localtime( $timestamp );

	$year  = $yearOffset + 1900;

	$month = $month + 1;	
	$month = "0$month" if ($month < 10);

	$dayOfMonth = "0$dayOfMonth" if ($dayOfMonth < 10);
	$hour       = "0$hour"       if ($hour < 10);
	$minute     = "0$minute"     if ($minute < 10);
	$second     = "0$second"     if ($second < 10);

	return "$year-$month-$dayOfMonth/$hour:$minute:$second";
}

# FIXME Centralise
sub calc_timestamps() {

	use Time::Local;
	
	my ($start_date, $start_time, $end_date, $end_time) = @_;
	
	# start
	my ($year, $month, $day) = split /-/, $start_date;
	my ($hours, $min, $sec)  = split /:/, $start_time;
	my $times_start = timelocal( $sec, $min, $hours, $day, $month - 1, $year );

	# end
	($year, $month, $day) = split /-/, $end_date;
	($hours, $min, $sec)  = split /:/, $end_time;
	my $times_end = timelocal( $sec, $min, $hours, $day, $month - 1, $year );

	return "$times_start/$times_end";
}

# FIXME Centralise
sub calc_timestampsToday() {

	use Time::Local;
	
	my ($sec, $min, $hrs, $day, $month,
	    $year, $dow, $doy, $dst) = localtime(time);

	# start	
	my $times_start = timelocal( 0, 0, 0, $day, $month, $year );

	# end
	my $times_end = timelocal( 0, 0, 0, $day + 1, $month, $year );

	return "$times_start/$times_end";
}

sub serverNameOK() {
	my $srv = shift;
	my $listsrv = &get_all_servers();
	my @servers = split /\s+/, $listsrv;
	
	return 1 if (grep(/$srv/, @servers));
	return 0;
}

sub hottest {
	
	my ($timestamp, $volumes_volumeid, $volumes_volumename, $volumes_server, 
		$volumes_partition, $volumes_type, $volumes_size, $volumes_files, 
		$volumes_status, $volumes_quota, $volumes_availability, 
		$volumes_currentaccesses, $volumes_currentreads, $volumes_currentwrites,
		$volumes_totalaccesses, $volumes_totalreads, $volumes_totalwrites, 
		$volumes_totalaccesses, $volumes_project);
	
	if (&serverNameOK($server)) {
		$serverText = $server;
	} else {
		$serverText = "Global";
	}

	# time window given
	my $times;
	if ($table eq "M_T_905") {
		if ($today) {
			$times = &calc_timestampsToday();
		} else {
			$times = &calc_timestamps($start_date, $start_time,
									  $end_date, $end_time);
		}

	}
		
	my $srvprt;
	if (defined $server and $server ne '') {
		$srvprt = $server;
		if (defined $partition and $partition ne '') {
			$srvprt .= "/$partition";
		}
	}

	$sortedByText = $sortedByTexts{$sortedBy};
	&startTableTable( "$serverText Hottest Volumes (sorted by $sortedByText)", $Tonly );	
	&headline4Table_VolumesHottest( $srv, $howManyRows );

	$data_counter = 0;
	my @hottest = ConsoleDB::volumes_hottest($srvprt, $times,
											 $sortedBy, $entries);
	foreach (@hottest) {
		$timestamp = &convertTimestamp($_->[0]);
		&row4Table_VolumesHottest($timestamp, $_->[1], $_->[2], $_->[3], 
								  $_->[4], $_->[5], $_->[6], $_->[7], $_->[8], 
								  $_->[9], $_->[10], $_->[11], $_->[12], 
								  $_->[11] + $_->[12], $_->[13], $_->[14], 
								  $_->[15], $_->[16], $sortedBy);
		$data_counter += 1;
	}
	&endTableTable();
	
	if (!$data_counter) {
		
		print "<p align=center><font face='verdana, arial, helvetica' size=2 color=#FF0000>No data available!</font></p>";
	} 

	return 1;
}


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query = new CGI;
print $query->header();

# Parameter evaluation
foreach (param()) {

	# text-only
	if ($_ eq 'T') { 
		
		$Tonly = 1; 
		next;
	}

	# number of lines
	if (/^entries$/) {
		
		$entries = param($_);
		unless (($entries =~ /^\d+$/) && ($entries >= 1) && ($entries <= 100000)) {
		    $entries = 10;
		}
		next;
	}
	
	# sorted by
	if (/^sortedBy$/) {
	       	
		$sortedBy = param($_);

		my $found = 0;
		foreach my $s (@AvolumeFields) {
		    if (lc $s eq $sortedBy) {$found = 1;}
		}
                if ($sortedBy eq 'currentreads + currentwrites') {$found =1;}
		if (!$found) {$sortedBy = 'currentaccesses';}
                next;
	}

	# select the data table
	if (/^now$/) {
		
		my $when = param($_);
        # Initial code
		#if ($when eq "mostRecent") {
		#    $table = $VOLUMES_TABLE_CURR;
		#} elsif ($when eq "today") {
		#    $today = 1;
		#} 

        # Changes I made for some twisted reasons
		#if ($when eq "mostRecent") {
		#    $table = $VOLUMES_TABLE_CURR;
		#} else {
		#    $today = 1;
		#} 

		if ($when eq "mostRecent") {
		    $table = $VOLUMES_TABLE_CURR;
		} elsif ($when eq "today") {
		    $today = 1;
		} 

		next;
	}

	# date & time
	if (/start_date$/) {
		
		$start_date = param($_);
		unless ($start_date =~ /^\d\d\d\d-\d\d-\d\d$/) {
		    print "Input error: Invalid start date $start_date\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^end_date$/) {
		
		$end_date = param($_);
		unless ($start_date =~ /^\d\d\d\d-\d\d-\d\d$/) {
		    print "Input error: Invalid end date $start_date\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^start_time$/) {
		
		$start_time = param($_);
		unless ($start_time =~ /^\d\d:\d\d:\d\d$/) {
		    print "Input error: Invalid start time $start_time\n";
		    &footer_simple( $Tonly );
		    exit 1;
		}
		next;
	}
	if (/^end_time$/) {
		
	    $end_time = param($_);
	    unless ($end_time =~ /^\d\d:\d\d:\d\d$/) {
		print "Input error: Invalid end time $end_time\n";
		&footer_simple( $Tonly );
		exit 1;
	    }
	    next;
	}

	# server name
	if (/^server$/) {
		
		$server = param($_);		
		unless (($server =~ /^afs\d\d\d?$/) || 
			($server =~ /^afs\d\d\d?\/\w\w+$/) || 
			($server =~ /^afsdb\d$/) ||                        
                        ($server =~ /^afsdb\d\/\w\w?$/) || 
			($server =~ /^afs\d\d\d?\/\w$/)) {
		    #$server = "Global";
			$server = undef;
		}
		if ($server =~ /\//) {			
			($server, $partition) = split /\//, $server;
		}
		next;
	}	
}


# determine the number of rows to be displayed on the page
$howManyRows = $display;

# HTML creation
&heading( "Hottest Volumes", $Tonly );
&hottest();

&footer_simple( $Tonly );
