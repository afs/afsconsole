#!/usr/bin/env perl

#------------------------------------------------------------------------------
# partitions.cgi  : entry page for partition informatin
#
# Initial version : Arne Wiebalck CERN IT-FIO/LA, 2007.
#------------------------------------------------------------------------------

use CGI ':standard';
use CGI::Carp qw(fatalsToBrowser);
use DBI;

use FindBin;
use lib "$FindBin::Bin/../modules"; # better than from some random location on AFS

use ConsoleConstants;
require 'ConsoleHTML.pl';
require 'ConsoleUtils.pl';
require "ConsoleAccessTimes.pl";


#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

sub heading() {
	
	&make_imbed_grafx(\$graphs_icons_kfm_home, "icons/kfm_home");
	print "<html>";
	print "<head><title>CERN AFS Console View - Volumes</title>";
	print "</head>";
	print "<body bgcolor=#ffffff font=arial, helevetica>";
	print "<HTML><table width=100% cellpadding=1 border=0 cellspacing=0 bgcolor=#000000><tr><td>";
	print "<HTML><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeef><tr><td>";
	print "<A href='index.cgi'><img border='0' src='$graphs_icons_kfm_home'></A> &nbsp&nbsp&nbsp <font size=3 face='verdana, arial, helvetica'>CERN AFS Console View</font>";
	print "</table></tr></td>";
	print "</table></tr></td>";
	print "<HR></HR>";

	return 1;
}


sub main_frame() {
	
	print " <div align=center>";

	        # all items and footer
	        print "
                <table width=100% border=0>
                   <tr>
	           <td>";
                   
                      # item: hottest partitions
	              print "
		      <table border='1' width='98%' cellspacing ='0' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>
			 <tr>
			       <th><div align=left>
                               <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			          <tr>
			          <td bgcolor='#5FA6D7'>
			             <font size=2 face='verdana, arial, helvetica'>
			             <b><font size=2 face='verdana, arial, helvetica' color='#000000'>";

                                     # item header
	                             print "
                                     <HTML>
                                     <table width=100% cellpadding=2 border=0 cellspacing=1>
                                        <tr>
                                        <td>
                                            <font size=2 face='verdana, arial, helvetica'><b>Top partitions</b></font> 
                                         </td>
                                         </tr>
                                     </table>
                                     </font></b>
			          </td>
			          </tr>
			          </div>
                               </table>
                               </th>
		         </tr>

		         <tr>
		         <td>
                            <div align=left>
                            <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
			       <tr bgcolor='#FFFFCC'>
                               <td width='2%'><font face='verdana, arial, helvetica' size=2 color='#FFFFCC'>invisible</font></td>
                               <td bgcolor='#FFFFCC' width='60%'>
			          <div align=left>
			          <font face='verdana, arial, helvetica' size=3 color='#465e90'>";
	
	                          # item action
 	                          my $action = 'partitions_hottest.cgi';
  	                          my $currDate = &today();
	                          my $currTime = &now();
	                          print " 
                                  <form method='GET' action=$action name='entries' target='_parent'>
   	 	             
                                  Get the <input type='text' name='entries' value='20' maxlength=4 size=4> top partitions in terms of 

                                  <select name=\"sortedBy\" size=\"1\" >";
#				  <option value=\"partitions_accesses\">Total Number of Accesses</option>
#				  <option value=\"partitions_currentaccesses\">Current Accesses</option>
#				  <option value=\"partitions_currentreads\">Current Reads</option>
#				  <option value=\"partitions_currentwrites\">Current Writes</option>
	                          
	                          print "
                                  <option value=\"fillstatus\"selected=\"selected\">Fill Status</option>
				  <option value=\"size\">Size</option>
				  <option value=\"volumes\">Number of Volumes</option>
				  <option value=\"readaccesstime\">Slowest Read Access</option>
				  <option value=\"writeaccesstime\">Slowest Write Access</option>

			          </select>
                                  
                                  <font color='#465e90'>&nbsp(sorted by time</font>
                                                <form method='GET' action=$action name='now' target='_parent'>
                                                <input type='checkbox' name='time'>)
                               </td>
                               <td>
                                 
                               <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>

                                 <form method='GET' action=$action name='now' target='_parent'>

                                 <td align=left width='40%'>

                                 <input type='radio' name='now' value='mostRecent' checked='checked'>
                                 <font color='#465e90'>Most recent</font><br>

                                 <input type='radio' name='now' value='today'>
                                 <font color='#465e90'>Today</font><br>

                                 <input type='radio' name='now' value='interval'>
                                 <font color='#465e90'>Interval</font></td>

                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                               </tr>

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>From:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='start_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='start_time' value='00:00:00' maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
                               </tr> 

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>To:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='end_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='end_time' value=$currTime maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
  		               </tr>
 
                               <tr bgcolor='#FFFFCC'>
                               <div align=center>
                                 <td></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td align=center><input type='submit' value='Submit'></td>
                               </tr>
                                  
                                  </form><p>
                                  </div>
 
	                      </tr>
	                    </table>

                               </td>
                               </tr>

                             </table>
                         </td>
	                 </tr>
                      </table>";
                      # end item hottest partitions                          

	print "<br>";

                      # item: partitions's info
	              print "
		      <table border='1' width='98%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>
			 <tr>
			       <th><div align=left>
                               <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			          <tr>
			          <td bgcolor='#5FA6D7'>
			             <font size=2 face='verdana, arial, helvetica'>
			             <b><font size=2 face='verdana, arial, helvetica' color='#000000'>";

                                     # item header
	                             print "
                                     <HTML>
                                     <table width=100% cellpadding=2 border=0 cellspacing=1>
                                        <tr>
                                        <td>
                                            <font size=2 face='verdana, arial, helvetica'><b>Partition's info</b></font> 
                                         </td>
                                         </tr>
                                     </table>
                                     </font></b>
			          </td>
			          </tr>
			          </div>
                               </table>
                               </th>
		         </tr>

		         <tr>
		         <td>
                            <div align=left>
                            <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
			       <tr bgcolor='#FFFFCC'>
                               <td width='2%'><font face='verdana, arial, helvetica' size=2 color='#FFFFCC'>invisible</font></td>
                               <td bgcolor='#FFFFCC' width='60%'>
			          <div align=left>
			          <font face='verdana, arial, helvetica' size=3 color='#465e90'>";
	
	                          # item action
 	                          my $action = 'partitions_info.cgi';
  	                          my $currDate = &today();
	                          my $currTime = &now();
	                          print " 
                                  <form method='GET' action=$action name='info' target='_parent'>
   	 	             
                                  Get info about partition";
                                  # get the current list of servers and partitions
                             	  @prod_servers_S = split /\s+/, &get_all_servers();
	                          push( @prod_servers_S, split /\s+/, &get_all_partitions());	                        
                               	  @prod_servers = sort @prod_servers_S;

                                  print "    	                          
                                  for server/partition
                                  <select name=\"partition\" size=\"1\" >
                                  <option value=\"all\">all</option>";
                                  foreach (@prod_servers) {
					  
					  # exclude afsdbX
					  unless (/^afsdb/) {
						  print "<option value=\"$_\">$_</option>"; 
					  }
				  }
	                          print "</select>

                                  (sorted by
                                  <select name=\"orderBy\" size=\"1\" >

						  \"<option value=\"time\">time</option>\"
						  \"<option value=\"server\">server, partition</option>\"
						  \"<option value=\"partition\">partition</option>";

	                          print "</select>)

                               </td>
                               <td>
                                 
                               <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>

                                 <form method='GET' action=$action name='now' target='_parent'>

                                 <td align=left width='40%'>

                                 <input type='radio' name='now' value='mostRecent' checked='checked'>
                                 <font color='#465e90'>Most recent</font><br>

                                 <input type='radio' name='now' value='today'>
                                 <font color='#465e90'>Today</font><br>

                                 <input type='radio' name='now' value='interval'>
                                 <font color='#465e90'>Interval</font></td>

                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                               </tr>

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>From:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='start_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='start_time' value='00:00:00' maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
                               </tr> 

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>To:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='end_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='end_time' value=$currTime maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
  		               </tr>
 
                               <tr bgcolor='#FFFFCC'>
                               <div align=center>
                                 <td></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td align=center><input type='submit' value='Submit'></td>
                               </tr>
                                  
                                  </form><p>
                                  </div>
 
	                      </tr>
	                    </table>

                               </td>
                               </tr>

                             </table>
                         </td>
	                 </tr>
                      </table>";
                      # end item partition's info    

	print "<br>";

                      # item: plot data
	              print "
		      <table border='1' width='98%' cellspacing ='1' cellpadding='0' align=center BORDERCOLOR=#5FA6D7>
			 <tr>
			       <th><div align=left>
                               <table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			          <tr>
			          <td bgcolor='#5FA6D7'>
			             <font size=2 face='verdana, arial, helvetica'>
			             <b><font size=2 face='verdana, arial, helvetica' color='#000000'>";

                                     # item header
	                             print "
                                     <HTML>
                                     <table width=100% cellpadding=2 border=0 cellspacing=1>
                                        <tr>
                                        <td>
                                            <font size=2 face='verdana, arial, helvetica'><b>Plot partition's data</b></font> 
                                         </td>
                                         </tr>
                                     </table>
                                     </font></b>
			          </td>
			          </tr>
			          </div>
                               </table>
                               </th>
		         </tr>

		         <tr>
		         <td>
                            <div align=left>
                            <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
			       <tr bgcolor='#FFFFCC'>
                               <td width='2%'><font face='verdana, arial, helvetica' size=2 color='#FFFFCC'>invisible</font></td>";
	
	                          # item action
 	                          my $action = 'partitions_plot.cgi';
  	                          my $currDate = &today();
	                          my $currTime = &now();
	                          print " 
                                  
                              <form method='GET' action=$action name='plot' target='_parent'>

                              <td align='left' width='15%'>

			          <div align=left>

                                                <form method='GET' action=$action name='files' target='_parent'>
                                                <input type='checkbox' name='fillStatus'>
                                                <font color='#465e90'>Fill Status<br>

                                                <form method='GET' action=$action name='quota' target='_parent'>
                                                <input type='checkbox' name='numberOfVolumes'>
                                                <font color='#465e90'>Number of Volumes<br>

                                                <form method='GET' action=$action name='size' target='_parent'>
                                                <input type='checkbox' name='size'>
                                                <font color='#465e90'>Size
                               </td>
  
                               <td align='left' width='15%'>

                                                <form method='GET' action=$action name='current_accesses' target='_parent'>
                                                <input type='checkbox' name='readAccessTimes' checked value='on'>
                                                <font color='#465e90'>Read Access Times<br>

                                                <form method='GET' action=$action name='current_reads' target='_parent'>
                                                <input type='checkbox' name='writeAccessTimes' checked value='on'>
                                                <font color='#465e90'>Write Access Times

                               </td>

                               <td align='left' width='5%'>

                               </td>                                
                               <td width='25%'>

                                  <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>
                                  <tr bgcolor='#FFFFCC'>
                                    <td align=right>
                                      <font color='#465e90'>for partition</font>
                                    </td>
                                    <td>";                                  	                        
                                      @prod_servers_S = ();
	                              push( @prod_servers_S, split /\s+/, &get_all_partitions());
	                              @prod_servers = sort @prod_servers_S;

                                      print "    	                          
                                      <select name=\"partition\" size=\"1\" >";
                                      foreach (@prod_servers) {
					  
					      # exclude afsdbX
					      unless (/^afsdb/) {
						      print "<option value=\"$_\">$_</option>"; 
					      }
				      }
	                              print "</select>
                                    </td>
                                  </tr>
                                  <tr bgcolor='#FFFFCC'>
                                    <td align=right>  
                                      <font color='#465e90'>cut off (&mu;s)</font> 
                                    </td>
                                    <td>
                                      <input type='text' name='cutoff' value='' maxlength=7 size=7>
                                    </td> 
                                  </tr>  
                                  </table>

                               </td>
                               <td>
                                 
                               <table width='100%' cellspacing='0' cellpadding='1' border='0' bgcolor='#165480'>

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>

                                 <form method='GET' action=$action name='now' target='_parent'>

                                 <td align=left width='40%'>

                                 <input type='radio' name='now' value='today' checked='checked'>
                                 <font color='#465e90'>Today</font><br>

                                 <input type='radio' name='now' value='interval'>
                                 <font color='#465e90'>Interval&nbsp;&nbsp;</font></td>

                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                               </tr>

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>From:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='start_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='start_time' value='00:00:00' maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
                               </tr> 

                               <tr bgcolor='#FFFFCC'>
                                 <td></td>
                                 <td align=right><font color='#465e90'>To:</font>
                                                <form method='GET' action=$action name='info' target='_parent'></td>
                                 <td align=center><input type='text' name='end_date' value=$currDate maxlength=10 size=10></td>
                                 <td align=center><input type='text' name='end_time' value=$currTime maxlength=8 size=8></td>
                                 <td bgcolor='#FFFFCC'></td>
  		               </tr>
                                
                               <tr bgcolor='#FFFFCC'>
                               <div align=center>
                                 <td></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td bgcolor='#FFFFCC'></td>
                                 <td align=center><input type='submit' value='Submit'></td>
                               </tr>
                                  
                                  </form><p>
                                  </div>
 
	                      </tr>
	                    </table>

                               </td>
                               </tr>

                             </table>
                         </td>
	                 </tr>
                      </table>";
                      # plot data

                print "
                </table>
                </div>";
	return 1;
}


sub foot() {

	print "<HR></HR><body bgcolor=#ffffff font=arial, helevetica><div align=center><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeee><tr><td><div align=right><font face='verdana, arial, helvetica' size=2>AFS Service - CERN IT</font></div></table></tr></td></div>";
	return 1;
}

#-------------------------------------------------------------------------------
#
# Main
#
#-------------------------------------------------------------------------------

my $query=new CGI;
print $query->header();
&heading( "Partition History", $Tonly );
$val = main_frame();
&footer_simple(0);

