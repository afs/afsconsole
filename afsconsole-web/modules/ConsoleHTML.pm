#!/usr/bin/perl

################################################################################
#
#  Script      : ConsoleHTML.pl
#                Functions to build the console's web pages
#
#  Change log  :
#
#                05.11.2007 awi  : drop stale data entries
#
#  Original version by Arne Wiebalck, CERN IT-FIO/LA, 2007 
################################################################################

use URI;
use ConsoleConstants;

sub make_imbed_grafx {
    my ($ref, $fn) = @_;
    return if defined($$ref);
    $$ref = URI->new("data:");
    $$ref->media_type("image/png");
    my $path = ($fn =~ m,^/tmp/afs-console,) ? $fn : $ConsoleConstants::GRAPHS."/$fn.png";
    open(X, $path); sysread(X, $_, 256*1024); close(X);
    $$ref->data($_);
}

make_imbed_grafx(\$graphs_icons_xmag, "icons/xmag");

#-------------------------------------------------------------------------------
#
# General page layout
#
#-------------------------------------------------------------------------------

# @name   : heading
# @brief  : creates the header line of a console web page
# @params : $title  the title to be displayed
#           $Tonly  flag for text-only output
# @returns: always 1
#
sub heading {

	my ($title, $Tonly) = @_;
	
	print "<html>";

	unless ($Tonly) {

		make_imbed_grafx(\$graphs_icons_kfm_home, "icons/kfm_home");
		$title = $ConsoleConstants::CONSOLE_NAME . " - " . $title;
		print "<head><title>$title</title>";
		print "</head>";
		print "<body bgcolor=#ffffff font=arial, helevetica>";
		print "<HTML><table width=100% cellpadding=1 border=0 cellspacing=0 bgcolor=#000000><tr><td>";
		print "<HTML><table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeef><tr><td>";
		print "<A href='index.cgi'><img border='0' src='$graphs_icons_kfm_home'></A> &nbsp&nbsp&nbsp ";
		print "<font size=3 face='verdana, arial, helvetica'><A name='init'>" . $ConsoleConstants::CONSOLE_NAME . "</A></font>";
		print "</table></tr></td>";
		print "</table></tr></td>";
		print "<HR></HR>";
	}

	return 1;
}


# @name   : footer
# @brief  : creates the footer line of a console web page
# @params : $movedCounter no of volumes not displayed due to a recent move
#           $lastDBUpdate the timestamp of the last DB update
#           $Tonly        flag for text-only output
# @returns: always 1
#
sub footer {
	
	my ($movedCounter, $lastDBUpdate, $Tonly) = @_;

	unless( $Tonly ) {
		
		print "<HR></HR><body bgcolor=#ffffff font=arial, helevetica><div align=center>";
		print "<table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeee>";
		print "<tr><td><div align=right><font face='verdana, arial, helvetica' size=2>";

			print "Not displayed (today moved/stale data): $movedCounter --- Last DB Update: ";

		# older than 1 hour
		#
		if ((time - $lastDBUpdate) < 3600) {
			print "".localtime($lastDBUpdate)."";
		} else {
			print "<font color=\"#FF0000\"><b><blink>".localtime($lastDBUpdate)."</blink></b></font>";
		}
		
		use Time::Local;
		
		my ($sec, $min, $hrs, $day, $month,
		    $year, $dow, $doy, $dst) = localtime(time);
		
		$year = $year + 1900;
		$month = $month + 1;
		if ($month =~ /^\d$/) {$month = "0$month";};
		if ($day =~ /^\d$/) {$day = "0$day";};
		if ($hrs =~ /^\d$/) {$hrs = "0$hrs";};
		if ($min =~ /^\d$/) {$min = "0$min";};
		if ($sec =~ /^\d$/) {$sec = "0$sec";};

		print " --- AFS Service - CERN IT - Generated $year/$month/$day \@$hrs:$min:$sec</font></div></table></tr></td></div>" ;
	}
	
	return 1;
}

# @name   : footer_simple
# @brief  : creates the footer line of a console web page
# @returns: always 1
#
sub footer_simple {
	
	my ($Tonly) = @_;

	unless( $Tonly ) {
		
		use Time::Local;
		
		my ($sec, $min, $hrs, $day, $month,
		    $year, $dow, $doy, $dst) = localtime(time);
		
		$year = $year + 1900;
		$month = $month + 1;
		if ($month =~ /^\d$/) {$month = "0$month";};
		if ($day =~ /^\d$/) {$day = "0$day";};
		if ($hrs =~ /^\d$/) {$hrs = "0$hrs";};
		if ($min =~ /^\d$/) {$min = "0$min";};
		if ($sec =~ /^\d$/) {$sec = "0$sec";};

		print "<HR></HR><body bgcolor=#ffffff font=arial, helevetica><div align=center>";
		print "<table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeee>";
		print "<tr><td><div align=right><font face='verdana, arial, helvetica' size=2>";
		print "<a href=\"http://afs.web.cern.ch/afs/\">AFS Service</a> - CERN IT - Generated $year/$month/$day \@$hrs:$min:$sec</font></div></table></tr></td></div>" ;
	}
	
	return 1;
}

sub footer_simple2 {
	
	my ($msg) = @_;

	print "<HR></HR><body bgcolor=#ffffff font=arial, helevetica><div align=center>";
	print "<table width=100% cellpadding=2 border=0 cellspacing=1 bgcolor=#eeeeee>";
	print "<tr><td><div align=right><font face='verdana, arial, helvetica' size=2>";
	print "AFS Service--$msg-- </font></div></table></tr></td></div>" ;
	
	return 1;
}

#-------------------------------------------------------------------------------
#
# General Tables
#
#-------------------------------------------------------------------------------

sub startTableTable() {

	my ($title, $Tonly) = @_;
	
	print "<div align=center><table width='100%'>
		<tr><td>
		<table border='0' width='100%' cellspacing ='1' cellpadding='3' border ='0'>
		<tr>
		<th><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		<tr>
			<td bgcolor='#5FA6D7'>
			<img src='$graphs_icons_xmag'>
			<font size=2 face='verdana, arial, helvetica'>
			<b><font size=2 face='verdana, arial, helvetica' color='#000000'>$title</font></b>
			</font>
			</td>
			</tr>
			</div></table></th>
			</tr>
			<tr>
			<td><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
			<tr><td bgcolor='#FFFFCC'>
			<div align=left>
			<font color=#465e90 size=2 face='verdana, arial, helvetica'>" unless $Tonly;
	return 1;
}

sub startTableTable2() {

	my ($title, $Tonly) = @_;
	
	print "<div align=center><table width='100%'>
		<tr><td>
		<table border='0' width='100%' cellspacing ='1' cellpadding='3' border ='0'>
		<tr>
		<th><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#165480'>
		<tr>
			<td bgcolor='#5FA6D7'>
			<img src='$graphs_icons_xmag'>
			<font size=2 face='verdana, arial, helvetica'>
			<b><font size=2 face='verdana, arial, helvetica' color='#000000'>$title</font></b>
			</font>
			</td>
			</tr>
			</div></table></th>
			</tr>
			<tr>
			<td><div align=left><table width='100%' cellspacing='1' cellpadding='2' border='0' bgcolor='#FFFFCC'>" unless $Tonly;
	return 1;
}

sub endTableTable() {
	
	print "</table></div>";
	print "</font></div></font>
			</td></tr></table></div>
			</td></tr>";
	print "</table></table></div>";
	return 1;
}

#-------------------------------------------------------------------------------
#
# Volume Table
#
#-------------------------------------------------------------------------------

# @name   : headline4volumeTable
# @brief  : creates the header line of a table containing volume information
# @params : $what        what to access
#           $howManyRows the numberof rows to be displayed
#           $Tonly       flag for text-only output
# @returns: always 1
#
sub headline4volumeTable {

	my ($what, $howManyRows, $Tonly, $script) = @_;
		print "<div align=center><table border='2' width='75%'>
               <tr>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Volume Id</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Volume Name</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Status</b>
                   </font>
               </th>" unless $Tonly;

	my $param = join "-", $what, "volsize";
	$param = $param . "&entries=$howManyRows";
	print "<th bgcolor='#5FA6D7'>
	           <A href=$script?=$param>
                       <font size=2 face='verdana, arial, helvetica' color=#465e90>
                           <b>Size</b>
                       </font>
                   </A>
               </th>" unless $Tonly;

	my $param = join "-", $what, "files";
	$param = $param . "&entries=$howManyRows";
	print "<th bgcolor='#5FA6D7'>
	           <A href=$script?=$param>
                       <font size=2 face='verdana, arial, helvetica' color=#465e90>
	                   <b>Files</b>
                       </font>
                   </A>
               </th>" unless $Tonly;

	my $param = join "-", $what, "accesses";
	$param = $param . "&entries=$howManyRows";
	print "<th bgcolor='#5FA6D7'>
	           <A href=$script?=$param>
                       <font size=2 face='verdana, arial, helvetica' color=#465e90>
	                   <b>Accesses</b>
                  </A><br><font size=1>(since midnight)</font></font>
               </th>

               <th bgcolor='#5FA6D7'> 
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
	               <b>Accs/sec</b><br>
                   <font size=1>(since midnight)</font></font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
	               <b>Accs/sec</b><br>
                       <font size=1>(since DB Update)</font>
                   </font>
               </th>" unless $Tonly;

	my $param = join "-",$what,"read_ops";
	$param = $param . "&entries=$howManyRows";
        print " <th bgcolor='#5FA6D7'>
	            <A href=$script?=$param>
                        <font size=2 face='verdana, arial, helvetica' color=#465e90>
	                     <b>Reads</b>
                    </A><br><font size=1>(since midnight)</font></font>
                </th>

                <th bgcolor='#5FA6D7'>
	            <font size=2 face='verdana, arial, helvetica' color=#465e90>
	                <b>Reads/sec</b><br>
                    <font size=1>(since midnight)</font></font>
                </th>

                <th bgcolor='#5FA6D7'>
     	            <font size=2 face='verdana, arial, helvetica' color=#465e90>
	                <b>Reads/sec</b><br>
                    <font size=1>(since DB Update)</font></font>
                </th>" unless $Tonly;

	my $param = join "-",$what,"write_ops";
	$param = $param . "&entries=$howManyRows";
        print "<th bgcolor='#5FA6D7'>
	           <A href=$script?=$param>
                   <font size=2 face='verdana, arial, helvetica' color=#465e90>
 	               <b>Writes</b></A><br>
                   <font size=1>(since midnight)</font></font>
              </th>

              <th bgcolor='#5FA6D7'>
	          <font size=2 face='verdana, arial, helvetica' color=#465e90>
	              <b>Writes/sec</b><br>
                  <font size=1>(since midnight)</font></font>
              </th>

              <th bgcolor='#5FA6D7'>
	          <font size=2 face='verdana, arial, helvetica' color=#465e90>
	              <b>Writes/sec</b><br>
                  <font size=1>(since DB Update)</font></font>
              </th>

              <th bgcolor='#5FA6D7'>
	          <font size=2 face='verdana, arial, helvetica' color=#465e90>
 	              <b>Project</b>
                  </font>
              </th>

              <th bgcolor='#5FA6D7'>
	          <font size=2 face='verdana, arial, helvetica' color=#465e90>
	              <b>Partition</b>
                  </font>
              </th>

              <th bgcolor='#5FA6D7'>
	          <font size=2 face='verdana, arial, helvetica' color=#465e90>
	              <b>Server</b>
                  </font>
              </th>
        </tr>" unless $Tonly;
	
	return 1;
}

# @name   : row4volumeTable
# @brief  : creates a row for a table containing volume information
# @params : yep, a lot ...
# @returns: 0 if the line is printed
#           1 if the line is dropped
#
sub row4volumeTable {

	my ($id_vol, $vol_name, $status, $volsize, $files,
	    $accesses, $old_accesses, $project, $location,
	    $id_srv, $read_ops, $write_ops, $time_upd, 
	    $old_time_upd, $Tonly, $sortedBy) = @_;

	# avoid negative rates due to stale data
	if (0 == $accesses) {
		return 1;
	}

	# get the seconds since midnight for the avg
	my $now       = time;
	my $secs      = $now % 86400; 
	my $avg       = sprintf "%.1f" , $accesses/$secs;
	my $avg_read  = sprintf "%.1f" , $read_ops/$secs;
	my $avg_write = sprintf "%.1f" , $write_ops/$secs;

	# get the current data and compare with DB data
	my $vos = "/usr/sbin/vos";	
	my @vos_output = `$vos exa $id_vol -extended -noauth`;
	my ($total_reads,$total_writes,$current_accesses);
	foreach (@vos_output) {
		
		if (/accesses/) {

			my @elems = split /\s+/, $_;
			$current_accesses = $elems[1];
			next;
			
		} elsif (/Reads/) {
			
			my @elems = split /\s+/,$_;
			$total_reads = $elems[2];
			next;
			
		} elsif (/Writes/) {

			my @elems = split /\s+/,$_;
			$total_writes = $elems[2];
			last; # required if the pattern shall stay simple ...
		}
	}

	my $delta = ($now - $time_upd);

	# the current access rate
	my $ins = ($current_accesses - $accesses)/$delta;
	$ins = sprintf "%.1f" , $ins;
	$ins = "n.a." if ($ins<0);

	my $readRate  = sprintf "%.0f", ($total_reads - $read_ops)/$delta;
	my $writeRate = sprintf "%.0f", ($total_writes - $write_ops)/$delta;


	$total_reads  = $read_ops; 
	$total_writes = $write_ops; 

	print "<tr> <td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $id_vol
               </font></td>\n" unless $Tonly;
	print "<td bgcolor='#ffffcc'>
               <A href=vos_data.cgi?=$vol_name>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>" unless $Tonly;
	print $vol_name;
	print "</font></A></td>
               <td bgcolor='#ffffcc'>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $status
               </font></td>" unless $Tonly; if ($Tonly) {$status=~s/-line//; print " $status"; }

	if ($sortedBy eq "volsize") {
		$volsize = '<b>'.$volsize.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $volsize
               </font></td>" unless $Tonly; print " B $volsize" if $Tonly;
	if ($sortedBy eq "files") {
		$files = '<b>'.$files.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $files
               </font></td>" unless $Tonly;
	if ($sortedBy eq "accesses") {
		$accesses = '<b>'.$accesses.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $accesses
               </font></td>" unless $Tonly; print " A $accesses" if $Tonly;
	print "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $avg
               </font></td>
               <td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $ins
               </font></td>" unless $Tonly; print " $avg $ins" if $Tonly;
	if ($sortedBy eq "read_ops") {
		$total_reads = '<b>'.$total_reads.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $total_reads
               </font></td>

               <td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $avg_read
               </font></td>

               <td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $readRate
               </font></td>";

	if ($sortedBy eq "write_ops") {
		$total_writes = '<b>'.$total_writes.'</b>';
	}
	print  "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $total_writes
               </font></td>

               <td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $avg_write
               </font></td>

               <td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $writeRate
               </font></td>" unless $Tonly;

 	print "<td bgcolor='#ffffcc' align=right>
               <A href=proj_detail.cgi?$project>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $project
               </font></A></td>
               <td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $location
               </font></td>
               <td bgcolor='#ffffcc'>
               <A href=servers_data.cgi?=$id_srv align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $id_srv
               </font></A></td>
               </tr>" unless $Tonly; print "<br>" if $Tonly;

	return 0;
}

#-------------------------------------------------------------------------------
#
# Volume Table: Hottest Volumes
#
#-------------------------------------------------------------------------------

# @name   : headline4Table_VolumesHottest
# @brief  : creates the header line of a table containing hottest volume information
# @params : $howManyRows the numberof rows to be displayed
#           $Tonly       flag for text-only output
# @returns: always 1
#
sub headline4Table_VolumesHottest {

	my ($what, $howManyRows, $Tonly, $script) = @_;
		print "<div align=center><table border='2' width='98%'>
               <tr>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Timestamp</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Volume Id</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Volume Name</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
                   <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Info</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
                   <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Graph</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Server</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Partition</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Type</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Size [kByte]</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Files</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Status</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Quota [kByte]</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Current Accesses [1/s]</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Current Reads [1/s]</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Current Writes [1/s]</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Current Reads+Writes [1/s]</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Total Accesses</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Total Reads</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Total Writes</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Project</b>
                   </font>
               </th>

              </tr>" unless $Tonly;
	
	return 1;
}

# @name   : row4Table_VolumesHottest
# @brief  : creates a row for a table containing hottest volume information
# @params : yep, a lot ...
# @returns: always 1
#
sub row4Table_VolumesHottest {

	my ($timestamp, $volumes_volumeid, $volumes_volumename, $volumes_server,
	    $volumes_partition, $volumes_type, $volumes_size, $volumes_files,
	    $volumes_status, $volumes_quota, $volumes_currentaccesses,
	    $volumes_currentreads, $volumes_currentwrites,  $volumes_currentreadswrites, 
	    $volumes_totalaccesses, $volumes_totalreads, $volumes_totalwrites, $volumes_project, $sortedBy) =@_;

	print "<tr>\n";

	print "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $timestamp
               </font></td>\n";

	print "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $volumes_volumeid
               </font></td>\n";
        
        print "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $volumes_volumename
               </font></td>\n";

	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
               <A href=volumes_info.cgi?volumeName=$volumes_volumename>
	       Info 
               </A>
	       </font></td>\n";

        print "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               <A href=volumes_plot.cgi?volumeName=$volumes_volumename&files=on&quota=on&size=on&current_all=on&total_all=on&now=today>
               Graph 
               </A>
               </font></td>\n";

	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_server
	       </font></td>\n";

	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_partition
	       </font></td>";

	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_type
	       </font></td>";

	if ($sortedBy eq "size") {
		$volumes_size = '<b>'.$volumes_size.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_size
	       </font></td>";

	if ($sortedBy eq "files") {
		$volumes_files = '<b>'.$volumes_files.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_files
	       </font></td>";

	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_status
	       </font></td>";

	if ($sortedBy eq "quota") {
		$volumes_quota = '<b>'.$volumes_quota.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_quota
	       </font></td>";

	if ($sortedBy eq "currentaccesses") {
		$volumes_currentaccesses = '<b>'.$volumes_currentaccesses.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_currentaccesses
	       </font></td>";

	if ($sortedBy eq "currentreads") {
		$volumes_currentreads = '<b>'.$volumes_currentreads.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_currentreads
	       </font></td>";

	if ($sortedBy eq "currentwrites") {
		$volumes_currentwrites = '<b>'.$volumes_currentwrites.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $volumes_currentwrites	       
	       </font></td>";

	if ($sortedBy eq "currentreads + currentwrites") {
		$volumes_currentreadswrites = '<b>'.$volumes_currentreadswrites.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $volumes_currentreadswrites	       
	       </font></td>";

	if ($sortedBy eq "totalaccesses") {
		$volumes_totalaccesses = '<b>'.$volumes_totalaccesses.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_totalaccesses
	       </font></td>";

	if ($sortedBy eq "totalreads") {
		$volumes_totalreads = '<b>'.$volumes_totalreads.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_totalreads
	       </font></td>";

	if ($sortedBy eq "totalwrites") {
		$volumes_totalwrites = '<b>'.$volumes_totalwrites.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_totalwrites
	       </font></td>";

	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $volumes_project
	       </font></td>";

	
	print "</tr>";

	return 1;
}

#-------------------------------------------------------------------------------
#
# Partition Table: Hottest Partitions
#
#-------------------------------------------------------------------------------

# @name   : headline4Table_PartitionsHottest
# @brief  : creates the header line of a table containing hottest partition information
# @params : $howManyRows the numberof rows to be displayed
#           $Tonly       flag for text-only output
# @returns: always 1
#
sub headline4Table_PartitionsHottest {

	my ($what, $howManyRows, $Tonly, $script) = @_;
		print "<div align=center><table border='2' width='98%'>
               <tr>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Timestamp</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Server</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Partition</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Fill Status [%]</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Size [kByte]</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Used [kByte]</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Volumes</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Online</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Offline</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Committed [kByte]</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Read Access Time</b>
                   </font>
               </th>

               <th bgcolor='#5FA6D7'>
	           <font size=2 face='verdana, arial, helvetica' color=#465e90>
                       <b>Write Access Time</b>
                   </font>
               </th>

              </tr>" unless $Tonly;
	
	return 1;
}

# @name   : row4Table_PartitionsHottest
# @brief  : creates a row for a table containing hottest volume information
# @params : yep, a lot ...
# @returns: always 1
#
sub row4Table_PartitionsHottest {

	my ($timestamp, $partitions_server, $partitions_partition, $partitions_fillstatus,
	    $partitions_size, $partitions_used, $partitions_volumes, $partitions_online,
	    $partitions_offline, $partitions_committed, $partitions_readaccesstime, 
	    $partitions_writeaccesstime, $sortedBy) =@_;

	print "<tr>\n";

	print "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $timestamp
               </font></td>\n";

	print "<td bgcolor='#ffffcc' align=right>
               <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $partitions_server
               </font></td>\n";
	
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $partitions_partition
	       </font></td>\n";

	if ($sortedBy eq "fillstatus") {
		$partitions_fillstatus = '<b>'.$partitions_fillstatus.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $partitions_fillstatus
	       </font></td>\n";

	if ($sortedBy eq "size") {
		$partitions_size = '<b>'.$partitions_size.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $partitions_size
	       </font></td>";

	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $partitions_used
	       </font></td>";

	if ($sortedBy eq "volumes") {
		$partitions_volumes = '<b>'.$partitions_volumes.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $partitions_volumes
	       </font></td>";

	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $partitions_online
	       </font></td>";

	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $partitions_offline
	       </font></td>";

	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $partitions_committed
	       </font></td>";

	if ($sortedBy eq "readaccesstime") {
		$partitions_readaccesstime = '<b>'.$partitions_readaccesstime.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
	       $partitions_readaccesstime
	       </font></td>";

	if ($sortedBy eq "writeaccesstime") {
		$partitions_writeaccesstime = '<b>'.$partitions_writeaccesstime.'</b>';
	}
	print "<td bgcolor='#ffffcc' align=right>
	       <font face='verdana, arial, helvetica' size=2 color=#465e90>
               $partitions_writeaccesstime
	       </font></td>";
	
	print "</tr>";

	return 1;
}


sub insertTableData() {	

	$first = 1;
	$moved_counter = 0;
	while ($query->fetch()) {

		# add a row to the table, if the volume still resides on
		# this server
		if (&checkVolumeStillHere( $id_vol, $id_srv, $location )) {				
			
			&row4volumeTable( $id_vol, $vol_name, $status, $volsize, $files,
					  $accesses, $old_accesses, $project, $location,
					  $id_srv, $read_ops, $write_ops, $time_upd, $old_time_upd,
					  $Tonly );
		} else {

			$moved_counter++;
		}

		# get the time of the last DB update from the first entry,
		# not totally accurate ...
		if ($first) {
			
			$lastDBUpdate = localtime( $time_upd );
			$first = 0;
		}
	}
}

1;
