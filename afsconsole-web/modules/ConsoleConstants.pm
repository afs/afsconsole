#!/usr/bin/perl

package ConsoleConstants;
#-------------------------------------------------------------------------------
#
# AFS Console
#
#-------------------------------------------------------------------------------

use constant CONSOLE_PATH => "/afs/cern.ch/project/afs/dev/monitoring/console/";
use constant CONSOLE_NAME => "CERN AFS Console";

$CONSOLE_PATH         = CONSOLE_PATH;
$CONSOLE_NAME         = CONSOLE_NAME;
$MAX_SERVER_RESP_TIME = 50; # in ms


#-------------------------------------------------------------------------------
#
# Config files
#
#-------------------------------------------------------------------------------

# shared between web and afs-console
$GRAPHS = $CONSOLE_PATH . '/graphs';
$PROJECTS_FILE = $CONSOLE_PATH . '/data_files/projects';
$CONSOLE_FILE = $CONSOLE_PATH . '/data_files/console';
$VOLSLOGS_DIR = $CONSOLE_PATH . '/data_files/volslogs';
$OTHERLOGS_DIR = $CONSOLE_PATH . '/data_files/otherlogs';
$DAYALARM_PREFIX = $CONSOLE_PATH . '/data_files/otherlogs/dayalarm_log';

# possibly wider sharing
$STATISTICS = '/afs/cern.ch/project/afs/var/console/statistics';
$ACCESS_TIMES = '/afs/cern.ch/project/afs/var/console/accessTimes';
$AFS_STAT = '/afs/cern.ch/project/afs/var/stats/AFS.stat';

# shared just between the afsconsole-scripts and gnuplot:
$DATA_FILES = $CONSOLE_PATH . '/data_files';


1;