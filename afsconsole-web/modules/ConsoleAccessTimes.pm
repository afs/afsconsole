#!/usr/bin/perl

################################################################################
#
#  Script      : modules/accessTimes.pl
#
#                Group all functions relevant for retrieving the 
#                access times
#
#  Original version by A. Wiebalck, IT/FIO, 2007
################################################################################

#-------------------------------------------------------------------------------
#
# Globals
#
#-------------------------------------------------------------------------------
use ConsoleConstants;
my $accessTimes_dataPath = $ConsoleConstants::ACCESS_TIMES;
use ConsoleUtils;

#-------------------------------------------------------------------------------
#
# Subroutines
#
#-------------------------------------------------------------------------------

# @name   : average
# @brief  : calc the geometric average of an array
# @params : an array with the data samples
# @returns: geometric average of an array
#
sub average() {

	my @a = @_;
	my $sum = 0;
	my $ctr = 0;
	
	foreach my $i (@a) {
		
		$sum += $i;
		$ctr++;
	}
	
	return ($sum/$ctr) if (0 != $ctr);

	return 0;
}

# @name   : get_accessTimes
# @brief  : get the average access time during the last hour (usec)
# @params : servername, partition letter(s), seconds
# @returns: the geometric average of the access times in the last 
#           N seconds (last hour by default)
#
sub get_accessTimes() {

	my ($srv, $prt, $secs) = @_;
	my @accessTimes;
	
	# set the time threshold 
	$secs = 3600 if ($secs == '');
	my $threshold = time - $secs;	

	# get all data file, newer than the threshold
	my $fname = $ConsoleConstants::ACCESS_TIMES .'/'. $srv . "." . &today . '.dat';
	if( open DF4711, "$fname") {

		while (<DF4711>) {
			my @tmp = split /\s+/, $_;
			
			if ($tmp[0] >= $threshold && $tmp[2] eq $prt) {
				push @accessTimes, $tmp[5];
			}
		}
		close DF4711;
	}
	
	return &average( @accessTimes );
}

1;
