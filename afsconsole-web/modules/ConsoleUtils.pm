#!/usr/bin/perl -w
#
use strict;
use ConsoleConstants;

use lib '/usr/share/afsconsole/common';
use AFSConsoleConf;

sub get_all_partitions {

	return `$AFSConsoleConf::get_partitions_command`;
}

sub get_all_servers {

	return `$AFSConsoleConf::get_servers_command`;
}

sub get_slow_servers {

	# get the servers with local response time > $MAX_SERVER_RESP_TIME
	#
	# returns: an array of the slow servers

	my @ASlowServers = ();
	my $file = $ConsoleConstants::DATA_FILES . '/servers_switch';
	open(DATA, '<', $file) or die "cannot open $file:$!";
        while (<DATA>) {
		
                my @data = split /\s+/, $_;
                my $accessTime = $data[2] + $data[3] + $data[4] + $data[5] + $data[6];
                my $srv = $data[1];
                push @ASlowServers, "$srv/$accessTime" if ($accessTime > $ConsoleConstants::MAX_SERVER_RESP_TIME);
        }
	close(DATA);

	return @ASlowServers;
}

sub checkVolumeStillHere {

        # check if the volume has been moved recently; the DB cleanup
        # only occurs once a day (OBSOLETE?)
        #
        # params : $id    the volume ID
        #          $srv   the server name
        #          $part  the partition
	#
        # returns: 1 if the volume info is valid
        #          0 if the volume is not in the expected place

	my ($id, $srv, $part) = @_;

	my $vos = "/usr/sbin/vos";
	my @vos_output = `$vos exa $id`;

	foreach (@vos_output) {

		if (/$srv.cern.ch/ && /$part/) {
			
			return 1;
			last;
		}
	}
	return 0;
}

sub today {

	my ($sec, $min, $hrs, $day, $mon,
	    $yr, $dow, $doy, $dst) = localtime(time);
	
	if ($day<10) {
		$day = "0" . $day;
	}

	$mon++;
	if ($mon<10) {
		$mon = "0" . $mon;
	}
	
	$yr += 1900;

	return "$yr-$mon-$day";
}


sub yesterday {

	my $y_time = time - 86400;

	my ($sec, $min, $hrs, $day, $mon,
	    $yr, $dow, $doy, $dst) = localtime( $y_time );
	
	if ($day<10) {
		$day = "0" . $day;
	}

	$mon++;
	if ($mon<10) {
		$mon = "0" . $mon;
	}
	
	$yr += 1900;

	return "$yr-$mon-$day";
}


sub now {

	my ($second, $minute, $hour, $dayOfMonth, $month, $yearOffset, $dayOfWeek, $dayOfYear, $daylightSavings) = localtime();
	
	$second = '0'.$second if ($second<10);
	$minute = '0'.$minute if ($minute<10);
	$hour   = '0'.$hour if ($hour<10);

	return "$hour:$minute:$second";
}

sub today_now {

	# return date in time in "YYYY/MM/DD @ HH:MM:SS" 
	
	my ($sec, $min, $hrs, $day, $mon,
	    $yr, $dow, $doy, $dst) = localtime(time);
	
	$day = "0" . $day if ($day<10);

	$mon++;
	$mon = "0" . $mon if ($mon<10);
	      
	$yr += 1900;

	$sec = '0' . $sec if ($sec<10);
	$min = '0' . $min if ($min<10);
	$hrs = '0' . $hrs if ($hrs<10);
	
	return "$yr/$mon/$day @ $hrs:$min:$sec";
}


sub convertTimestamp {

	my $timestamp = shift;

	my ($second, $minute, $hour, $dayOfMonth, 
	    $month, $yearOffset, $dayOfWeek, $dayOfYear, 
	    $daylightSavings) = localtime( $timestamp );

	my $year  = $yearOffset + 1900;

	$month = $month + 1;	
	$month = "0$month" if ($month < 10);

	$dayOfMonth = "0$dayOfMonth" if ($dayOfMonth < 10);
	$hour       = "0$hour"       if ($hour < 10);
	$minute     = "0$minute"     if ($minute < 10);
	$second     = "0$second"     if ($second < 10);

	return "$year-$month-$dayOfMonth/$hour:$minute:$second";
}

our %sortedByTexts = (
		  "size"            => "Volume Size",
		  "files"           => "Number of Files",
		  "quota"           => "Quota",
		  "currentaccesses" => "Current Accesses",
		  "currentreads"    => "Current Reads",
		  "currentwrites"   => "Current Writes",
		  "currentreads + currentwrites" => "Sum of Current Reads and Writes",
		  "totalaccesses"   => "Total Accesses",
		  "totalreads"      => "Total Read Accesses",
		  "totalwrites"     => "Total Write Accesses",
		  );


1;
