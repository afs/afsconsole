#!/usr/bin/perl

######################################################################
#
#  Script      : modules/log_it.pl
#
#                Logging subroutines shared by the scripts
#
#  Original version by A. Wiebalck, IT/FIO, 2006
######################################################################

# switch logging on (1) or off (0)
use constant LOGGING => 1;

# write a log message to stdout 
sub log_it {
	
	if( LOGGING ) {
		
		my ($msg, $lvl) = @_;
		if(! $lvl ) {		
			$lvl = 1;
		}
		my $caller = (caller($lvl))[3];
		$caller = '(undef)' unless $caller; # e.g in debugger
		print localtime( time ) . " $msg in $0  $caller \n";
	}
}

sub log_start {

	log_it( "Started", 2 );
}

sub log_finish {

	log_it( "Finished", 2 );
}

1;
