#!/usr/bin/perl
use lib '/usr/share/afsconsole/common';
use AFSConsoleConf;

sub db_connect {
    my $backend = $AFSConsoleConf::db_backend;
    my $dbname = $AFSConsoleConf::db_name;
    my $host = $AFSConsoleConf::db_host;
    my $username = $AFSConsoleConf::db_ro_username;
    my $passwd = $AFSConsoleConf::db_ro_passwd;

    my $dbh = DBI->connect("DBI:$backend:database=$dbname;host=$host", $username, $passwd)
        or die "Couldn't connect to data source on $to";
    return $dbh;
}

sub db_disconnect {
    my $_dbh = shift;

    if ($_dbh) {
        $_dbh->disconnect();
    }
}

sub db_lastupdate {
    my $_dbh = shift;
    my $_table = shift;

    if ($_table ne 'volumes' and $_table ne 'partitions') {
        return;
    }

    if (not $_dbh) {
        return;
    }

    my @arow = $_dbh->selectrow_array("SELECT MAX(timestamp) FROM $_table")
        or return;

    return "@arow";
}

sub db_busycount {
    my $_dbh = shift;

    if (not $_dbh) {
        return;
    }

    my $query = <<EOF;
SELECT COUNT(nodename) FROM volumes
WHERE recent = 1
AND currentAccesses > 2500
EOF
    my @arow = $_dbh->selectrow_array($query)
        or return;

    return "@arow";

}

# the db fields
@AvolumeFields = qw( NODENAME TIMESTAMP VOLUMEID VOLUMENAME SERVER PARTITION
 TYPE SIZE FILES STATUS QUOTA AVAILABILITY CURRENTACCESSES CURRENTREADS
 CURRENTWRITES TOTALACCESSES TOTALREADS TOTALWRITES PROJECT QUALITY DEPTH FLAG
 TOTALREMOTEREADS TOTALREMOTEWRITES );

@ApartitionFields = qw( NODENAME TIMESTAMP SERVER PARTITION FILLSTATUS SIZE 
 USED VOLUMES ONLINE OFFLINE COMMITTED READACCESSTIME WRITEACCESSTIME );

1;
