Name: afsconsole-web
Version: 1.01
Release: 1
Summary: AFS Console scripts
Group: Filesystems
License: GPL
BuildArch: noarch
Requires: afsconsole-common

%description
Installs all AFS Console related web scripts.

%files
/usr/share/afsconsole/web/cgi-scripts/volumes.cgi
/usr/share/afsconsole/web/cgi-scripts/volumes_hottest.cgi
/usr/share/afsconsole/web/cgi-scripts/volumes_info.cgi
/usr/share/afsconsole/web/cgi-scripts/volumes_plot.cgi
/usr/share/afsconsole/web/cgi-scripts/partitions.cgi
/usr/share/afsconsole/web/cgi-scripts/partitions_hottest.cgi
/usr/share/afsconsole/web/cgi-scripts/partitions_info.cgi
/usr/share/afsconsole/web/cgi-scripts/partitions_plot.cgi


%post

%postun
