Name: afsconsole-common
Version: 1.02
Release: 1
Summary: AFS Console Common files
Group: Filesystems
License: GPL
BuildArch: noarch
Vendor: CERN
Packager: Arne Wiebalck <arne.wiebalck@cern.ch>
Provides: perl(AFSConsoleConf)
Provides: perl(AFSConsoleConfWrite)

%description
Installs AFS Console common files.

%files
/usr/share/afsconsole/common/AFSConsoleConf.pm.example
/usr/share/afsconsole/common/AFSConsoleConfWrite.pm.example
%ghost /usr/share/afsconsole/common/AFSConsoleConf.pm
%ghost /usr/share/afsconsole/common/AFSConsoleConfWrite.pm
/usr/share/afsconsole/common/afsconsole-common.readme

%post

%postun
