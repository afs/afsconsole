Name: afsconsole-accesstimes
Version: 2.4
Release: 0%{?dist}
Summary: AFS Console accessTime probe

Group: CERN/Utilities
License: CERN
URL: https://gitlab.cern.ch/afs/afsconsole/-/tree/master/accessTimes
Source: %{name}-%{version}.tgz

Requires: openafs-client

BuildRequires: golang
# C8 (and above?) have don't have %%systemd_post etc defined without this
%{?!el7:BuildRequires: systemd-rpm-macros}

%define debug_package %{nil}


%description
The accessTimes probe queries latency of CERN AFS servers.
Results go to a (legacy) file on AFS. This tool is CERN-specific.

%prep
%setup -q -n %{name}-%{version}


%build
make -C go


%install
mkdir -p %{buildroot}%{_bindir} %{buildroot}%{_unitdir}
install -m 0755 go/accessTimes %{buildroot}%{_bindir}/
install -m 0644 go/accessTimes.service %{buildroot}%{_unitdir}/


%files
%defattr(-,root,root,-)
%{_bindir}/accessTimes
%{_unitdir}/accessTimes.service
%doc README.md

%post
%systemd_post accessTimes.service

%preun
%systemd_preun  accessTimes.service

%postun
%systemd_postun_with_restart accessTimes.service

%changelog
* Fri Oct 28 2022 Jan Iven <jan.iven@cern.ch> - 2.4-0
- run optional command for 'too-slow' servers
- loop until we get a list of partitions for a server

* Mon Feb 28 2022 Jan Iven <jan.iven@cern.ch> - 2.3-0
- add 5sec timout for Carbon operations

* Wed Oct 27 2021 Jan Iven <jan.iven@cern.ch> - 2.2-0
- fix FD leak on legacy result file

* Thu Oct 21 2021 Jan Iven <jan.iven@cern.ch> - 2.0-0
- ship the 'go' version of accessTimesw, incl systemd unit file

* Thu Oct 21 2021 Jan Iven <jan.iven@cern.ch> - 1.0-0
- build as RPM
