package simpleMovingAverage

import (
	"testing"
	"time"
	"math"
)

func tooDifferent(a, b time.Duration) bool {
	return math.Abs(float64(a - b)) > 1e-6
}

func TestSMA(t *testing.T) {
	sma := simpleMovingAverage.New(5)
	want := time.Duration(0.0 * float64(time.Millisecond))
	got := sma(0 * time.Millisecond)
	if tooDifferent(got, want) {
		t.Log("0: want", want, "got", got )
		t.Fail()
	}
	sma(1 * time.Millisecond)
	sma(2 * time.Millisecond)
	want = time.Duration(float64(3 + 2 + 1 + 0 + 0)/5.0 * float64(time.Millisecond))
	got = sma(3 * time.Millisecond)
	if tooDifferent(got, want) {
		t.Log("3: want", want, "got", got )
		t.Fail()
	}
	sma(4 * time.Millisecond)
	sma(5 * time.Millisecond)
	want = time.Duration(float64(6 + 5 + 4 + 3 + 2)/5.0 * float64(time.Millisecond))
	got = sma(6 * time.Millisecond)
	if tooDifferent(got, want) {
		t.Log("6: want", want, "got", got )
		t.Fail()
	}

	sma1  := simpleMovingAverage(1)
	got = sma1(123 * time.Millisecond)
	want = time.Duration(float64( 123.0 )/1.0 * float64(time.Millisecond))
	if tooDifferent(got, want) {
		t.Log("sma1: want", want, "got", got )
		t.Fail()
	}
	got = sma1(456 * time.Millisecond)
	want = time.Duration(float64( 456.0 )/1.0 * float64(time.Millisecond))
	if tooDifferent(got, want) {
		t.Log("sma1: want", want, "got", got )
		t.Fail()
	}

	sma2  := simpleMovingAverage(2)
	sma2(100 * time.Millisecond)
	got = sma2(100 * time.Millisecond)
	want = time.Duration(float64( 100.0 + 100.0 )/2.0 * float64(time.Millisecond))
	if tooDifferent(got, want) {
		t.Log("sma2: want", want, "got", got )
		t.Fail()
	}
}
