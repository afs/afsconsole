package simpleMovingAverage

import (
	"time"
	"fmt"
)

type smaBucket struct {
	vals []int64
	pointer int
	prev float64
}

func printSmaBucket(sma *smaBucket) {
	fmt.Printf("sma=%p  sma.vals=%p sma.pointer=%d sma.prev=%f\n", sma, sma.vals, sma.pointer, sma.prev)
	for i, val := range sma.vals {
		fmt.Printf("%d: %d\n", i, val)
	}
}

func New(buckets int) func(next time.Duration) time.Duration {
	sma := new(smaBucket)
	sma.vals = make([]int64, buckets)
	sma.pointer = 0
	sma.prev = 0.0
	return func(next time.Duration) time.Duration {
		nexti := next.Milliseconds()
		// printSmaBucket(sma)
		oldestval := sma.vals[sma.pointer]
		sma.vals[sma.pointer] = nexti
		sma.pointer = ((sma.pointer + 1) % buckets)
		sma.prev += float64( nexti - oldestval )/float64(buckets)
		ret := time.Duration(sma.prev * float64(time.Millisecond))
		return ret
	}
}

