package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"sync"
	"time"
	"gitlab.cern.ch/afs/afsconsole/accessTimes/simpleMovingAverage"
)

type timingData struct {
	StartTime time.Time
	Server    string
	Partition string
	Operation string
	Latency   time.Duration
	RwBytes   int
}

const (
	sleepTimeSec   = 60  // run each test at what interval?
	sleepJitter    = 10  // randomize, just a bit
	warnTimeSec    = 300 // log warnings if no partitions result after this
	carbonProtocol = "udp"
	carbonTimeout  = 5
	testSize       = 64 // size of test file in kB
	maxPartitions  = 16 // just used for sizing the Go channel
	syncFlag       = 1  // we run f.Sync() after f.Write()
	afsflushFlag   = 1  // we run 'fs flush' on the testfile before Read()
)

var (
	testpathPrefix   = "/afs/cern.ch/project/afs/s"                       // per-partition test files by default go here
	testfile         = "accessTime"                                       // add '.remote', if we run the probe on some monitoring host
	resultpathPrefix = "/afs/cern.ch/project/afs/var/console/accessTimes" // legacy '.dat' files by default go here
	carbonPrefix     = "afs.accesstimes"                             // .{local,remote}.<server>.<partition>.{read,write}.latency_s
	slowthreshold time.Duration    // which latency is 'slow'
	slowcommand = ""               // command to run if 'slow'
	slowwait = time.Duration(5 * time.Minute)  // minimum time between 'slow' command runs
	lastslow = time.Now()           // last time 'slow' command was run
)

func randomSleep(sleepSec int, jitterSec int) {
	s := 0
	if jitterSec > 0 {
		s = sleepSec - jitterSec/2 + rand.Intn(jitterSec)
	} else {
		s = rand.Intn(sleepSec)
	}
	time.Sleep(time.Duration(s) * time.Second)
}

func getServers() ([]string, error) {
	var Servers []string
	// vos listaddrs
	out, err := exec.Command("vos", "listaddrs", "-noauth").Output()
	if err != nil {
		return Servers, fmt.Errorf("Error: 'vos listaddrs' failed: %v", err)
	}
	for _, thisServer := range strings.Fields(string(out)) {
		shorthost := strings.SplitN(thisServer, ".", 2)
		Servers = append(Servers, shorthost[0])
	}
	return Servers, nil
}
func getPartitions(Server string) ([]string, error) {
	var Partitions []string
	// vos listpart -server SERVER -noauth
	_ = `The partitions on the server are:
    /vicepa     /vicepb     /viceps 
Total: 3
`
	out, err := exec.Command("vos", "listpart", "-server", Server, "-noauth").Output()
	if err != nil {
		return Partitions, fmt.Errorf("Error: 'vos listpart %s' failed: %v", Server, err)
	}
	partPat := regexp.MustCompile(`/vicep([a-z]+)`)
	for _, p := range partPat.FindAllSubmatch(out, -1) {
		Partitions = append(Partitions, string(p[1]))
	}
	return Partitions, nil
}


func createOrOpenLegacylog(prefix string, Server string) *os.File {
	today := time.Now().Format("2006-01-02")
	resultpath := prefix + "/" + Server + "." + today + ".dat"
	err := os.MkdirAll(prefix, 0755) // no error if existing
	if err != nil {
		log.Fatalln(err)
	}
	needHeader := false
	_, err = os.Stat(resultpath)
	if os.IsNotExist(err) { // potential race condition but not important
		needHeader = true
	}
	f, err := os.OpenFile(resultpath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatalln(err)
	}
	if needHeader {
		if _, err := fmt.Fprintf(f, "%-10s %-7s%3s\t%s\t%12s\t%s\t\t%s\n#\n",
			"#Timestamp", "Srv", "Prt", "Op", "     bytes", "  mu secs", "      Options"); err != nil {
			log.Fatalln(err)
		}
	}
	return f
}

func writeLegacylog(f *os.File, t timingData) {
	if _, err := fmt.Fprintf(f, "%d %6s%3s\t%s\t%12d\t%8d\t\tsync=%d,afsflush=%d,stride=%d\n",
		t.StartTime.Unix(),
		t.Server,
		t.Partition,
		t.Operation,
		t.RwBytes,
		uint(t.Latency.Microseconds()),
		syncFlag,
		afsflushFlag,
		testSize,
	); err != nil {
		log.Fatalln(err)
	}
}

func sendCarbon(t timingData, carbonPrefix string, carbonServers []string) {
	plaintext := []byte(fmt.Sprintf("%s.%s.%s.%s.latency_s %f %d\n",
		carbonPrefix,
		t.Server,
		t.Partition,
		t.Operation,
		t.Latency.Seconds(),
		t.StartTime.Unix()))

	for _, carbonHostPort := range carbonServers {
		conn, err := net.DialTimeout(carbonProtocol,
			carbonHostPort,
			carbonTimeout * time.Second)
		if err != nil {
			log.Println("Error connecting to", carbonHostPort, err)
			continue
		}
		defer conn.Close()
		conn.SetWriteDeadline(time.Now().Add(carbonTimeout * time.Second))
		_, err = conn.Write(plaintext)
		if err != nil {
			log.Println("Error sending to", carbonHostPort, err)
			continue
		}
	}
}

func runSlow(server string, partition string, latency time.Duration, debug bool){
	lat :=  fmt.Sprintf("%d", latency.Milliseconds())
	if time.Now().Sub(lastslow) > slowwait {
		log.Println("Warn: high latency", latency, "for", server, partition, "- running", slowcommand)
		cmd := exec.Command(slowcommand, server, partition, lat)
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		cmd.Stdin = nil
		lastslow = time.Now()
		cmd.Run()
	} else {
		if debug {
			log.Println("Debug: ignoring repeat high latency", latency, "for", server, partition)
		}
	}
}

func readTest(testpath string, rwBuf []byte) (int, error) {
	rTotal := 0
	rCur := 0

	if afsflushFlag != 0 {
		err := exec.Command("fs", "flush", testpath).Run()
		if err != nil {
			return rTotal, err
		}
	}

	f, err := os.OpenFile(testpath, os.O_RDONLY, 0644)
	if err != nil {
		return rTotal, err
	}
	defer f.Close()
	for rTotal < len(rwBuf) {
		rCur, err = f.Read(rwBuf) // Go Read() may return less than requested
		if err != nil {
			if err != io.EOF {
				return rTotal, err
			}
			break
		}
		if rCur <= 0 {
			break
		}
		rTotal += rCur
	}
	if rTotal != len(rwBuf) {
		return rTotal, fmt.Errorf("Error during read (got rw=%d bytes instead of %d) on %s", rTotal, len(rwBuf), testpath)
	}
	return rTotal, nil
}

func writeTest(testpath string, buf []byte) (int, error) {
	f, err := os.OpenFile(testpath, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)
	if err != nil {
		log.Fatalln(err)
	}
	defer f.Close()

	written, err := f.Write([]byte(buf)) // Go Write() returns an error on short writes - no need for a loop.
	if err != nil {
		return written, fmt.Errorf("Error during write (pos rw=%d) on %s: %v", written, testpath, err)
	}
	if syncFlag != 0 {
		err = f.Sync()
		if err != nil {
			return written, fmt.Errorf("Error during fsync() on %s: %v", testpath, err)
		}
	}
	err = f.Close()
	if err != nil {
                return written, fmt.Errorf("Error from close() on %s: %v", testpath, err)
        }
	return written, nil
}

func testOnePartition(timing chan<- timingData, server string, partition string, partsWg *sync.WaitGroup) {
	defer (*partsWg).Done()
	log.Println("testing server", server, "partition", partition)
	timeRead := timingData{Server: server, Partition: partition, Operation: "read"}
	timeWrite := timingData{Server: server, Partition: partition, Operation: "write"}
	var buf = make([]byte, 1024*testSize)

	testpath := testpathPrefix + "/" + server + "." + partition
	err := os.MkdirAll(testpath, 0755) // no error if existing
	if err != nil {
		log.Fatalln(err)
	}

	testpath = testpath + "/" + testfile
	randomSleep(sleepTimeSec, 0) // prevent stampeding herd at startup

	for {
		timeRead.StartTime = time.Now()
		timeRead.RwBytes, err = readTest(testpath, buf) // can fail silently+fast if file is not present
		if err == nil {
			timeRead.Latency = time.Since(timeRead.StartTime)
			timing <- timeRead
		} else {
			log.Println(err)
		}

		timeWrite.StartTime = time.Now()
		timeWrite.RwBytes, err = writeTest(testpath, buf)
		if err == nil {
			timeWrite.Latency = time.Since(timeWrite.StartTime)
			timing <- timeWrite
		} else {
			log.Println(err)
		}

		randomSleep(sleepTimeSec, sleepJitter)
	}
}

func testOneServer(server string, debug bool, legacylog bool, carbonServers []string, carbonprefix string, serversWg *sync.WaitGroup) {
	defer (*serversWg).Done()
	log.Println("Starting test for server", server)
	var partsWg sync.WaitGroup
	var Partitions []string
	var err error

	timing := make(chan timingData, maxPartitions*2) // expect at most one read+one write outstanding result, per partition
	sendcarbon := (len(carbonServers) > 0)
	var t timingData

	for {
		Partitions, err = getPartitions(server)
		if err != nil || len(Partitions) <= 0 {
			log.Println("Warn: cannot get partitions for server", server, "- waiting")
			time.Sleep(time.Duration(sleepTimeSec) * time.Second)
		} else {
			break
		}
	}
	for _, part := range Partitions {
		partsWg.Add(1)
		go testOnePartition(timing, server, part, &partsWg) // start the actual tests
	}

	// unloaded VM servers have 5ms read latency (write has less), but with occasional 225ms spikes (on read).
	// average these out to be normally below 50ms..
	sma := simpleMovingAverage.New(5 * len(Partitions))

	// now wait for test timings to arrive, and record them
	for {
		select {
		case t = <-timing:
			{
				if legacylog {
					f := createOrOpenLegacylog(resultpathPrefix, server)
					writeLegacylog(f, t)
					err = f.Close()
					if err != nil {
						log.Println("Warn: error on closing legacy results in ",resultpathPrefix,"for server", server, ":", err)
					}
				}
				if sendcarbon {
					sendCarbon(t, carbonprefix, carbonServers)
				}
				serverAvgLatency := sma(t.Latency)
				if len(slowcommand) > 0 && serverAvgLatency >= slowthreshold {
					go runSlow(server, t.Partition, serverAvgLatency, debug)
				}
				if debug {
					log.Println("Debug:",server, t.Partition, t.Operation, "latency=",t.Latency, "server_avg=",serverAvgLatency)
				}
			}
		case <-time.After(warnTimeSec * time.Second):
			{
				fmt.Println("Warn: still no partition test results received for", server)
			}
		}
	}
}

func main() {
	var serversWg sync.WaitGroup
	var Servers []string
	var carbonServers []string
	var err error

	var debug = flag.Bool("debug", false, "print all latency measurements. Default: don't")
	var probeAll = flag.Bool("all", false, "probe all known servers (remote). Default: just this host (local)")
	var legacylog = flag.Bool("legacylog", false, "write legacy .dat files into "+resultpathPrefix+". Default: don't")
	var resultpathArg = flag.String("resultpath", resultpathPrefix, "location of the per-server+part legacy result files")
	var carbonServerlist = flag.String("carbonservers", "", "send data via "+carbonProtocol+" to SERVER1:PORT1[,SERVER2:PORT2 ..]")
	var carbonPrefixArg = flag.String("carbonprefix", carbonPrefix, "where data gets stored in Carbon")
	var testpathArg = flag.String("testpath", testpathPrefix, "location of the per-server+part volume mounts")
	var slowthresholdarg = flag.Float64("slowthreshold", 0.0, "latency threshold (in msecs)")
	var slowcommandarg = flag.String("slowcommand", "", "executable to run when latency is above threshold\nArguments provided: SERVER PARTITION LATENCY_msec")

	flag.Parse()

	if *probeAll {
		Servers, err = getServers()
		if err != nil {
			log.Fatalf("Error - cannot get AFS servers:%v", err)
		}
		carbonPrefix = carbonPrefix + ".remote"
		testfile = testfile + ".remote"
	} else {
		thishost, err := os.Hostname()
		if err != nil {
			log.Fatal(err)
		}
		shorthost := strings.SplitN(thishost, ".", 2)
		Servers = append(Servers, shorthost[0])
		carbonPrefix = carbonPrefix + ".local"
	}
	if len(Servers) <= 0 {
		log.Fatalln("Error - no AFS servers found")
	} else {
		log.Printf("Will test %d AFS servers\n", len(Servers))
	}

	for _, cs := range strings.Split(*carbonServerlist, ",") {
		if strings.Contains(cs, ":") {
			carbonServers = append(carbonServers, cs)
		}
	}
	testpathPrefix = *testpathArg
	resultpathPrefix = *resultpathArg
	carbonPrefix = *carbonPrefixArg

	slowthreshold = time.Duration(*slowthresholdarg * float64(time.Millisecond))
	slowcommand = *slowcommandarg
	if slowcommand != "" {
		log.Println("Will run", slowcommand, "if avg latency is over",slowthreshold)
	}
	for _, s := range Servers {
		serversWg.Add(1)
		go testOneServer(s, *debug, *legacylog, carbonServers, carbonPrefix, &serversWg)
	}
	serversWg.Wait() // should block forever, as long as we run tests
	log.Fatalln("Error - all server tests have finished?")
}
