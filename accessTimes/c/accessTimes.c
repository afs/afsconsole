/*
** accessTimes.c
**
**    Usage
**          accessTimes <server> <partition letter> [<partition letter>*]
** 
** Access time measurement on test volumes created by createTestVolumes.pl
**
** Initial version by Arne Wiebalck IT/FIO, 2007.
*/

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/time.h>

/* rw size in kBytes                      */
#define SIZE   64

/* stride size in kBytes                  */
#define STRIDE (SIZE >> 0)

#define SYNC     
#define AFSFLUSH
#define TOFILE   

#ifdef notdef
#define DEBUG    
#define TOSTDOUT
#define LOGFILE
#endif

static const char usage[]         = "Usage: accessTimes <host> <partition letter> [<partition letter>*]";
static const char tfile[]         = "accessTime";
static const char lfile_name[]    = "/tmp/accessTimes/accessTimes.log";
static const char tpath_prefix[]  = "/afs/cern.ch/project/afs/s";
static const char rpath_prefix[]  = "/afs/cern.ch/project/afs/var/console/accessTimes"; 

int main( int argc, char *argv[] )
{
        int          i, fd, err = 0, sync_f = 0, afsflush_f = 0; 
        char         tpath[128];
        char        *rw_buf;
        FILE        *lfile;
#ifdef TOFILE
        struct stat  stat_buf;
        int          fo = 0;
        char         cur_date[16];
        time_t       cur_time;
        FILE        *rfile;
        char         rpath[128];
#endif
        /* no partitions given              */
        if( 2 > argc ) {
                
                printf( "%s\n", usage );
                return 1;
        }
#ifdef LOGFILE
        lfile = fopen( lfile_name, "a" );
        if( !lfile ) {

                fprintf( stderr, "Unable to open logfile file.\n" );                
                return 1;
        }
#endif
#ifdef TOFILE
        /* assemble the result path         */
        cur_time = time( NULL );
        strftime( cur_date, 16, "%Y-%m-%d", localtime( &cur_time ) );

        sprintf( rpath, "%s/%s.%s.dat", rpath_prefix, argv[1], cur_date );
#ifdef DEBUG
#ifdef LOGFILE
        fprintf( lfile, "Result path is %s\n", rpath );
#else
        printf( "Result path is %s\n", rpath );
#endif
#endif
        if( (-1 == stat( rpath, &stat_buf )) && (ENOENT == errno) ) {

                fo = 1;
        }                
        rfile = fopen(rpath, "a");
                
        if (!rfile) {
#ifdef LOGFILE                
                fprintf( lfile, "Unable to open result file. Exiting.\n" );                
#else
                fprintf( stderr, "Unable to open result file. Exiting.\n" );
#endif
                /* return 1; */
                /* do not return an error here: if the server storing the
                   results is not working, we should not create access 
                   alarms for the server we're currently looking at */
        }        

        /* first open?                      */
        if (fo && rfile) {
                fprintf( rfile, "%-10s %-7s%3s\t%s\t%12s\t%s\t\t%s\n#\n", 
                         "#Timestamp", "Srv", "Prt", "Op", "     bytes", "  mu secs", "      Options" ); 
        }
#endif
        /* prepare the rw buffer            */
        rw_buf = (char *) malloc( 1024 * STRIDE * sizeof( char ) );
        if( !rw_buf ) {
#ifdef LOGFILE
                fprintf( lfile, "Unable to alloc rw buffer.\n" );
#else                
                fprintf( stderr, "Unable to alloc rw buffer.\n" );
#endif
                return 1;
        }

        /* loop over the partitions         */
        for( i=2; i<argc; i++ ) {
                
                struct timeval s, e;
                unsigned long  t;
                ssize_t        rw, cur_rw;
#ifdef DEBUG
#ifdef LOGFILE
                fprintf( lfile, "Testing partition %s.\n", argv[i] );
#else
                printf( "Testing partition %s.\n", argv[i] );
#endif                
#endif         
                /* assemble the test path   */
                sprintf( tpath, "%s/%s.%s/%s", tpath_prefix, argv[1], argv[i], tfile );
#ifdef DEBUG
#ifdef LOGFILE
                fprintf( lfile, "Test path is %s\n", tpath );
#else
                printf( "Test path is %s\n", tpath );
#endif
#endif
#ifdef AFSFLUSH
                /* flush the buffer         */
                {
                        char cmd[128];
                        sprintf( cmd, "fs flush -path %s", tpath );
#ifdef DEBUG
#ifdef LOGFILE
                        fprintf( lfile, "%s\n", cmd );
#else
                        printf( "%s\n", cmd );
#endif
#endif
                        system( cmd );
                        afsflush_f = 1;
                }
#endif
#ifdef SYNC
                sync_f = 1;
#endif

                /* 
                ** read from the file      
                */
                fd = open( tpath, O_RDONLY, S_IRWXU );
                if( -1 == fd ) {
#ifdef LOGFILE
                        fprintf( lfile, "Unable to open test file %s: %s\n", 
                                 tpath, strerror( errno ) );
#else                        
                        fprintf( stderr, "Unable to open test file %s: %s\n", 
                                 tpath, strerror( errno ) );
#endif                        
                } else {

                        rw = 0;
                        gettimeofday( &s, NULL );
                        while( rw<SIZE*1024 ) {
                                cur_rw = read( fd, rw_buf, STRIDE*1024 );
                                if( cur_rw <= 0 )
                                        break;
                                rw += cur_rw;
                        }
                        gettimeofday( &e, NULL );
                        if( rw != SIZE*1024 ) {
#ifdef LOGFILE                        
                                fprintf( lfile, "Error during read (rw=%d)\n", (int)rw );
#else
                                fprintf( stderr, "Error during read (rw=%d)\n", (int)rw );
#endif
                        }                
                        t = (e.tv_sec-s.tv_sec)*1000000+(e.tv_usec-s.tv_usec);
#ifdef TOSTDOUT
                        printf( "%d %6s%3s\tread\t%12d\t%8lu\t\tsync=%d,afsflush=%d,stride=%d\n", 
                                (int)time( NULL ), argv[1], argv[i], (int)rw, t, sync_f, afsflush_f, STRIDE);
#endif
#ifdef TOFILE           
                        if (rfile) {
                                fprintf( rfile, "%d %6s%3s\tread\t%12d\t%8lu\t\tsync=%d,afsflush=%d,stride=%d\n", 
                                         (int)time( NULL ), argv[1], argv[i], (int)rw, t, sync_f, afsflush_f, STRIDE);
                        }
#endif 
                        close( fd );
                }
                /* end read */


                /* 
                **  write to the file        
                */
                fd = open( tpath, O_WRONLY|O_CREAT, S_IRWXU );
                if( -1 == fd ) {
#ifdef LOGFILE
                        fprintf( lfile, "Unable to open test file %s: %s\n", 
                                 tpath, strerror( errno ) );
#else
                        fprintf( stderr, "Unable to open test file %s: %s\n", 
                                 tpath, strerror( errno ) );
#endif                   
                        err = 1;
                        goto err_out;
                }
                
                rw = 0;
                gettimeofday( &s, NULL );
                while( rw<SIZE*1024 ) {
                        cur_rw = write( fd, rw_buf, STRIDE*1024 );
                        if( cur_rw <= 0 )
                                break;
                        rw += cur_rw;
                }
#ifdef SYNC
                fsync( fd );
                sync_f = 1;
#endif
                gettimeofday( &e, NULL );
                if( rw != SIZE * 1024 ) {
#ifdef LOGFILE
                        fprintf( lfile, "Error during write (rw=%d). Aborting.\n", (int)rw );
#else                        
                        fprintf( stderr, "Error during write (rw=%d). Aborting.\n", (int)rw );
#endif                        
                }

                t = (e.tv_sec-s.tv_sec)*1000000+(e.tv_usec-s.tv_usec);
#ifdef TOSTDOUT
                printf( "%d %6s%3s\twrite\t%12d\t%8lu\t\tsync=%d,afsflush=%d,stride=%d\n", 
                        (int)time( NULL ), argv[1], argv[i], (int)rw, t, sync_f, afsflush_f, STRIDE );
#endif
#ifdef TOFILE
                if (rfile) {
                        fprintf( rfile, "%d %6s%3s\twrite\t%12d\t%8lu\t\tsync=%d,afsflush=%d,stride=%d\n", 
                                 (int)time( NULL ), argv[1], argv[i], (int)rw, t, sync_f, afsflush_f, STRIDE );
                }
#endif
                close( fd );
                /* end write */
               
        } 

 err_out:
        free( rw_buf );
#ifdef TOFILE
        if (rfile) {
                fclose(rfile);
        }
#endif
#ifdef LOGFILE
        fclose( lfile );
#endif
        return err;
}
