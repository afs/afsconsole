#!/usr/bin/perl

#
# watch the access times 
#
use strict;
use warnings;


# max. acc. access time in ms 
#
my $THRESHOLD = 250;

sub watch_accessTimes() {
	
	my $message = '';
	
	open DATA, "/afs/cern.ch/project/afs/dev/monitoring/console/data_files/servers_switch" or 
	    die "Unable to open data file: $!";
	
	while (<DATA>) {
		
		my @data = split /\s+/, $_;
		my $accessTime = $data[2] + $data[3] + $data[4] + $data[5] + $data[6];
		my $srv = $data[1];
		if ($accessTime > $THRESHOLD) {
			
			$srv = $data[1]; 
			$message = sprintf( "$message\nafs$srv : %.2f ms (avg over last hour)", $accessTime );  
		} 
	}
	
	close DATA;
	
	if ($message ne '') {

		open  MAIL,"|/bin/mail -s 'AFS Access Time Alert' arne.wiebalck\@cern.ch";
		print MAIL "The access times of the following servers seem to be suspiciously high:\n";
		print MAIL "$message\n\n";
		print MAIL "Please have a look.\n";
		close( MAIL );
	}
}

&watch_accessTimes();
