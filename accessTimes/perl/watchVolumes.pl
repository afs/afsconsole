#!/usr/bin/perl

#
# watch the test volume locations
#
use strict;
use warnings;

sub watch_volumes() {
	

	my @info = ();
	@info = `/p/etc/createTestVolumes.pl -c -s`;
	
	if (@info) {
		
		open MAIL,"|/bin/mail -s 'AFS Test Volume Alert' arne.wiebalck\@cern.ch";
		print MAIL "Problems with the following test volumes have been detected:\n\n";
		foreach (@info) {
			print MAIL "$_\n";
		}
		print MAIL "Please run '/p/etc/createTestVolumes.pl -s' or '/p/etc/createTestVolumes.pl -S <afsXY>' to correct this.\n";
		close(MAIL);
	}
}

&watch_volumes();
