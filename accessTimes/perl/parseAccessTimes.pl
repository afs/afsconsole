#!/usr/local/bin/perl

################################################################################
#
# parseAccessTimes.pl
#
#    Usage:
#            parseAccessTimes.pl <data file> [<data files>*]
#
# Analysis of data files as created by "accessTimes".
#
# Writes files with access time distribution for each partition and per
# server. Output files have the same name as the input file with extensions
# ".<partition>.<read>/<write>" for the per partition file and .<read>/<write>
# for the per server file
#
# Initial version by Arne Wiebalck IT/FIO, 2007.
################################################################################

use strict;

################################################################################
#
# Data Structures, Constants, Globals
#
################################################################################

# the number of bins used in the timing distribution files
#
my $BINS = 100;


################################################################################
#
# Subroutines
#
################################################################################

# calc the harmonic sum of an array
#
sub harmonicSum() {

	my @a = @_;
	my ($s,$n) = (0, 0);

	foreach my $e (@a) {

		if( 0 < $e ) {

			$s += 1/$e; 
			$n++;

		} else {

			printf "Found zero value. Returning zero.\n";
			return 0;
		}
	}

	return $n / $s; 
}


################################################################################
#
# Main
#
################################################################################


my $usage = "Usage: $0 <accessTimes data file> [<accessTimes data file>*]\n";

(0 < @ARGV) || die $usage;

# loop over input files
foreach my $dataFile (@ARGV) {
	
	print "Parsing file $dataFile\n";
    
	if( !open DATAFILE, "<$dataFile" ) {
		
		print "Unable to open $dataFile. Skipping.\n";
		next;
	}

	# extract the data, hashes for the partitions, arrays for the whole server
	my (%rFreq, %wFreq, @rSrvFreq, @wSrvFreq, @rValues, @wValues);
	
	my @array = (0);
	@rSrvFreq = (@array) x $BINS;
	@wSrvFreq = (@array) x $BINS;

	while( <DATAFILE> ) {

		my ($ts,$srv,$prt,$op,$size,$time,$options) = split /\s+/, $_;

		if( "read" eq $op ) {
			
			if( !exists( $rFreq{$prt} ) ) {
				
				my @array = (0);
				$rFreq{$prt} = [ (@array) x $BINS ];
			}
			
			# overflow?
			my $ndx = int($time/1000+1);
			if( $ndx >= $BINS ) {
				$ndx = $BINS - 1;
			}

			# inc the bins
			$rFreq{$prt}[$ndx]++;
			$rSrvFreq[$ndx]++;

			# add to the list of all values
			push @rValues, $time; 

		} elsif( "write" eq $op ) {

			if( !exists( $wFreq{$prt} ) ) {
				
				my @array = (0);
				$wFreq{$prt} = [ (@array) x $BINS ];
			}
			# overflow?
			my $ndx = int($time/1000+1);
			if( $ndx >= $BINS ) {
				$ndx = $BINS - 1;
			}
			
			# inc the bins
			$wFreq{$prt}[$ndx]++;
			$wSrvFreq[$ndx]++;
			
			# add to the list of all values
			push @wValues, $time; 

		} else {

			print "Unknown operation found: $op\n";
		}
	}

	close DATAFILE;

	# write out the extracted data per partition
	while( my ($key, $value) = each %rFreq ) {

		if( !open OUTFILE, ">$dataFile.$key.read" ) {
			
			print "Unable to open output file $dataFile.$key.read.\n";
			next;
		}

		for my $i (0..99) {
			
			print OUTFILE $rFreq{$key}[$i] . "\n";
		}
		
		close OUTFILE;
	}

	while( my ($key, $value) = each %wFreq ) {

		if( !open OUTFILE, ">$dataFile.$key.write" ) {
			
			print "Unable to open output file $dataFile.$key.write.\n";
			next;
		}

		for my $i (0..99) {
			
			print OUTFILE $wFreq{$key}[$i] . "\n";
		}
		
		close OUTFILE;
	}

	# write out server summary files
	if( !open OUTFILE, ">$dataFile.read" ) {
			
		print "Unable to open output file $dataFile.read.\n";
	} else {
		
		# harmonic sum (in ms)
		my $hs = &harmonicSum( @rValues ) / 1000;
		print OUTFILE "# harmonic sum: $hs\n";

		foreach my $i (0..$BINS) {

			print OUTFILE $rSrvFreq[$i] . "\n";
		}
	}
	close OUTFILE;

	if( !open OUTFILE, ">$dataFile.write" ) {
			
		print "Unable to open output file $dataFile.write.\n";
	} else {

		# harmonic sum (in ms)
		my $hs = &harmonicSum( @wValues ) / 1000;
		print OUTFILE "# harmonic sum: $hs\n";

		foreach my $i (0..$BINS) {

			print OUTFILE $wSrvFreq[$i] . "\n";
		}
	}

	close OUTFILE;
}
